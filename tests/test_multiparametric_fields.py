#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 08:46:43 2021

@author: souzarf
"""

import unittest
import numpy as np
from postiguar import multiparametric_field


data_type=np.float64

class TestMultiparametricField(unittest.TestCase):

    def setUp(self):

        rng = np.random.default_rng()        

        self.impedance=rng.random((5,10),dtype=data_type)
        self.reflectivity=rng.random((7,3),dtype=data_type)
        self.amplitude=rng.random((8,2),dtype=data_type)

        self.mult_field=multiparametric_field()
        self.mult_field.add_field(self.impedance,field_name="impedance",
                                  short_field_name="Z")
        self.mult_field.add_field(self.reflectivity,field_name="reflectivity",
                                  short_field_name="r")
        self.mult_field.add_field(self.amplitude,field_name="amplitude",
                                  short_field_name="a")
        self.mult_field.add_field(self.amplitude,field_name="impedance",
                                  short_field_name="r")
        
        
    def test_get_item_position(self):
        
        #Test exception raising:
        self.assertRaises(ValueError,self.mult_field._get_field_position,key="permeability")
        
        
        #Test serch of full name:
        position=self.mult_field._get_field_position("impedance")
        self.assertEqual(position,0)
        
        
        #Test serch of short name:
        position=self.mult_field._get_field_position("a")
        self.assertEqual(position,2)
        
    def test_get_item(self):
        
        
        #Testing get item by number:
        
        testing_field=self.mult_field.get_field(1,flatten=False)
        
        np.testing.assert_allclose(testing_field,self.reflectivity)
    
    
    
        testing_field=self.mult_field.get_field(-1,flatten=False)
        
        np.testing.assert_allclose(testing_field,self.amplitude)
    
        #Testing get item by full name:
        testing_field=self.mult_field.get_field("reflectivity",flatten=False)
        
        
        np.testing.assert_allclose(testing_field,self.reflectivity)
        
        
        #Test get item by short name:

        testing_field=self.mult_field.get_field("a",flatten=False)
        
        
        np.testing.assert_allclose(testing_field,self.amplitude)
            
        
    def test_metadata(self):
        
         testing_multi_paratric_field=self.mult_field

         testing_field=testing_multi_paratric_field.get_field(0)
         
         field_metadata=testing_field.dtype.metadata["multiparametric_field"]

         assert field_metadata is testing_multi_paratric_field
         
         
    def test_get_external_field_position(self):
        
        testing_multi_paratric_field=self.mult_field
        
        for ii in range(testing_multi_paratric_field.number_of_fields):
            
            field_view=testing_multi_paratric_field.get_field(ii)
            
            field_number=testing_multi_paratric_field._get_external_field_position(field_view)
        
            assert ii == field_number
        
if __name__ == "__main__":
    unittest.main()

