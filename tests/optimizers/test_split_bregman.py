import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import convolution_reflectivity, total_variation
from postiguar.models import Levenberg_Marquardt_Tykhonov
from postiguar.optimizers import split_bregman
from numpy import testing
from scipy.linalg import toeplitz

class TestBregmanMinimal(unittest.TestCase):

    def setUp(self):


        self.Nx=20
        self.Nz=20
        
        self.input_data_shape=(self.Nx,self.Nz)
        self.reference_data_shape=(self.Nx,self.Nz)

        self.input_data=np.zeros((self.Nx,self.Nz))
        self.reference_data=np.zeros((self.Nx,self.Nz))

        
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.reference_data[ii,:]=(1+np.sin(2.*jj/10.)**2)*np.exp(-np.linspace(0,1,self.Nz)**2)
                
        

        #Creating inversion data:
        self.input_fields=ptg.multiparametric_field()
        self.input_fields.add_field(self.input_data)
        
        
        self.reference_fields=ptg.multiparametric_field()
        self.reference_fields.add_field(self.reference_data)

       
    def test_L2_LMT(self):

        model_epsilons=[0.01,1,10]
        tik_epsilons=[0.01,0.1,1,10.]
        lambda_bs=[0.01,0.1,1.]
        
        
        for model_epsilon in model_epsilons:
            for tik_epsilon in tik_epsilons:
                for test_lambda_b in lambda_bs:
                
                    print(f"lambda_b={test_lambda_b},tik_epsilon={tik_epsilon},m_epsilon={model_epsilon}")
                    testing_model=Levenberg_Marquardt_Tykhonov(self.input_fields[0],
                                                               self.reference_fields[0],
                                                               epsilon=model_epsilon) 
                
                    testing_model.setup()


                    tikhonov_regularizer=total_variation(self.input_fields[0],
                                                         order=0,epsilon=tik_epsilon,
                                                         verbose=False)

                    tikhonov_regularizer.setup()

                    
                    testing_optimizer=split_bregman(smooth_model=testing_model,
                                                    nonsmooth_model=tikhonov_regularizer,
                                                    tol=1e-14,
                                                    inner_iterations=10,
                                                    opt_iterations=500,
                                                    iteration_method="L-BFGS-B",
                                                    lambda_b=test_lambda_b,
                                                    verbose=False)  
                    testing_optimizer.setup()
        
            
            
            
                    theoretical_inversion=model_epsilon*self.reference_data/(model_epsilon+tik_epsilon)
        
        
            #Calculating reflectivity by split-bregman:
                    testing_optimizer.iterate(2e5)
                    sbregman_inversion=self.input_fields.get_field(0,flatten=False)
        
                    testing.assert_allclose(theoretical_inversion,sbregman_inversion,atol=1e-8,rtol=1e-8)
        
                    sbregman_inversion[:]=0


class TestSplitBregmanSetup(unittest.TestCase):

    def setUp(self):

        self.rng = np.random.default_rng(565238)
        n_wav=11
        data_type=np.float64

        self.Nx=20
        self.reflectivity_shape=(self.Nx,self.Nx)
        self.amplitude_shape=self.reflectivity_shape

        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1

        self.reflectivity=self.rng.random(self.reflectivity_shape,dtype=data_type)
        wavelet=self.rng.uniform(low=1.0, high=2.0, size=n_wav)


        #Creating toeplitz matrix:
        toeplitz_c=np.zeros(self.amplitude_shape[0])
        toeplitz_r=np.zeros(self.amplitude_shape[1])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]
        
        self.toeplitz_matrix_same=toeplitz(toeplitz_c,toeplitz_r)

        toeplitz_c=np.zeros(self.full_amplitude_shape[1])
        toeplitz_r=np.zeros(self.full_amplitude_shape[0])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]

        self.toeplitz_matrix_full=toeplitz(toeplitz_c,toeplitz_r)        


        #Creating inversion data:
        self.reflectivity_fields=ptg.multiparametric_field()
        self.reflectivity_fields.add_field(self.reflectivity)
        
        
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(self.reflectivity)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[0],
                               causal_wavelet=True,
                               convolution_mode="same")


        
        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[1],
                               causal_wavelet=True,
                               convolution_mode="full")

        #Reseting reflectivity to 0:
        initial_reflectivity=self.reflectivity_fields.get_field(0)
        initial_reflectivity[:]=0


        self.test_epsilon_1=0.2
        self.test_epsilon_2=2.0
        
        self.test_norm_1="L2"
        self.test_norm_2="L1"
        
        
        self.ps_model=convolution_reflectivity(self.reflectivity_fields[0],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[1],
                                               causal_wavelet=True,
                                               convolution_mode="full")
        self.ps_model.setup()
        
        
        #Create regularization models:
        self.tikhonov_regularier=total_variation(self.reflectivity_fields[0],
                                                 order=1,epsilon=self.test_epsilon_1,
                                                 norm=self.test_norm_1)
        self.tikhonov_regularier.setup()
        
        self.tikhonov_regularier_2=total_variation(self.reflectivity_fields[0],
                                              order=0,epsilon=self.test_epsilon_2,
                                              norm=self.test_norm_2)

        self.tikhonov_regularier_2.setup()
    
        self.testing_optimizer=split_bregman(smooth_model=self.ps_model,
                                             nonsmooth_model=[self.tikhonov_regularier,
                                                             self.tikhonov_regularier_2],
                                             tol=1e-18,
                                             inner_iterations=25,
                                             iteration_method="cg",
                                             verbose=False)
        self.testing_optimizer.setup()

        #setup the fields:
        uk=self.reflectivity_fields.get_multiparametric_field()
        uk[:]=self.rng.random(uk.shape)

        bk=self.testing_optimizer.bk
        dk=self.testing_optimizer.dk

        bk[0][:]=self.rng.random(bk[0].shape)
        bk[1][:]=self.rng.random(bk[1].shape)
        
        dk[0][:]=self.rng.random(dk[0].shape)
        dk[1][:]=self.rng.random(dk[1].shape)

    def test_passed_parameters(self):
            
        norms=self.testing_optimizer.nonsmooth_norm_names
        epsilons=self.testing_optimizer.nonsmooth_epsilon

        #Check Norms:
        self.assertEqual(norms[0],"L2")
        self.assertEqual(norms[1],"L1")

        self.assertEqual(epsilons[0],self.test_epsilon_1)
        self.assertEqual(epsilons[1],self.test_epsilon_2)


    def test_objective_functions(self):

        #Setup the test:
        uk=self.reflectivity_fields.get_multiparametric_field()

        Hu=self.testing_optimizer.smooth_global_objective_function
        Ju=self.testing_optimizer.Ju
        
        ideal_obj=self.ps_model.get_objective_function()
        ideal_reg_1=self.tikhonov_regularier.get_objective_function()
        ideal_reg_2=self.tikhonov_regularier_2.get_objective_function()


        #Check smooth objective function:
        testing.assert_equal(Hu(uk),ideal_obj(uk))
        
        #Check nonsmooth objective_function:
        testing.assert_equal( Ju(uk) , ideal_reg_1(uk)+ideal_reg_2(uk) )
        
    def test_test_inner_objective(self):
        
        #Setup the test:
        lambda_b=self.testing_optimizer.lambda_b
            
            
        #setup the fields:
        uk=self.reflectivity_fields.get_multiparametric_field()

        bk=self.testing_optimizer.bk
        dk=self.testing_optimizer.dk
        
        ress_0=bk[0].copy()
        ress_1=bk[1].copy()
        
        inner_bregman=self.testing_optimizer.inner_bregman_obj

        #Calculate ideal objective function:
        Hu=self.testing_optimizer.smooth_global_objective_function
        
        reg_0=self.tikhonov_regularier.create_residues_function()
        reg_1=self.tikhonov_regularier_2.create_residues_function()
        
        
        #Calculate ideal residuals:
            
        reg_0(uk,ress_0)
        reg_1(uk,ress_1)
        
        
        missfit_0=( (dk[0]-ress_0-bk[0])**2 ).sum()
        missfit_1=( (dk[1]-ress_1-bk[1])**2 ).sum()                      
        
        
        
        ideal_misfit=Hu(uk)+0.5*lambda_b*(missfit_0+missfit_1)
        

        
        #Calculate testing objective:
            
        testing_missfit=inner_bregman(uk)
        
        testing.assert_equal(ideal_misfit,testing_missfit)
        
    def test_bregman_inner_grad(self):
        
        #Setup the test:
        lambda_b=self.testing_optimizer.lambda_b
            
            
        #setup the fields:
        uk=self.reflectivity_fields.get_multiparametric_field()

        bk=self.testing_optimizer.bk
        dk=self.testing_optimizer.dk
        
        ress_0=bk[0].copy()
        ress_1=bk[1].copy()
        
        grad_0=self.testing_optimizer.nonsmooth_grad_workspace[0].copy()
        grad_1=self.testing_optimizer.nonsmooth_grad_workspace[1].copy()
        
        inner_bregman=self.testing_optimizer.inner_bregman_grad

        #Calculate ideal objective function:
        DHu=self.testing_optimizer.smooth_global_gradient_function
        
        reg_0=self.tikhonov_regularier.create_residues_function()
        #reg_1=self.tikhonov_regularier_2.create_residues_function()
        
        jacobian_dot_vec_0=self.tikhonov_regularier.jacobian_dot_vec
        #jacobian_dot_vec_1=self.tikhonov_regularier_2.jacobian_dot_vec
        #Calculate ideal residuals:
            
        reg_0(uk,ress_0)
        ress_1[:]=uk[:]
        
        ress_0= (dk[0]-ress_0-bk[0])
        ress_1= (dk[1]-ress_1-bk[1])                      
        
        jacobian_dot_vec_0(uk,ress_0,grad_0)
        grad_1[:]=ress_1[:]
        
        ideal_grad=DHu(uk)-lambda_b*(grad_0+grad_1)
        

        
        #Calculate testing objective:            
        testing_grad=inner_bregman(uk)

        
        testing.assert_allclose(ideal_grad,testing_grad,atol=1e-12)
        
        
    def test_shrinkage(self):
        
                
        #Setup the test:
        lambda_b=self.testing_optimizer.lambda_b            
        epsilons=self.testing_optimizer.nonsmooth_epsilon
        
        #setup the fields:
        uk=self.reflectivity_fields.get_multiparametric_field()

        bk=self.testing_optimizer.bk
        dk=self.testing_optimizer.dk
        
        ress_0=bk[0].copy()
        ress_1=bk[1].copy()
        
        update_dk=self.testing_optimizer.update_dk
        update_bk=self.testing_optimizer.update_bk

        #Calculate ideal objective function:
        reg_0=self.tikhonov_regularier.create_residues_function()
        
        #Calculate residuals:
            
        reg_0(uk,ress_0)
        ress_1[:]=uk[:]
        
        #Calculating ideal auxiliaries:
        dk_ideal_0=(ress_0+bk[0])/(2*epsilons[0]/lambda_b+1.)
        bk_ideal_0=bk[0]+ress_0-dk_ideal_0
        
        dk_ideal_1=np.maximum((ress_1+bk[1])-(epsilons[1]/lambda_b),0)
        bk_ideal_1=bk[1]+ress_1-dk_ideal_1
        
        
        
        #Calculate testing auxiliares:
        dk_test=[dk[0].copy(),dk[1].copy()]
        bk_test=[bk[0].copy(),bk[1].copy()]
        
        update_dk(uk,bk_test,dk_test)
        update_bk(uk,bk_test,dk_test)
        #Check consistence:
        
        testing.assert_allclose(bk_ideal_0,bk_test[0],atol=1e-12)
        testing.assert_allclose(dk_ideal_0,dk_test[0],atol=1e-12)
        
        testing.assert_allclose(bk_ideal_1,bk_test[1],atol=1e-12)
        testing.assert_allclose(dk_ideal_1,dk_test[1],atol=1e-12)

       



class TestSplitBregmanConv(unittest.TestCase):

    def setUp(self):


        n_wav=11
        self.Nx=20
        self.Nz=self.Nx
        
        self.reflectivity_shape=(self.Nx,self.Nx)
        self.amplitude_shape=self.reflectivity_shape

        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1

        self.reflectivity=np.zeros(self.Nx*self.Nx).reshape(self.Nx,self.Nz)
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.reflectivity[ii,:]=(1+np.cos(2.*jj/10.)**2)*np.exp(np.linspace(0,1,self.Nz)**2)
                
        
        wavelet=np.linspace(0,1,n_wav)
        wavelet=(1+wavelet**2)*np.exp(-wavelet[:]**2/2)


        #Creating toeplitz matrix:
        toeplitz_c=np.zeros(self.amplitude_shape[0])
        toeplitz_r=np.zeros(self.amplitude_shape[1])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]
        
        self.toeplitz_matrix_same=toeplitz(toeplitz_c,toeplitz_r)

        toeplitz_c=np.zeros(self.full_amplitude_shape[1])
        toeplitz_r=np.zeros(self.full_amplitude_shape[0])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]

        self.toeplitz_matrix_full=toeplitz(toeplitz_c,toeplitz_r)        


        #Creating inversion data:
        self.reflectivity_fields=ptg.multiparametric_field()
        self.reflectivity_fields.add_field(self.reflectivity)
        
        
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(self.reflectivity)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[0],
                               causal_wavelet=True,
                               convolution_mode="same")


        
        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[1],
                               causal_wavelet=True,
                               convolution_mode="full")

        #Reseting reflectivity to 0:
        initial_reflectivity=self.reflectivity_fields.get_field(0)
        initial_reflectivity[:]=0

       
    def test_L2_tikonov(self):

        test_epsilons=[0.1,1.,10.]
        test_lambda_b=[0.1,1]
        
        for test_epsilon in test_epsilons:
            for lambda_breg in test_lambda_b:
                
                print(f"epsilon={test_epsilon}, lambda={lambda_breg}")
                ps_model=convolution_reflectivity(self.reflectivity_fields[0],
                                                  self.wavelet_fields[0],
                                                  self.amplitude_fields[0],
                                                  causal_wavelet=True,
                                                  convolution_mode="same",
                                                  verbose=False)
                ps_model.setup()
        
                tikhonov_regularier=total_variation(self.reflectivity_fields[0],
                                            order=0,epsilon=test_epsilon)

                tikhonov_regularier.setup()
    
                testing_optimizer=split_bregman(smooth_model=ps_model,
                                                nonsmooth_model=tikhonov_regularier,
                                                tol=1e-16,
                                                inner_iterations=5,
                                                opt_iterations=100,
                                                iteration_method="L-BFGS-B",
                                                lambda_b=lambda_breg,
                                                verbose=False)
                testing_optimizer.setup()
        
            #Creating ideal data:
            
                pseudo_inv=np.matmul(self.toeplitz_matrix_same.T,self.toeplitz_matrix_same)+test_epsilon*np.identity(self.Nx)
                pseudo_inv=np.linalg.inv(pseudo_inv)
        
                amplitude=self.amplitude_fields.get_field(0,flatten=False)
                theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_same.T,amplitude.T)).T
        
        
            #Calculating reflectivity by split-bregman:
                testing_optimizer.iterate(2000)
                bregman_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
                testing.assert_allclose(theoretical_reflectivity,bregman_reflectivity,atol=1e-5,rtol=1e-5)
        
                bregman_reflectivity[:]=0
        
        
    def test_L2_1Dtv(self):

        test_epsilons=[2.,0.2]
        
        for test_epsilon in test_epsilons:
            ps_model=convolution_reflectivity(self.reflectivity_fields[0],
                                              self.wavelet_fields[0],
                                              self.amplitude_fields[1],
                                              causal_wavelet=True,
                                              convolution_mode="full")
            ps_model.setup()
        
        
            #Create regularization models:
            tikhonov_regularier=total_variation(self.reflectivity_fields[0],
                                            order=1,epsilon=test_epsilon)
            tikhonov_regularier.setup()
        
            tikhonov_regularier_2=total_variation(self.reflectivity_fields[0],
                                            order=1,epsilon=test_epsilon)

            tikhonov_regularier_2.setup()
    
            testing_optimizer=split_bregman(smooth_model=ps_model,
                                            nonsmooth_model=[tikhonov_regularier,
                                                             tikhonov_regularier_2],
                                            tol=1e-18,
                                            inner_iterations=25,
                                            iteration_method="L-BFGS-B",
                                            verbose=False)
            testing_optimizer.setup()
        
            #Creating Regularizer Toeplitz matrix:
            toep_c=np.zeros(self.amplitude_shape[0]-1)
            toep_r=np.zeros(self.amplitude_shape[1])
        
        
        
            toep_r[0]=-1
            toep_r[1]=1
        
            toep_c[0]=toep_r[0]
        
        
            reg_toep=toeplitz(toep_c,toep_r)
        

        
            #Creating ideal data:    
        
            pseudo_inv=np.matmul(self.toeplitz_matrix_full.T,self.toeplitz_matrix_full)+2*test_epsilon*np.matmul(reg_toep.T,reg_toep)
            pseudo_inv=np.linalg.inv(pseudo_inv)
        
            amplitude=self.amplitude_fields.get_field(1,flatten=False)
            theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_full.T,amplitude.T)).T
        
        
            #Calculating reflectivity by split-bregman:
            testing_optimizer.iterate(4000)
            bregman_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
            testing.assert_allclose(theoretical_reflectivity,bregman_reflectivity,atol=1e-5,rtol=1e-5)
        
            bregman_reflectivity[:]=0



    def test_full_nonsmooth_model(self):

        test_epsilons=[20,2,0.2]
        
        for test_epsilon in test_epsilons:
            
            ps_model=convolution_reflectivity(self.reflectivity_fields[0],
                                              self.wavelet_fields[0],
                                              self.amplitude_fields[0],
                                              causal_wavelet=True,
                                              convolution_mode="same")
            ps_model.setup()
        
            tikhonov_regularier=total_variation(self.reflectivity_fields[0],
                                            order=0,epsilon=test_epsilon)

            tikhonov_regularier.setup()
    
            testing_optimizer=split_bregman(nonsmooth_model=[ps_model,tikhonov_regularier],
                                        tol=1e-14,
                                        inner_iterations=5,
                                        iteration_method="L-BFGS-B",
                                        verbose=False)
            testing_optimizer.setup()
        
        #Creating ideal data:
            
            pseudo_inv=np.matmul(self.toeplitz_matrix_same.T,self.toeplitz_matrix_same)+test_epsilon*np.identity(self.Nx)
            pseudo_inv=np.linalg.inv(pseudo_inv)
        
            amplitude=self.amplitude_fields.get_field(0,flatten=False)
            theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_same.T,amplitude.T)).T
        
        
        #Calculating reflectivity by split-bregman:
            testing_optimizer.iterate(4000)
            bregman_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
            testing.assert_allclose(theoretical_reflectivity,bregman_reflectivity,atol=1e-5,rtol=1e-5)
        
            bregman_reflectivity[:]=0



            
if __name__ == "__main__":
    unittest.main()
