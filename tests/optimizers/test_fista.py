import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import Levenberg_Marquardt_Tykhonov
from postiguar.models import convolution_reflectivity
from postiguar.optimizers import fista
from numpy import testing
from scipy.linalg import toeplitz


class TestFistaMinimal(unittest.TestCase):

    def setUp(self):


        self.Nx=20
        self.Nz=20
        
        self.input_data_shape=(self.Nx,self.Nz)
        self.reference_data_shape=(self.Nx,self.Nz)

        self.input_data=np.zeros((self.Nx,self.Nz))
        self.reference_data=np.zeros((self.Nx,self.Nz))

        
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.input_data[ii,:]=(1+np.cos(2.*jj/10.)**2)*np.exp(np.linspace(0,1,self.Nz)**2)
                self.reference_data[ii,:]=(1+np.sin(2.*jj/10.)**2)*np.exp(-np.linspace(0,1,self.Nz)**2)
                
        

        #Creating inversion data:
        self.input_fields=ptg.multiparametric_field()
        self.input_fields.add_field(self.input_data)
        
        
        self.reference_fields=ptg.multiparametric_field()
        self.reference_fields.add_field(self.reference_data)
        
        #Reseting reflectivity to 0:
        initial_data=self.input_fields.get_field(0)
        initial_data[:]=0

       
    def test_L2_LMT(self):

        model_epsilons=[0.1,1.,10.]
        opt_epsilons=[0.01,1.,100.]
        test_Lcs=[10.,100]
        
        for model_epsilon in model_epsilons:
            for opt_epsilon in opt_epsilons:
                for Lc in test_Lcs:
                
                    print(f"Lc={Lc},opt_epsilon={opt_epsilon},m_epsilon={model_epsilon}")
                    testing_model=Levenberg_Marquardt_Tykhonov(self.input_fields[0],
                                                               self.reference_fields[0],
                                                               epsilon=model_epsilon) 
                
                    testing_model.setup()
        
    
                    testing_optimizer=fista(regularizer_norm='L2',
                                            smooth_model=testing_model,
                                            epsilon=opt_epsilon,
                                            Lc=Lc,
                                            tol=1e-16)
                    testing_optimizer.setup()
        
            
            
            
                    theoretical_inversion=model_epsilon*self.reference_data/(model_epsilon+opt_epsilon)
        
        
            #Calculating reflectivity by split-bregman:
                    testing_optimizer.iterate(2e5)
                    fista_reflectivity=self.input_fields.get_field(0,flatten=False)
        
                    testing.assert_allclose(theoretical_inversion,fista_reflectivity,atol=1e-12,rtol=1e-12)
        
                    fista_reflectivity[:]=0
    
    
class TestFistaConv(unittest.TestCase):

    def setUp(self):


        n_wav=11
        self.Nx=20
        self.Nz=20
        
        self.reflectivity_shape=(self.Nx,self.Nz)
        self.amplitude_shape=self.reflectivity_shape

        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1

        self.reflectivity=np.zeros(self.Nx*self.Nx).reshape(self.Nx,self.Nz)
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.reflectivity[ii,:]=(1+np.cos(2.*jj/10.)**2)*np.exp(np.linspace(0,1,self.Nz)**2)
                
        
        wavelet=np.linspace(0,1,n_wav)
        wavelet=(1+wavelet**2)*np.exp(-wavelet[:]**2/2)


        #Creating toeplitz matrix:
        toeplitz_c=np.zeros(self.amplitude_shape[0])
        toeplitz_r=np.zeros(self.amplitude_shape[1])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]
        
        self.toeplitz_matrix_same=toeplitz(toeplitz_c,toeplitz_r)

        toeplitz_c=np.zeros(self.full_amplitude_shape[1])
        toeplitz_r=np.zeros(self.full_amplitude_shape[0])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]

        self.toeplitz_matrix_full=toeplitz(toeplitz_c,toeplitz_r)        


        #Creating inversion data:
        self.reflectivity_fields=ptg.multiparametric_field()
        self.reflectivity_fields.add_field(self.reflectivity)
        
        
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(self.reflectivity)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[0],
                               causal_wavelet=True,
                               convolution_mode="same")


        
        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[1],
                               causal_wavelet=True,
                               convolution_mode="full")

        #Reseting reflectivity to 0:
        initial_reflectivity=self.reflectivity_fields.get_field(0)
        initial_reflectivity[:]=0

       
    def test_L2_tikonov(self):

        test_epsilons=[0.01,1.,100]
        test_Lcs=[1e3,2e3,4e3]
        
        for test_epsilon in test_epsilons:
            for Lc in test_Lcs:
                
                print(f"epsilon={test_epsilon}, Lc={Lc}")
                ps_model=convolution_reflectivity(self.reflectivity_fields[0],
                                                  self.wavelet_fields[0],
                                                  self.amplitude_fields[0],
                                                   causal_wavelet=True,
                                                   convolution_mode="same")
                ps_model.setup()
        
    
                testing_optimizer=fista(smooth_model=ps_model,
                                        regularizer_norm='L2',
                                        epsilon=test_epsilon,
                                        Lc=Lc,
                                        tol=1e-12)
                testing_optimizer.setup()
        
            #Creating ideal data:
            
                pseudo_inv=np.matmul(self.toeplitz_matrix_same.T,self.toeplitz_matrix_same)+test_epsilon*np.identity(self.Nx)
                pseudo_inv=np.linalg.inv(pseudo_inv)
        
                amplitude=self.amplitude_fields.get_field(0,flatten=False)
                theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_same.T,amplitude.T)).T
        
        
            #Calculating reflectivity by split-bregman:
                testing_optimizer.iterate(2e5)
                fista_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
                testing.assert_allclose(theoretical_reflectivity,fista_reflectivity,atol=1e-5,rtol=1e-5)
        
                fista_reflectivity[:]=0
        
                    
if __name__ == "__main__":
    unittest.main()
