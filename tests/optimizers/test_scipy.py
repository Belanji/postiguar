import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import convolution_reflectivity, total_variation
from postiguar.models import Levenberg_Marquardt_Tykhonov 
from numpy import testing
from scipy.linalg import toeplitz


class TestScipyMinimal(unittest.TestCase):

    def setUp(self):


        self.Nx=30
        self.Nz=20
        
        self.input_data_shape=(self.Nx,self.Nz)
        self.reference_data_shape=(self.Nx,self.Nz)

        self.input_data=np.zeros(self.input_data_shape)
        self.reference_data=np.zeros(self.reference_data_shape)

        
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.input_data[ii,:]=(1+np.cos(2.*jj/10.)**2)*np.exp(np.linspace(0,1,self.Nz)**2)
                self.reference_data[ii,:]=(1+np.sin(2.*jj/10.)**2)*np.exp(-np.linspace(0,1,self.Nz)**2)
                
        

        #Creating inversion data:
        self.input_fields=ptg.multiparametric_field()
        self.input_fields.add_field(self.input_data)
        
        
        self.reference_fields=ptg.multiparametric_field()
        self.reference_fields.add_field(self.reference_data)
        
        #Reseting reflectivity to 0:
        initial_data=self.input_fields.get_field(0)
        initial_data[:]=0


        #Creating Derivative matrix:
        toeplitz_c=np.zeros(self.Nz-1)
        toeplitz_r=np.zeros(self.Nz)
        
        toeplitz_c[0]=-1.
        toeplitz_r[0]=-1.
        toeplitz_r[1]=1
        
        self.Derivative_matrix=toeplitz(toeplitz_c,toeplitz_r)
       
    def test_L2_Tik(self):

        lmt_epsilons=[0.1,1.,10.]
        tik_epsilons=[0.01,1.,100.]
        
        for lmt_epsilon in lmt_epsilons:
            for tik_epsilon in tik_epsilons:
                

                lmt_model=Levenberg_Marquardt_Tykhonov(self.input_fields[0],
                                                       self.reference_fields[0],
                                                       epsilon=lmt_epsilon,
                                                       verbose=False) 
                
                lmt_model.setup()
        
                tikhonov_regularizer=total_variation(self.input_fields[0],
                                                     order=0,
                                                     epsilon=tik_epsilon,
                                                     verbose=False)

                tikhonov_regularizer.setup()
    
    
    
                testing_optimizer=ptg.optimizers.scipy([lmt_model,tikhonov_regularizer],
                                                   tol=1e-18)
                testing_optimizer.setup()
    
    

        
            
            
            
                theoretical_inversion=lmt_epsilon*self.reference_data/(lmt_epsilon+tik_epsilon)
        
        
                #Inverting data by scipy:
                testing_optimizer.iterate(2e5)
                scipy_inversion=self.input_fields.get_field(0,flatten=False)
        
                testing.assert_allclose(theoretical_inversion,scipy_inversion,atol=1e-12,rtol=1e-12)
        
                scipy_inversion[:]=0
                
    def test_L2_TV(self):
        
        lmt_epsilons=[0.1,1.,10.]
        tv_epsilons=[0.01,1.,100.]
        
        for lmt_epsilon in lmt_epsilons:
            for tv_epsilon in tv_epsilons:
                
                print(f"lmt_epsilon={lmt_epsilon},tv_epsilon={tv_epsilon}")
                lmt_model=Levenberg_Marquardt_Tykhonov(self.input_fields[0],
                                                       self.reference_fields[0],
                                                       epsilon=lmt_epsilon,
                                                       verbose=False) 
                
                lmt_model.setup()
        
                tv_regularizer=total_variation(self.input_fields[0],
                                                order=1,dimension=-1,
                                                epsilon=tv_epsilon)

                tv_regularizer.setup()
    
    
    
                testing_optimizer=ptg.optimizers.scipy([lmt_model,tv_regularizer],
                                                   options={'ftol': 1e-16, 'gtol': 1e-16})
                testing_optimizer.setup()
    
    

                pseudo_inv=tv_epsilon*np.matmul(self.Derivative_matrix.T,self.Derivative_matrix)+lmt_epsilon*np.identity(self.Nz)
                pseudo_inv=np.linalg.inv(pseudo_inv)
        
                Rhs=self.reference_fields.get_field(0,flatten=False).T
                theoretical_inversion=lmt_epsilon*np.matmul(pseudo_inv,Rhs).T        
        
                #Calculating reflectivity by split-bregman:
                testing_optimizer.iterate(2e6)
                scipy_inversion=self.input_fields.get_field(0,flatten=False)
        
                testing.assert_allclose(theoretical_inversion,scipy_inversion,atol=1e-8,rtol=1e-8)
        
                scipy_inversion[:]=0
                


if __name__ == "__main__":
    unittest.main()
