import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import convolution_reflectivity, total_variation 
from numpy import testing
from scipy.linalg import toeplitz


class TestScipyOptimizer(unittest.TestCase):

    def setUp(self):


        n_wav=20
        self.Nx=40
        self.Nz=40
        
        self.reflectivity_shape=(self.Nx,self.Nz)
        self.amplitude_shape=self.reflectivity_shape

        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1

        self.reflectivity=np.zeros(self.Nx*self.Nx).reshape(self.Nx,self.Nz)
        for jj in range(self.Nz):
            for ii in range(self.Nx):
                
                self.reflectivity[ii,:]=(1+np.cos(2.*jj/10.)**2)*np.exp(np.linspace(0,1,self.Nz)**2)
                
        
        wavelet=np.linspace(0,1,n_wav)
        wavelet=(1-wavelet**2)*np.exp(-15*wavelet[:]**2/2)


        #Creating toeplitz matrix:
        toeplitz_c=np.zeros(self.amplitude_shape[0])
        toeplitz_r=np.zeros(self.amplitude_shape[1])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]
        
        self.toeplitz_matrix_same=toeplitz(toeplitz_c,toeplitz_r)

        toeplitz_c=np.zeros(self.full_amplitude_shape[1])
        toeplitz_r=np.zeros(self.full_amplitude_shape[0])
        
        toeplitz_c[0:n_wav]=wavelet
        toeplitz_r[0]=wavelet[0]

        self.toeplitz_matrix_full=toeplitz(toeplitz_c,toeplitz_r)        


        #Creating inversion data:
        self.reflectivity_fields=ptg.multiparametric_field()
        self.reflectivity_fields.add_field(self.reflectivity)
        
        
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(self.reflectivity)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data((self.reflectivity_fields,0),
                               (self.wavelet_fields,0),
                               (self.amplitude_fields,0),
                               causal_wavelet=True,
                               convolution_mode="same")


        
        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data((self.reflectivity_fields,0),
                               (self.wavelet_fields,0),
                               (self.amplitude_fields,1),
                               causal_wavelet=True,
                               convolution_mode="full")

        #Reseting reflectivity to 0:
        initial_reflectivity=self.reflectivity_fields.get_field(0)
        initial_reflectivity[:]=0

        
    def test_L2_tikonov_high_epsilon(self):

        test_epsilons=[50,5,0.5,0.05,0.005,0.0005]
        
        for test_epsilon in test_epsilons:
            
            ps_model=convolution_reflectivity((self.reflectivity_fields,0),
                                              (self.wavelet_fields,0),
                                              (self.amplitude_fields,0),
                                              causal_wavelet=True,
                                              convolution_mode="same")
            ps_model.setup()
        
            tikhonov_regularier=total_variation((self.reflectivity_fields,0),
                                                order=0,epsilon=test_epsilon)

            tikhonov_regularier.setup()
    
            testing_optimizer=ptg.optimizers.scipy(self.reflectivity_fields,
                                                   [ps_model,tikhonov_regularier],
                                                   tol=1e-18)
            testing_optimizer.setup()
        
            #Creating ideal data:
            
            pseudo_inv=np.matmul(self.toeplitz_matrix_same.T,self.toeplitz_matrix_same)+test_epsilon*np.identity(self.Nx)
            pseudo_inv=np.linalg.inv(pseudo_inv)
        
            amplitude=self.amplitude_fields.get_field(0,flatten=False)
            theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_same.T,amplitude.T)).T
        
        
            #Calculating reflectivity by scipy minmize:
            sucess=testing_optimizer.iterate(30000)
            scipy_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
            testing.assert_allclose(scipy_reflectivity,theoretical_reflectivity,rtol=1e-12)
        
            scipy_reflectivity[:]=0
            
            
    def test_L2_1Dtv(self):

        test_epsilon=1
        ps_model=convolution_reflectivity((self.reflectivity_fields,0),
                                               (self.wavelet_fields,0),
                                               (self.amplitude_fields,1),
                                               causal_wavelet=True,
                                               convolution_mode="full")
        ps_model.setup()
        
        
        #Create regularization models:
        tikhonov_regularizer=total_variation((self.reflectivity_fields,0),
                                            order=1,epsilon=test_epsilon)
        tikhonov_regularizer.setup()
        
        tikhonov_regularizer_2=total_variation((self.reflectivity_fields,0),
                                            order=1,epsilon=test_epsilon)

        tikhonov_regularizer_2.setup()
    
        testing_optimizer=ptg.optimizers.scipy(self.reflectivity_fields,
                                                   [ps_model,
                                                    tikhonov_regularizer,
                                                    tikhonov_regularizer_2],
                                                   tol=1e-16)
        testing_optimizer.setup()
        
        #Creating Regularizer Toeplitz matrix:
        toep_c=np.zeros(self.amplitude_shape[0]-1)
        toep_r=np.zeros(self.amplitude_shape[1])
        
        
        
        toep_r[0]=-1
        toep_r[1]=1
        
        toep_c[0]=toep_r[0]
        
        
        reg_toep=toeplitz(toep_c,toep_r)
        

        
        #Creating ideal data:    
        
        pseudo_inv=np.matmul(self.toeplitz_matrix_full.T,self.toeplitz_matrix_full)+2*test_epsilon*np.matmul(reg_toep.T,reg_toep)
        pseudo_inv=np.linalg.inv(pseudo_inv)
        
        amplitude=self.amplitude_fields.get_field(1,flatten=False)
        theoretical_reflectivity=np.matmul(pseudo_inv,np.matmul(self.toeplitz_matrix_full.T,amplitude.T)).T
        
        
        #Calculating reflectivity:
        testing_optimizer.iterate(80000)
        scipy_reflectivity=self.reflectivity_fields.get_field(0,flatten=False)
        
        testing.assert_allclose(scipy_reflectivity,theoretical_reflectivity,atol=1e-12,rtol=1e-12)
        
        scipy_reflectivity[:]=0

        

if __name__ == "__main__":
    unittest.main()
