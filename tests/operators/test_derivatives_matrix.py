import unittest
import numpy as np
from postiguar.operators.derivatives import create_derivative_matrix
from numpy import testing



class TestDerivatives(unittest.TestCase):

    def setUp(self):

        nx=13
        ny=7
        nz=19

        rng = np.random.default_rng(565238)

        self.data_1D=rng.random(nx)
        self.data_2D=rng.random((nx,ny))
        self.data_3D=rng.random((nx,ny,nz))

    def test_1D_first_derivative(self):

        Derivative_Matrix=create_derivative_matrix(self.data_1D.shape,
                                                   dimensions=0,order=1)
        
        
        ideal_data=self.data_1D[1:]-self.data_1D[0:-1]
        test_data=Derivative_Matrix._mul_vector(self.data_1D)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
    def test_2D_first_derivative(self):

        #Test derivative in the last dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=-1,order=1)
        
        
        ideal_data=(self.data_2D[:,1:]-self.data_2D[:,0:-1]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_2D.flatten())
        
    
        testing.assert_allclose(ideal_data,test_data)
        
        
        #Test Derivative in the first dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=0,order=1)
        
        
        ideal_data=(self.data_2D[1:,:]-self.data_2D[0:-1,:]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_2D.flatten())
        
    
        testing.assert_allclose(ideal_data,test_data)
        
        
        
        
    def test_3D_first_derivative(self):

        #Test derivative in the last dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=-1,order=1)
        
        
        ideal_data=(self.data_3D[...,1:]-self.data_3D[...,0:-1]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_3D.flatten())
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        
        #Test derivative in the second dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=1,order=1)
        
        
        ideal_data=(self.data_3D[:,1:,:]-self.data_3D[:,0:-1,:]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_3D.flatten())
        
        
        #Test derivative in the first dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=0,order=1)
        
        
        ideal_data=(self.data_3D[1:,:,:]-self.data_3D[0:-1,:,:]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_3D.flatten())
        
        

        testing.assert_allclose(ideal_data,test_data)
        

    def test_2D_crossed_derivative(self):
        
        #Test derivative in the last dimension:
        Derivative_Matrix=create_derivative_matrix(self.data_2D.shape,
                                                           dimensions=(-1,-2),order=(1,1))
        
        
        ideal_data=(self.data_2D[:-2,:-2]+self.data_2D[2:,2:]-self.data_2D[:-2,2:]-self.data_2D[2:,:-2]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_2D.flatten())

    
        testing.assert_allclose(ideal_data,test_data)
        

    def test_3D_crossed_derivative(self):
        
        #Test derivative in the last dimensions:
        Derivative_Matrix=create_derivative_matrix(self.data_3D.shape,
                                                           dimensions=(-1,-2),order=(1,1))
        
        
        ideal_data=(self.data_3D[:,:-2,:-2]+self.data_3D[:,2:,2:]-self.data_3D[:,:-2,2:]-self.data_3D[:,2:,:-2]).flatten()
        test_data=Derivative_Matrix._mul_vector(self.data_3D.flatten())

    
        testing.assert_allclose(ideal_data,test_data)



        
if __name__ == "__main__":
    unittest.main()


        
