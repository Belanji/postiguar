#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 14:47:15 2021

@author: souzarf
"""

import unittest
import numpy as np
from scipy.linalg import toeplitz
from numpy import testing


from postiguar.operators.convolution import create_convolution_function, create_convolution_transposed_function

rng = np.random.default_rng()

data_file="../ReflectionTestData2D.bin"
data_shape=(10,2000)
data_type=np.float64


test_data=rng.random(data_shape,dtype=data_type)

class TestSameNonCausalConvolution(unittest.TestCase):

    def setUp(self):
        
        n_wav=51
        
        wavelet_times = np.arange(-(n_wav//2),n_wav//2+1,1,dtype=np.float64)*np.pi/n_wav
        self.circ_wav=np.sin(wavelet_times)

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(data_shape[1])
        toeplitz_c[0:n_wav//2+1]=self.circ_wav[n_wav//2:n_wav]

        rows=self.circ_wav[0:n_wav//2+1]
        rows=np.flip(rows)
        
        toeplitz_r=np.zeros(data_shape[1])
        toeplitz_r[0:n_wav//2+1]=rows[:]
        
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)

    def test_valid_left_convolution(self):

        function_data=np.zeros(data_shape).flatten()
        
        convolve=create_convolution_transposed_function(test_data.shape,self.circ_wav.shape,test_data.shape,causal_wavelet=False,convolution_mode="same")


        function_data=test_data.flatten().copy()
        ideal_data=np.matmul(test_data,self.toeplitz_matrix)
                
        
        
        convolve(test_data.flatten(),self.circ_wav,function_data)
        

        testing.assert_allclose(ideal_data.flatten(),function_data.flatten())
                    

    def test_valid_right_convolution(self):

        function_data=np.zeros(data_shape).flatten()
        
        #Call the operator we want to test
        convolve=create_convolution_function(self.circ_wav.shape,test_data.shape,test_data.shape,causal_wavelet=False,convolution_mode="same")
        
        
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T

        
        convolve(self.circ_wav,test_data.flatten(),function_data)
        testing.assert_allclose(ideal_data.flatten(),function_data)
             
        
        
if __name__ == "__main__":
    unittest.main()

    
