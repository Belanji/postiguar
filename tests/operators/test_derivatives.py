import unittest
import numpy as np
from postiguar.operators.derivatives import derivative_operator, create_derivative_matrix
from numpy import testing



class TestDerivatives(unittest.TestCase):

    def setUp(self):

        rng = np.random.default_rng(565238)        

        self.nx=15
        self.ny=7
        self.nz=9

        self.data_1D=rng.random(self.nx)
        self.data_2D=rng.random((self.nx,self.ny))
        self.data_3D=rng.random((self.nx,self.ny,self.nz))


    def test_data_dimensions(self):
        
        """Assert the operator breaks when data does not have requested
        derivative dimension"""
        
        #1D:
        self.assertRaises(ValueError,derivative_operator,self.data_1D.shape,
                                                         dimensions=1,order=1)
        
        self.assertRaises(ValueError,derivative_operator,self.data_1D.shape,
                                                         dimensions=-2,order=1)
        
        #2D:
        self.assertRaises(ValueError,derivative_operator,self.data_2D.shape,
                                                         dimensions=-3,order=1)
        
        self.assertRaises(ValueError,derivative_operator,self.data_2D.shape,
                                                         dimensions=2,order=1)  

        #3D:
        self.assertRaises(ValueError,derivative_operator,self.data_3D.shape,
                                                         dimensions=-4,order=1)
        
        self.assertRaises(ValueError,derivative_operator,self.data_3D.shape,
                                                         dimensions=3,order=1) 
    
    def test_0_order_derivative(self):
        
        
        #1D:
        Derivative_operator=derivative_operator(self.data_1D.shape,
                                                   dimensions=0,order=0)
        
        
        #Apply from the left:
        ideal_data=self.data_1D
        
        test_data=np.zeros(self.nx)
        Derivative_operator.left_apply(self.data_1D,test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        #Apply from the right:
        
        ideal_data=self.data_1D
        
        test_data=np.zeros(self.nx)
        Derivative_operator.right_apply(self.data_1D,test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        
        #2D
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                   dimensions=0,order=0)
        
        
        #Left Apply:
        ideal_data=self.data_2D.flatten()
        
        test_data=np.zeros(self.nx*self.ny)
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        #Right Apply:
            
        ideal_data=self.data_2D.flatten()
        
        test_data=np.zeros(self.nx*self.ny)
        Derivative_operator.right_apply(self.data_2D.flatten(),test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        #3D
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=1,order=0)
        
        #Left apply:
        ideal_data=self.data_3D.flatten()
        
        test_data=np.zeros(self.nx*self.ny*self.nz)
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)
        
        #right apply:
        ideal_data=self.data_3D.flatten()
        
        test_data=np.zeros(self.nx*self.ny*self.nz)
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
        

        testing.assert_allclose(ideal_data,test_data)

    def test_1D_first_derivative(self):

        Derivative_operator=derivative_operator(self.data_1D.shape,
                                                   dimensions=0,order=1)
    
        
        #Left Apply:
        left_ideal_data=self.data_1D[1:]-self.data_1D[0:-1]
        
        test_data=np.zeros(self.nx-1)
        Derivative_operator.left_apply(self.data_1D,test_data)

        testing.assert_allclose(left_ideal_data,test_data)
        
        
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_1D.shape,
                                                   dimensions=0,order=1)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
        
        
    def test_2D_first_derivative(self):

        #Test derivative in the last dimension:
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                   dimensions=-1,order=1)
        
        
        left_ideal_data=(self.data_2D[:,1:]-self.data_2D[:,0:-1]).flatten()
        test_data=np.zeros(self.nx*(self.ny-1))
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
        
    
        testing.assert_allclose(test_data,left_ideal_data)
        
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=-1,order=1)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
        
        
        #Test Derivative in the first dimension:
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                   dimensions=0,order=1)
        
        #Create derivative matrix for right apply test:
        
        test_data=np.zeros((self.nx-1)*(self.ny))
        left_ideal_data=(self.data_2D[1:,:]-self.data_2D[0:-1,:]).flatten()
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
        
    
        testing.assert_allclose(test_data,left_ideal_data)
        
        
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=0,order=1)
       
        
        
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        

        testing.assert_allclose(test_data,right_ideal_data)
        
        
    def test_3D_first_derivative(self):

        #Test derivative in the last dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=-1,order=1)
        
        
        left_ideal_data=(self.data_3D[:,:,1:]-self.data_3D[:,:,0:-1]).flatten()
        
        test_data=np.zeros((self.nx)*(self.ny)*(self.nz-1))
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
        

        testing.assert_allclose(left_ideal_data,test_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=-1,order=1)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=2,order=1)
       
        
        
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        

        testing.assert_allclose(test_data,right_ideal_data)
        
        
        #Test derivative in the second dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=1,order=1)
        
        
        left_ideal_data=(self.data_3D[:,1:,:]-self.data_3D[:,0:-1,:]).flatten()
        
        test_data=np.zeros((self.nx)*(self.ny-1)*(self.nz))
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
        

        testing.assert_allclose(left_ideal_data,test_data)
        
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=1,order=1)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
        
        
        #Test derivative in the first dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=0,order=1)
        
        
        left_ideal_data=(self.data_3D[1:,:,:]-self.data_3D[0:-1,:,:]).flatten()
        
        test_data=np.zeros((self.nx-1)*(self.ny)*(self.nz))
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
        

        testing.assert_allclose(left_ideal_data,test_data)
            
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=0,order=1)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
    
    def test_1D_second_derivative(self):

        Derivative_operator=derivative_operator(self.data_1D.shape,
                                                   dimensions=0,order=2)
        
        
        left_ideal_data=self.data_1D[2:]-2*self.data_1D[1:-1]+self.data_1D[0:-2]
        
        test_data=np.zeros(self.nx-2)
        Derivative_operator.left_apply(self.data_1D,test_data)
        
        testing.assert_allclose(test_data,left_ideal_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_1D.shape,
                                                   dimensions=0,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data)
        
        
    def test_2D_second_derivative(self):

        #Test derivative in the last dimension:
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                   dimensions=-1,order=2)
        
        left_ideal_data=(self.data_2D[:,2:]-2*self.data_2D[:,1:-1]+self.data_2D[:,0:-2]).flatten()

        test_data=np.zeros(self.nx*(self.ny-2))
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
        
    
        testing.assert_allclose(left_ideal_data,test_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=-1,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data) 
        
        
        
        
        #Test Derivative in the first dimension:
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                   dimensions=0,order=2)

        
        left_ideal_data=(self.data_2D[2:,:]-2*self.data_2D[1:-1,:]+self.data_2D[0:-2,:]).flatten()

        test_data=np.zeros((self.nx-2)*(self.ny))
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
        
    
        testing.assert_allclose(left_ideal_data,test_data)


        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_2D.shape,
                                                   dimensions=0,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        #testing.assert_allclose(test_data,right_ideal_data)        

    def test_3D_second_derivative(self):

        #Test derivative in the first dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=-3,order=2)
        
        left_ideal_data=(self.data_3D[2:,:,:]-2*self.data_3D[1:-1,:,:]+self.data_3D[0:-2,:,:]).flatten()

        test_data=np.zeros((self.nx-2)*self.nz*self.ny)
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
    
        testing.assert_allclose(left_ideal_data,test_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=-3,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data) 
        
        
        
        
        
    
        #Test derivative in the second dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=-2,order=2)
        
        left_ideal_data=(self.data_3D[:,2:,:]-2*self.data_3D[:,1:-1,:]+self.data_3D[:,0:-2,:]).flatten()

        test_data=np.zeros((self.ny-2)*self.nz*self.nx)
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
    
        testing.assert_allclose(left_ideal_data,test_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=-2,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data) 



        
        
        #Test derivative in the second dimension:
        Derivative_operator=derivative_operator(self.data_3D.shape,
                                                   dimensions=2,order=2)
        
        left_ideal_data=(self.data_3D[...,2:]-2*self.data_3D[...,1:-1]+self.data_3D[...,0:-2]).flatten()

        test_data=np.zeros((self.nz-2)*self.nx*self.ny)
        Derivative_operator.left_apply(self.data_3D.flatten(),test_data)
        
    
        testing.assert_allclose(left_ideal_data,test_data)
        
        #Right Apply:
        derivative_matrix=create_derivative_matrix(self.data_3D.shape,
                                                   dimensions=2,order=2)
        derivative_matrix=derivative_matrix.T
        test_data=np.zeros(self.nx*self.ny*self.nz)
        
        right_ideal_data=derivative_matrix._mul_vector(left_ideal_data)
        Derivative_operator.right_apply(left_ideal_data,test_data)
        
        testing.assert_allclose(test_data,right_ideal_data) 
    
    
    def test_2D_crossed_derivative(self):
        
        #Test derivative in the last dimension:
        Derivative_operator=derivative_operator(self.data_2D.shape,
                                                           dimensions=(-1,-2),order=(1,1))
        
        #Create test data to execute the test:
        test_data=np.zeros((self.nx-2)*(self.ny-2))
        
        left_ideal_data=(self.data_2D[:-2,:-2]+self.data_2D[2:,2:]-self.data_2D[:-2,2:]-self.data_2D[2:,:-2]).flatten()
        
        Derivative_operator.left_apply(self.data_2D.flatten(),test_data)
    
        testing.assert_allclose(left_ideal_data,test_data)
        
        #Testing right apply:
        Derivative_Matrix=create_derivative_matrix(self.data_2D.shape,
                                                           dimensions=(-1,-2),order=(1,1))
        Derivative_Matrix=Derivative_Matrix.T
        right_ideal_data=Derivative_Matrix._mul_vector(left_ideal_data)
        
        
        test_data=np.zeros(self.nx*self.ny)
        test_data=Derivative_Matrix._mul_vector(left_ideal_data)
        
        testing.assert_allclose(test_data,right_ideal_data) 

if __name__ == "__main__":
    unittest.main()


        
