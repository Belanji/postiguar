import unittest
import numpy as np
from scipy.linalg import toeplitz
from numpy import testing


from postiguar.operators.convolution import create_convolution_function, create_convolution_transposed_function

rng = np.random.default_rng()

data_shape=(5,2000)
data_type=np.float64

n_wav=51

test_data=rng.random(data_shape,dtype=data_type)
wav=rng.random(n_wav)


class TestSameCausalConvolution(unittest.TestCase):

    def setUp(self):
        
        #Create the toeplitz matrix
        toeplitz_c=np.zeros(data_shape[-1])
        toeplitz_c[:n_wav]=wav[:n_wav]
      
        toeplitz_r=np.zeros(data_shape[-1])

        
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)

    def test_same_left_convolution(self):

        convolution_data=np.zeros(data_shape).flatten()

        convolve=create_convolution_transposed_function(test_data.shape,wav.shape,test_data.shape,causal_wavelet=True,convolution_mode="same")

        
        ideal_data=np.matmul(test_data,self.toeplitz_matrix)
        convolve(test_data.flatten(),wav,convolution_data)
        

        testing.assert_allclose(ideal_data.flatten(),convolution_data)
                    

    def test_same_right_convolution(self):

        convolution_data=np.zeros(data_shape).flatten()    

        #Call the operator we want to test
        convolve=create_convolution_function(wav.shape,test_data.shape,test_data.shape,causal_wavelet=True,convolution_mode="same")
        
        
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T
        
        
        convolve(wav,test_data.flatten(),convolution_data)
        testing.assert_allclose(ideal_data.flatten(),convolution_data)
      


class TestFullCausalConvolution(unittest.TestCase):

    def setUp(self):

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(data_shape[-1]+n_wav-1)
        toeplitz_c[:n_wav]=wav[:n_wav]
      
        toeplitz_r=np.zeros(data_shape[-1])

        
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)

    def test_full_left_convolution(self):

        residues_shape=(data_shape[0],data_shape[-1]+n_wav-1)
        
        residues=rng.random(residues_shape)
        convolution_data=np.zeros(data_shape).ravel()

        convolve=create_convolution_transposed_function(residues_shape,wav.shape,data_shape,causal_wavelet=True,convolution_mode="full")

        
        #Generates ideal and function data to be compared:
        ideal_data=np.matmul(residues,self.toeplitz_matrix)
        convolve(residues.flatten(),wav.flatten(),convolution_data)
        
        #breakpoint()
        testing.assert_allclose(ideal_data.flatten(),convolution_data)
                    

    def test_full_right_convolution(self):

        
        output_shape=(data_shape[0],data_shape[-1]+n_wav-1)
        
        
        convolution_data=np.zeros(output_shape).flatten()    

        #Call the operator we want to test
        convolve=create_convolution_function(wav.shape,test_data.shape,output_shape,causal_wavelet=True,convolution_mode="full")
        
        
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T
        
        
        convolve(wav,test_data.flatten(),convolution_data)
        testing.assert_allclose(ideal_data.flatten(),convolution_data)


                            
if __name__ == "__main__":
    unittest.main()

    
