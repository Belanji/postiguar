import unittest
import numpy as np
import postiguar as ptg
from postiguar.models.model_base import inversion_model
from numpy import testing


#Create an actual empty model for testing the absteact base model:
class EmptyModel(inversion_model):
         
    _number_of_fields=2
         
    def __init__(self,field_1,field_2,
                optimize_fields=(True,True),
                norm="L2",
                epsilon=5):
                
        super().__init__([field_1,field_2],
                       optimize_fields,
                       norm,
                       epsilon)
         
    def get_objective_function(self,*args,**kwargs):
               
        pass
           
    def calculate_missfit(self,*args,**kwargs):
               
        pass

    def setup(self,*args,**kwargs):
        """Setp up the seismic model.

        Returns:
        ----------------------

        itself"""
        
        
        return self

class TestModelBase(unittest.TestCase):


    def setUp(self):

        
        #rng = np.random.default_rng()
        #data_type=np.float64

        self.nx1=7; self.ny1=15
        self.nx2=30; self.ny2=25; self.nz2=14; 
        
        self.field_1_shape=[self.nx1,self.ny1]
        self.field_2_shape=[self.nx2,self.ny2,self.nz2]
        

        self.field_1=ptg.multiparametric_field(number_of_fields=1,
                                               field_shapes=self.field_1_shape)
        self.field_1.add_field(field_shape=(3,7))        


        self.field_2=ptg.multiparametric_field(number_of_fields=1,
                                               field_shapes=self.field_2_shape)
        
        self.numpy_field=np.zeros((13,21))

    def test_set_field_shape(self):
        
        
        testing_model=EmptyModel(self.field_1["x0"],
                                 self.numpy_field,
                                 optimize_fields=(True,False)
                                 )
        
        #Test field shape:
        field_shapes=testing_model._field_shapes
        
        testing.assert_allclose(field_shapes[0],self.field_1_shape)
        testing.assert_allclose(field_shapes[1],self.numpy_field.shape)
        
    def test_set_field_offsets(self):
        
        #Test field offset:
        testing_model=EmptyModel(self.numpy_field,
                                 self.field_1["x1"],
                                 optimize_fields=(True,False)
                                 )
        
        field_offsets=testing_model._field_offsets
        
        #Calculate field_1[x1] offset:
        f1_offset=self.nx1*self.ny1
        
        testing.assert_equal(field_offsets[0],0)
        testing.assert_equal(field_offsets[1],f1_offset)
            
    def test_set_working_multiparametric_field(self):
        
        #Test with field 1:
        testing_model=EmptyModel(self.field_1["x0"],
                                 self.field_2["x0"],
                                 optimize_fields=(True,False)
                                 )
        multiparametric_field=testing_model._working_multiparametric_field
        
        assert multiparametric_field is self.field_1
        
        #Test with optimizing field 2:
        testing_model=EmptyModel(self.field_1["x0"],
                                 self.field_2["x0"],
                                 optimize_fields=(False,True)
                                 )
        multiparametric_field=testing_model._working_multiparametric_field
        
        assert multiparametric_field is self.field_2
        
        #Testing with a numpy array:
        numpy_field=np.ones((35,60))
         
        testing_model=EmptyModel(numpy_field,
                                 self.field_2["x0"],
                                 optimize_fields=(True,False)
                                 )
        
        multiparametric_field=testing_model._working_multiparametric_field
        testing.assert_allclose(multiparametric_field,numpy_field.flatten())
            
if __name__ == "__main__":
    unittest.main()