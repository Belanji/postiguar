import unittest
import numpy as np
from postiguar import multiparametric_field
from postiguar.models import total_variation
from numpy import testing


class TestTvModel(unittest.TestCase):


    def setUp(self):
        
        
        self.nx=20
        self.ny=8
        self.nz=13
        
        self.data_1D=np.zeros(self.nx)
        self.data_2D=np.zeros(self.nx*self.ny).reshape(self.nx,self.ny)
        self.data_3D=np.zeros(self.nx*self.ny*self.nz).reshape(self.nx,self.ny,self.nz)
        
        for ii in range(self.nx):
            
            self.data_1D[ii]=np.cos(np.pi*float(ii)/self.nx)

            for jj in range(self.ny):
            
                self.data_2D[ii,jj]=self.data_1D[ii]+(float(jj)/self.ny)**2    
            
                for kk in range(self.nz):

                    self.data_3D[ii,jj,kk]=self.data_2D[ii,jj]*np.exp(-float(kk)/self.nz)                

        self.input_data=multiparametric_field()
        self.input_data.add_field(self.data_1D)
        self.input_data.add_field(self.data_2D)
        self.input_data.add_field(self.data_3D)
        
    def test_1D_zero_derivative(self):
        
        test_epsilon=0.37
        
        testing_model=total_variation(self.input_data[0],
                                      epsilon=test_epsilon,
                                      norm="L2",order=0,
                                      dimensions=-1,
                                      verbose=False)
        
        #Testing residual:
        testing_model.setup()
        x_opt=self.input_data.get_multiparametric_field()
        
        residues=np.ones(self.data_1D.shape).flatten()
        ideal_residues=self.data_1D.flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #Testing missfit:
        ideal_missfit=test_epsilon*(residues*residues).sum()
        testing_missfit=testing_model.calculate_missfit()

        testing.assert_almost_equal(ideal_missfit,testing_missfit,decimal=12)
                                    
        #Testing Grad:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        grad_func=testing_model.get_gradient_function()
        
        
        ideal_grad[:self.nx]=2.*test_epsilon*self.data_1D
        testing_grad[:]=grad_func(x_opt)
        
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)


    def test_2D_zero_derivative(self):
        

        test_epsilon=0.43
        
        testing_model=total_variation(self.input_data[1],
                                      epsilon=test_epsilon,
                                      norm="L2",order=0,
                                      dimensions=-1,
                                      verbose=False)
        
        #Testing residual:
        testing_model.setup()
        x_opt=self.input_data.get_multiparametric_field()
        
        residues=np.ones(self.data_2D.shape).flatten()
        ideal_residues=self.data_2D.flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #Testing missfit:
        ideal_missfit=test_epsilon*(residues*residues).sum()
        testing_missfit=testing_model.calculate_missfit()

        testing.assert_almost_equal(ideal_missfit,testing_missfit,decimal=12)
                                    
        #Testing Grad:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        grad_func=testing_model.get_gradient_function()
        
        ofset=self.nx
        length=self.nx*self.ny
        
        ideal_grad[ofset:ofset+length]=2.*test_epsilon*self.data_2D.flatten()
        testing_grad[:]=grad_func(x_opt)
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)



    def test_2D_first_derivative_dimension_1(self):
        
        test_epsilon=0.57
        
        testing_model=total_variation(self.input_data[1],
                                      epsilon=test_epsilon,
                                      norm="L2",order=1,
                                      dimensions=-1,
                                      verbose=False)
        
        #Testing residual:
        testing_model.setup()
        x_opt=self.input_data.get_multiparametric_field()
        
        residues=np.ones( self.nx*(self.ny-1) ).flatten()
        ideal_residues=(self.data_2D[:,1:]-self.data_2D[:,:-1]).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #Testing missfit:
        ideal_missfit=test_epsilon*(residues*residues).sum()
        testing_missfit=testing_model.calculate_missfit()

        testing.assert_almost_equal(ideal_missfit,testing_missfit,decimal=12)
                                    
        #Testing Grad:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        
        #Calculating Ideal Gradient:
        ideal_residues=ideal_residues.reshape(self.nx,self.ny-1)
        local_grad=np.zeros((self.nx,self.ny))
        
        local_grad[:,:-1]-=ideal_residues
        local_grad[:,1:]+=ideal_residues
        
        ofset=self.nx
        length=self.nx*self.ny
        
        ideal_grad[ofset:ofset+length]=2.*test_epsilon*local_grad.flatten()
        
        
        #Calculate testing gradient testing:
        grad_func=testing_model.get_gradient_function()
        
        testing_grad[:]=grad_func(x_opt)
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)


    def test_first_derivative_dimension_0(self):
        
        test_epsilon=0.57
        
        testing_model=total_variation(self.input_data[1],
                                      epsilon=test_epsilon,
                                      norm="L2",order=1,
                                      dimensions=0,
                                      verbose=False)
        
        #Testing residual:
        testing_model.setup()
        x_opt=self.input_data.get_multiparametric_field()
        
        residues=np.ones( self.ny*(self.nx-1) ).flatten()
        ideal_residues=(self.data_2D[1:,:]-self.data_2D[:-1,:]).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #Testing missfit:
        ideal_missfit=test_epsilon*(residues*residues).sum()
        testing_missfit=testing_model.calculate_missfit()

        testing.assert_almost_equal(ideal_missfit,testing_missfit,decimal=12)
                                    
        #Testing Grad:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        
        #Calculating Ideal Gradient:
        ideal_residues=ideal_residues.reshape(self.nx-1,self.ny)
        local_grad=np.zeros((self.nx,self.ny))
        
        local_grad[:-1,:]-=ideal_residues
        local_grad[1:,:]+=ideal_residues
        
        ofset=self.nx
        length=self.nx*self.ny
        
        ideal_grad[ofset:ofset+length]=2.*test_epsilon*local_grad.flatten()
        
        
        #Calculate testing gradient testing:
        grad_func=testing_model.get_gradient_function()
        
        testing_grad[:]=grad_func(x_opt)
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)


if __name__ == "__main__":
    unittest.main()
