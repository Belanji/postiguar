import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import convolution_ln_impedance
from numpy import testing


class TestConvolutionLnImpedance(unittest.TestCase):


    def setUp(self):

        rng = np.random.default_rng()
        n_wav=51
        data_type=np.float64


        self.impedance_shape=[5,2000]
        self.amplitude_shape=[5,1999]
        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1



        impedance=rng.random(self.impedance_shape,dtype=data_type)+0.001
        wavelet=rng.random(n_wav)

        ln_impedance=np.log(impedance)


        self.impedance_fields=ptg.multiparametric_field()
        self.impedance_fields.add_field(impedance)       
        self.impedance_fields.add_field(ln_impedance)
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(field_shape=self.amplitude_shape)
        self.amplitude_fields.add_field(field_shape=self.amplitude_shape)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        amp_filter=ptg.filters.get_filter("impT2ampT")
        amp_filter.filter_data(self.impedance_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[0],
                               ln_approximation=True,
                               causal_wavelet=True,
                               convolution_mode="same")


        amp_filter.filter_data(self.impedance_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[1],
                               ln_approximation=True,
                               causal_wavelet=False,
                               convolution_mode="same")
        

        amp_filter.filter_data(self.impedance_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[2],
                               ln_approximation=True,
                               causal_wavelet=True,
                               convolution_mode="full")

        
    def test_causal_residues(self):

        testing_model=convolution_ln_impedance(self.impedance_fields[1],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[0],
                                               causal_wavelet=True,
                                               convolution_mode="same")

        testing_model.setup()
        x_opt=self.impedance_fields.get_multiparametric_field()
        
        residues=np.ones(self.amplitude_shape).flatten()
        ideal_residues=np.zeros(self.amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues)
        

    def test_noncausal_residues(self):

        testing_model=convolution_ln_impedance(self.impedance_fields[1],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[1],
                                               causal_wavelet=False,
                                               convolution_mode="same")

        testing_model.setup()
        x_opt=self.impedance_fields.get_multiparametric_field()
        
        
        residues=np.ones(self.amplitude_shape).flatten()
        ideal_residues=np.zeros(self.amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues)
        
        #Test the objective function
        missfit=testing_model.calculate_missfit()    
        testing.assert_almost_equal(missfit,0)
  
       
    def test_full_causal_residues(self):

        testing_model=convolution_ln_impedance(self.impedance_fields[1],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[2],
                                               causal_wavelet=True,
                                               convolution_mode="full")

        testing_model.setup()
        x_opt=self.impedance_fields.get_multiparametric_field()
        
        residues=np.ones(self.full_amplitude_shape).flatten()
        ideal_residues=np.zeros(self.full_amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues)



if __name__ == "__main__":
    unittest.main()
