import unittest
import numpy as np
from postiguar import multiparametric_field
from postiguar.models import Levenberg_Marquardt_Tykhonov
from numpy import testing

class TestLMTRegularizer(unittest.TestCase):
    
    def setUp(self):
        
        
        self.nx=20
        self.ny=8
        self.nz=13
        
        self.data_1D=np.zeros(self.nx)
        self.data_2D=np.zeros(self.nx*self.ny).reshape(self.nx,self.ny)
        self.data_3D=np.zeros(self.nx*self.ny*self.nz).reshape(self.nx,self.ny,self.nz)
        
        self.ref_data_1D=np.zeros(self.nx)
        self.ref_data_2D=np.zeros(self.nx*self.ny).reshape(self.nx,self.ny)
        self.ref_data_3D=np.zeros(self.nx*self.ny*self.nz).reshape(self.nx,self.ny,self.nz)
        
        
        self.weights_data_1D=np.zeros(self.nx)
        self.weights_data_2D=np.zeros(self.nx*self.ny).reshape(self.nx,self.ny)
        self.weights_data_3D=np.zeros(self.nx*self.ny*self.nz).reshape(self.nx,self.ny,self.nz)
        
        for ii in range(self.nx):
            
            self.data_1D[ii]=np.cos(np.pi*float(ii)/self.nx)
            self.ref_data_1D[ii]=np.sin(np.pi*float(ii)/self.nx)
            self.weights_data_1D[ii]=np.sin(np.pi*float(ii)/self.nx)**2

            for jj in range(self.ny):
            
                self.data_2D[ii,jj]=self.data_1D[ii]+(float(jj)/self.ny)**2    
                self.ref_data_2D[ii,jj]=self.ref_data_1D[ii]-(float(jj)/self.ny)**4 
                self.weights_data_2D[ii,jj]=self.weights_data_1D[ii]*( 1-(float(jj-1)/self.ny)**4 ) 
            
            
                for kk in range(self.nz):

                    self.data_3D[ii,jj,kk]=self.ref_data_2D[ii,jj]*np.exp(-float(kk)/self.nz)                
                    
                    self.ref_data_3D[ii,jj,kk]=self.ref_data_2D[ii,jj]*np.exp(-float(kk)/self.nz)            
                    self.weights_data_3D[ii,jj,kk]=self.weights_data_2D[ii,jj]*np.exp(-float(kk)/self.nz)  
                    
        self.input_data=multiparametric_field()
        self.input_data.add_field(self.data_1D)
        self.input_data.add_field(self.data_2D)
        self.input_data.add_field(self.data_3D)
 
    
        self.ref_data=multiparametric_field()
        self.ref_data.add_field(self.ref_data_1D)
        self.ref_data.add_field(self.ref_data_2D)
        self.ref_data.add_field(self.ref_data_3D)
        
        self.weights=multiparametric_field()
        self.weights.add_field(self.weights_data_1D)
        self.weights.add_field(self.weights_data_2D)
        self.weights.add_field(self.weights_data_3D)
        
            
 
    def test_lmt_2D(self):
        
        test_epsilon=0.36
        testing_model=Levenberg_Marquardt_Tykhonov(self.input_data[1],
                                                   self.ref_data[1],
                                                   self.weights[1],
                                                   epsilon=test_epsilon) 
        
        
        testing_model.setup()
        
        offset=self.nx
        length=self.nx*self.ny
        
        x_opt=self.input_data.get_multiparametric_field()
        ref_data=self.ref_data.get_multiparametric_field()
        weights=self.weights.get_multiparametric_field()
        
        residues=np.ones(self.nx*self.ny).flatten()
        
        
        #Calculate ideal residues:
        
        ideal_residues=weights[offset:offset+length]*( ref_data[offset:offset+length] - x_opt[offset:offset+length])
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #teting objective function:
        objctive_function=testing_model.get_objective_function()
            
        ideal_objective=test_epsilon*(ideal_residues*ideal_residues).sum()
        testing_objective=objctive_function(x_opt)
        
        testing.assert_almost_equal(ideal_objective,testing_objective)
        
        #Testing calculate_missfit:
        missfit=testing_model.calculate_missfit()
        testing.assert_almost_equal(ideal_objective,missfit)


        #calculate Gradient:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        grad_func=testing_model.get_gradient_function()
        
        
        ideal_grad[offset:offset+length]=-2.*test_epsilon*weights[offset:offset+length]*( ref_data[offset:offset+length] - x_opt[offset:offset+length])
        testing_grad[:]=grad_func(x_opt)
        
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)


    def test_lmt_2D_no_weigth(self):
        
        test_epsilon=0.36
        testing_model=Levenberg_Marquardt_Tykhonov(self.input_data[1],
                                                   self.ref_data[1],
                                                   epsilon=test_epsilon) 
        
        
        testing_model.setup()
        
        offset=self.nx
        length=self.nx*self.ny
        
        x_opt=self.input_data.get_multiparametric_field()
        ref_data=self.ref_data.get_multiparametric_field()
        
        residues=np.ones(self.nx*self.ny).flatten()
        
        
        #Calculate ideal residues:
        
        ideal_residues=( ref_data[offset:offset+length] - x_opt[offset:offset+length])
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues,atol=1e-14,rtol=1e-14)
        
        #teting objective function:
        objctive_function=testing_model.get_objective_function()
            
        ideal_objective=test_epsilon*(ideal_residues*ideal_residues).sum()
        testing_objective=objctive_function(x_opt)
        
        testing.assert_almost_equal(ideal_objective,testing_objective)
        
        #Testing calculate_missfit:
        missfit=testing_model.calculate_missfit()
        testing.assert_almost_equal(ideal_objective,missfit)


        #calculate Gradient:
        x_opt=self.input_data.get_multiparametric_field()
        
        testing_grad=np.zeros(x_opt.shape)
        ideal_grad=np.zeros(x_opt.shape)
        
        grad_func=testing_model.get_gradient_function()
        
        
        ideal_grad[offset:offset+length]=-2.*test_epsilon*( ref_data[offset:offset+length] - x_opt[offset:offset+length])
        testing_grad[:]=grad_func(x_opt)
        
        
        testing.assert_allclose(testing_grad,ideal_grad,atol=1e-14,rtol=1e-14)


        
if __name__ == "__main__":
    unittest.main()

 