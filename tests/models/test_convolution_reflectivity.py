import unittest
import numpy as np
import postiguar as ptg
from postiguar.models import convolution_reflectivity
from numpy import testing
from scipy.linalg import toeplitz


class TestConvolutionReflectivity(unittest.TestCase):

    def setUp(self):

        rng = np.random.default_rng()
        n_wav=51
        data_type=np.float64


        self.reflectivity_shape=(5,2000)
        self.amplitude_shape=self.reflectivity_shape

        self.full_amplitude_shape=list(self.amplitude_shape)
        self.full_amplitude_shape[-1]=self.amplitude_shape[-1]+n_wav-1



        reflectivity=rng.random(self.reflectivity_shape,dtype=data_type)
        wavelet=rng.random(n_wav)


        self.reflectivity_fields=ptg.multiparametric_field()
        self.reflectivity_fields.add_field(reflectivity)
        self.reflectivity_fields.add_field(field_shape=self.reflectivity_shape)        
        
        
        
        self.wavelet_fields=ptg.multiparametric_field()
        self.wavelet_fields.add_field(wavelet)

        
        self.amplitude_fields=ptg.multiparametric_field()
        self.amplitude_fields.add_field(reflectivity)
        self.amplitude_fields.add_field(reflectivity)
        self.amplitude_fields.add_field(field_shape=self.full_amplitude_shape)
        

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[0],
                               causal_wavelet=True,
                               convolution_mode="same")

        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[1],
                               causal_wavelet=False,
                               convolution_mode="same")
        
        ref_filter=ptg.filters.get_filter("refT2ampT")
        ref_filter.filter_data(self.reflectivity_fields[0],
                               self.wavelet_fields[0],
                               self.amplitude_fields[2],
                               causal_wavelet=True,
                               convolution_mode="full")



        
    def test_causal_residues(self):

        testing_model=convolution_reflectivity(self.reflectivity_fields[0],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[0],
                                               causal_wavelet=True,
                                               convolution_mode="same")

        testing_model.setup()
        x_opt=self.reflectivity_fields.get_field(0,flatten=True)
        
        residues=np.ones(self.amplitude_shape).flatten()
        ideal_residues=np.zeros(self.amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)       
        testing.assert_allclose(residues,ideal_residues)
        
        #Testing objective function:
            
        ideal_missfit=(residues*residues).sum()
        testing_missfit=testing_model.calculate_missfit()
        
        testing.assert_almost_equal(ideal_missfit,testing_missfit)

    def test_noncausal_residues(self):

        testing_model=convolution_reflectivity(self.reflectivity_fields[0],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[1],
                                               causal_wavelet=False,
                                               convolution_mode="same")

        testing_model.setup()
        x_opt=self.reflectivity_fields.get_field(0,flatten=True)
        
        residues=np.ones(self.amplitude_shape).flatten()
        ideal_residues=np.zeros(self.amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)        
        testing.assert_allclose(residues,ideal_residues)
        
    def test_full_causal_residues(self):

        testing_model=convolution_reflectivity(self.reflectivity_fields[0],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[2],
                                               causal_wavelet=True,
                                               convolution_mode="full")

        testing_model.setup()
        x_opt=self.reflectivity_fields.get_field(0,flatten=True)
        
        residues=np.ones(self.full_amplitude_shape).flatten()
        ideal_residues=np.zeros(self.full_amplitude_shape).flatten()
        
        testing_model.calculate_residues(x_opt,residues)        
        testing.assert_allclose(residues,ideal_residues)

    def test_full_causal_gradient(self):

        
        
        testing_model=convolution_reflectivity(self.reflectivity_fields[1],
                                               self.wavelet_fields[0],
                                               self.amplitude_fields[2],
                                               causal_wavelet=True,
                                               convolution_mode="full")

        testing_model.setup()
        
        x_opt=self.reflectivity_fields.get_multiparametric_field()
        ideal_residues=-2*self.amplitude_fields.get_field(2,flatten=False)
        wavelet=self.wavelet_fields.get_field(0)

        relfectivity_offset=self.reflectivity_fields.get_field_offset(1)
        n_wav=wavelet.shape[-1]
        
        
        toeplitz_c=np.zeros(self.full_amplitude_shape[-1])
        toeplitz_c[:n_wav]=wavelet[:n_wav]
        toeplitz_r=np.zeros(self.reflectivity_shape[-1])
        
        
        toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        
        
        grad_func=testing_model.get_gradient_function()
        
        
        testing_grad=grad_func(x_opt)
        ideal_grad=np.matmul(ideal_residues,toeplitz_matrix)
        
                
        testing.assert_allclose(ideal_grad.flatten(),testing_grad[relfectivity_offset:])


if __name__ == "__main__":
    unittest.main()
