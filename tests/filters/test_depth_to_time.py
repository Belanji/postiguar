#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 14:20:47 2021

@author: souzarf
"""

#Import testing framework:
import unittest
import numpy as np
from numpy import testing


from postiguar.filters import depth_to_time


class TestDepthToTime(unittest.TestCase):

    def setUp(self):
        
        self.max_depth=3
        self.max_offset=3
        self.reference_time=12
        self.velocity_depth=np.zeros((self.max_offset,self.max_depth))
        self.reference_time=0
        
        
        self.dz=10
        
        for ii in range(0,self.max_depth):
            self.velocity_depth[:,ii]=[2.5*(ii+1),5*(ii+1),10*(ii+1)]
            
        
    def test_point_depth_position_to_time_position(self):
        
        testing_time_position=depth_to_time._calculate_time_positions(self.velocity_depth,self.dz,self.reference_time)
       
        theoretical_time=self.velocity_depth.copy()
        theoretical_time[...,0]=self.reference_time
        
        theoretical_time[...,1]=[8,4,2]
        theoretical_time[...,2]=[12,6,3]
        
        testing.assert_allclose(testing_time_position,theoretical_time)

if __name__ == "__main__":
    unittest.main()
        
                                 