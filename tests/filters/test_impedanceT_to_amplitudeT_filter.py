#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 19:58:43 2021

@author: souzarf
"""

import unittest
import numpy as np
from scipy.linalg import toeplitz
from numpy import testing


from postiguar.filters import impedanceT_to_reflectivityT
from postiguar.filters import impedanceT_to_amplitudeT
from postiguar import multiparametric_field

rng = np.random.default_rng()
n_wav=51
data_type=np.float64


impedance_shape=[5,2001]
reflectivity_shape=[5,2000]
amplitude_shape=reflectivity_shape

full_amplitude_shape=list(amplitude_shape)
full_amplitude_shape[-1]=amplitude_shape[-1]+n_wav-1


impedance=rng.random(impedance_shape,dtype=data_type)
wavelet=rng.random(n_wav)





class TestImpedanceTToAmplitudeT(unittest.TestCase):

    def setUp(self):


        self.test_field=multiparametric_field()
        
        self.test_field.add_field(impedance)
        self.test_field.add_field(field_shape=reflectivity_shape)
        self.test_field.add_field(wavelet)
        self.test_field.add_field(field_shape=amplitude_shape)        
        self.test_field.add_field(field_shape=full_amplitude_shape)
        
        impedanceT_to_reflectivityT.filter_data((self.test_field,0),
                                                  (self.test_field,1)) 

    def test_transformation_with_noncausal_wavelet(self):

    
        reflectivity=self.test_field.get_field(1,flatten=False)

        #breakpoint()
        #Create the toeplitz matrix
        toeplitz_c=np.zeros(reflectivity_shape[-1])
        toeplitz_c[0:n_wav//2+1]=wavelet[n_wav//2:n_wav]

        rows=wavelet[0:n_wav//2+1]
        rows=np.flip(rows)
        
        toeplitz_r=np.zeros(reflectivity_shape[-1])
        toeplitz_r[0:n_wav//2+1]=rows[:]
        toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        
        ideal_data=np.matmul(toeplitz_matrix,reflectivity.T).T
        

        
        
        impedanceT_to_amplitudeT.filter_data(self.test_field[0],
                                             self.test_field[2],
                                             self.test_field[3],
                                             causal_wavelet=False,
                                             convolution_mode="same")

        filtered_data=self.test_field.get_field(3,flatten=False)
        

        
        testing.assert_allclose(filtered_data,ideal_data)
                    
    def test_transformation_with_causal_wavelet(self):

        reflectivity=self.test_field.get_field(1,flatten=False)

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(reflectivity_shape[-1])
        toeplitz_c[0:n_wav]=wavelet[0:n_wav]
        
        toeplitz_r=np.zeros(reflectivity_shape[-1])
        toeplitz_r[0]=wavelet[0]
        
        toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        #Calculate ideal data for comparison
        ideal_data=np.matmul(toeplitz_matrix,reflectivity.T).T
                
        #Apply the filter
        impedanceT_to_amplitudeT.filter_data(self.test_field[0],
                                             self.test_field[2],
                                             self.test_field[3],
                                             causal_wavelet=True,
                                             convolution_mode="same")

        filtered_data=self.test_field.get_field(3,flatten=False)
        testing.assert_allclose(ideal_data,filtered_data)
        
        
    def test_full_transformation_with_causal_wavelet(self):

        reflectivity=self.test_field.get_field(1,flatten=False)

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(full_amplitude_shape[-1])
        toeplitz_c[0:n_wav]=wavelet[0:n_wav]
        
        toeplitz_r=np.zeros(reflectivity_shape[-1])
        toeplitz_r[0]=wavelet[0]
        
        toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        #Calculate ideal data for comparison
        ideal_data=np.matmul(toeplitz_matrix,reflectivity.T).T
                
        #Apply the filter
        impedanceT_to_amplitudeT.filter_data(self.test_field[0],
                                             self.test_field[2],
                                             self.test_field[4],
                                             causal_wavelet=True,
                                             convolution_mode="full")

        filtered_data=self.test_field.get_field(4,flatten=False)
        testing.assert_allclose(ideal_data,filtered_data)

                            
if __name__ == "__main__":
    unittest.main()

    
