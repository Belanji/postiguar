#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 19:58:43 2021

@author: souzarf
"""

import unittest
import numpy as np
from scipy.linalg import toeplitz
from numpy import testing


from postiguar.filters import reflectivityT_to_amplitudeT
from postiguar import multiparametric_field

rng = np.random.default_rng()
n_wav=51
data_type=np.float64


data_shape=[5,2000]

full_data_shape=list(data_shape)
full_data_shape[-1]=data_shape[-1]+n_wav-1


test_data=rng.random(data_shape,dtype=data_type)
wavelet=rng.random(n_wav)

full_test_data=rng.random(full_data_shape)

class TestReflectivityTToAmplitudeT(unittest.TestCase):

    def setUp(self):
        
        self.test_field=multiparametric_field()
        
        self.test_field.add_field(test_data)
        self.test_field.add_field(wavelet)
        self.test_field.add_field(test_data)        
        self.test_field.add_field(full_test_data)
        

    def test_transformation_with_noncausal_wavelet(self):

        
        #Zero the amplituted to avoid subtle bugs
        filtered_data=self.test_field.get_field(2,flatten=False)
        filtered_data[...]=0


        #breakpoint()
        #Create the toeplitz matrix
        toeplitz_c=np.zeros(data_shape[1])
        toeplitz_c[0:n_wav//2+1]=wavelet[n_wav//2:n_wav]

        rows=wavelet[0:n_wav//2+1]
        rows=np.flip(rows)
        
        toeplitz_r=np.zeros(data_shape[1])
        toeplitz_r[0:n_wav//2+1]=rows[:]
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T
        

        
        
        reflectivityT_to_amplitudeT().filter_data(self.test_field[0],
                                                  self.test_field[1],
                                                  self.test_field[2],
                                                  causal_wavelet=False,
                                                  convolution_mode="same")

        filtered_data=self.test_field.get_field(2,flatten=False)
        

        
        testing.assert_allclose(filtered_data,ideal_data)
                    
    def test_transformation_with_causal_wavelet(self):

    
        #Zero the amplituted to avoid subtle bugs
        filtered_data=self.test_field.get_field(2,flatten=False)
        filtered_data[...]=0

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(data_shape[1])
        toeplitz_c[0:n_wav]=wavelet[0:n_wav]        
        toeplitz_r=np.zeros(data_shape[1])
        
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        #Calculate ideal data for comparison
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T
                
        #Apply the filter
        reflectivityT_to_amplitudeT().filter_data(self.test_field[0],
                                                  self.test_field[1],
                                                  self.test_field[2],
                                                  causal_wavelet=True, 
                                                  convolution_mode="same")

        filtered_data=self.test_field.get_field(2,flatten=False)

        testing.assert_allclose(ideal_data,filtered_data)
        
        
    def test_full_transformation_with_causal_wavelet(self):

    
        #Zero the amplituted to avoid subtle bugs
        filtered_data=self.test_field.get_field(3,flatten=False)
        filtered_data[...]=0

        #Create the toeplitz matrix
        toeplitz_c=np.zeros(full_data_shape[-1])
        toeplitz_c[0:n_wav]=wavelet[0:n_wav]        
        toeplitz_r=np.zeros(data_shape[-1])
        
        self.toeplitz_matrix=toeplitz(toeplitz_c,toeplitz_r)
        
        #Calculate ideal data for comparison
        ideal_data=np.matmul(self.toeplitz_matrix,test_data.T).T
                
        #Apply the filter
        reflectivityT_to_amplitudeT().filter_data(self.test_field[0],
                                                  self.test_field[1],
                                                  self.test_field[3],
                                                  causal_wavelet=True,
                                                  convolution_mode="full")

        filtered_data=self.test_field.get_field(3,flatten=False)
        testing.assert_allclose(ideal_data,filtered_data)

                            
if __name__ == "__main__":
    unittest.main()

    
