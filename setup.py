#!/usr/bin/env python

"""setup file taken from:
   https://raw.githubusercontent.com/simpeg/discretize/master/setup.py

Thank you very much for the help.

"""

import os
import sys


CLASSIFIERS = [
    "Development Status :: internal",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python",
    "Topic :: Scientific/Engineering",
    "Topic :: Scientific/Engineering :: Mathematics",
    "Topic :: Scientific/Engineering :: Physics",
    "Topic :: Scientific/Engineering :: Geophysics",
    "Operating System :: Unix",
]


def configuration(parent_package="", top_path=None):
    from numpy.distutils.misc_util import Configuration

    config = Configuration(None, parent_package, top_path)
    config.set_options(
        ignore_setup_xxx_py=True,
        assume_default_configuration=True,
        delegate_options_to_subpackages=True,
        quiet=True,
    )

    #config.add_subpackage("postiguar")

    return config


#with open("README.rst") as f:
#    LONG_DESCRIPTION = "".join(f.readlines())

build_requires = [
    "numpy>=1.18"
    "numba"
]

install_requires = build_requires + [
    "scipy>=0.13"
]

metadata = dict(
    name="postiguar",
    version="0.4.2",
    python_requires=">=3.6",
    setup_requires=build_requires,
    install_requires=install_requires,
    author="Renato Ferreira de Souza",
    author_email="rfs1011@gmail.com",
    description="Post Stack Inversion(Guaranteed) package",
#    long_description=LONG_DESCRIPTION,
    license="MIT",
    keywords="inversion",
    download_url="https://gitlab.com/Belanji/postiguar",
    classifiers=CLASSIFIERS,
    platforms=["Windows", "Linux", "Solaris", "Mac OS-X", "Unix"],
    packages=setuptools.find_packages(),
)

if len(sys.argv) >= 2 and (
    "--help" in sys.argv[1:]
    or sys.argv[1] in ("--help-commands", "egg_info", "--version", "clean")
):
    # For these actions, NumPy is not required.
    #
    # They are required to succeed without Numpy, for example when
    # pip is used to install discretize when Numpy is not yet present in
    # the system.
    try:
        from setuptools import setup
    except ImportError:
        from distutils.core import setup
else:
    if (len(sys.argv) >= 2 and sys.argv[1] in ("bdist_wheel", "bdist_egg")) or (
        "develop" in sys.argv
    ):
        # bdist_wheel/bdist_egg needs setuptools
        import setuptools

    from numpy.distutils.core import setup

    # Add the configuration to the setup dict when building
    # after numpy is installed
    metadata["configuration"] = configuration


setup(**metadata)
