import numpy as np

from .model_base import inversion_model, register_model
from ..operators.convolution import create_convolution_function, create_convolution_transposed_function
from postiguar.operators.derivatives import derivative_operator



@register_model
class convolution_ln_impedance(inversion_model):
    """Class implementing the stationary Robinson convolution model.    
    """

    _number_of_fiedls=3
    has_grad_function_factory=True
            

    model_names=["convolution_ln_impedance","convlnimp"]
    short_description="Classical impedance acoustic post-stack inversion."

    
    def __init__(self,ln_impedance,
                 wavelet,
                 amplitude,
                 epsilon=1.,
                 norm="L2",
                 optimize_fields=(True,False,False),
                 causal_wavelet=True,
                 convolution_mode="full",
                 **kwargs):

        #Unpack the required field position pairs:
        super().__init__([ln_impedance,wavelet,amplitude],
                       optimize_fields,
                       norm,
                       epsilon)
        
        self.kwargs=kwargs

        #Get the function minimization norm:
        self.optimize_fields=optimize_fields

        self.causal_wavelet=causal_wavelet
        self.convolution_mode=convolution_mode
    
    
        self.multiparametric_size=self._working_multiparametric_field.get_total_size()

        
    def setup(self,**kwargs):


        print('Initializing stationary Robinson convolution-model:\n')
        print(f'epsilon(scaling):      {self.epsilon}')
        print(f'Residues norm:         {self.norm_name}\n')


        #Set up the information necessary 
        self.ln_impedance_shape=self._field_shapes[0]
        self.wavelet_shape=self._field_shapes[1]
        self.amplitude_shape=self._field_shapes[2]

        #Calculate the reflectivity shape:
        self.reflectivity_shape=list(self.ln_impedance_shape)
        self.reflectivity_shape[-1]=self.ln_impedance_shape[-1]-1


        
        #Allocate memory to be reused in residues calculation:
        self.residues=np.zeros(self.amplitude_shape).ravel()
        self.grad=np.zeros(self.multiparametric_size)
        self.reflectivity=np.zeros(self.reflectivity_shape).ravel()

        
        #Create convolution operator and its derivative:
        self.convolve=create_convolution_function(self.wavelet_shape,
                                                  self.reflectivity_shape,
                                                  self.amplitude_shape,
                                                  causal_wavelet=self.causal_wavelet,
                                                  convolution_mode=self.convolution_mode)        

        self.field_times_grad_convolve=create_convolution_transposed_function(self.amplitude_shape,
                                                                              self.wavelet_shape,
                                                                              self.reflectivity_shape,
                                                                              causal_wavelet=self.causal_wavelet,
                                                                              convolution_mode=self.convolution_mode)        

        #Create functions to calculate redidues and grad_residues
        self.first_derivate=derivative_operator(self.ln_impedance_shape,
                                                        dimensions=-1,
                                                        order=1)
        self.calculate_residues=self.create_residues_function()
        self.jacobian_dot_vec=self.create_jacobian_dot_vec()
        
        return self

    def create_residues_function(self):

        convolve=self.convolve
        first_derivate=self.first_derivate
        
        optimize_ln_impedance=self._optimize_fields[0]
        optimize_wavelet=self._optimize_fields[1]
        optimize_amplitude=self._optimize_fields[2]
    
        

        ln_impedance=self._fields[0]
        wavelet=self._fields[1]
        amplitude=self._fields[2]
        reflectivity=self.reflectivity
        
        
        #Set up a constant ln_impedance or its possition in the multiparametric field:
        

        imp_offset=self._field_offsets[0]
        imp_length=self._field_lengths[0]
            
        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        wav_offset=self._field_offsets[1]
        wav_length=self._field_lengths[1]


        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        amp_offset=self._field_offsets[2]
        amp_length=self._field_lengths[2]
                
        
        def calculate_residues(x_opt,residues):
            nonlocal wavelet, amplitude, ln_impedance,reflectivity

            
            if optimize_wavelet:
                wavelet=x_opt[wav_offset:wav_offset+wav_length]

            if optimize_ln_impedance:
                ln_impedance=x_opt[imp_offset:imp_offset+imp_length]

            if optimize_amplitude:
                amplitude=x_opt[amp_offset:amp_offset+amp_length]


            first_derivate.left_apply(ln_impedance,reflectivity)
            reflectivity*=0.5
            
            #Calculate the residues:
            convolve(wavelet,reflectivity,residues)
            residues-=amplitude
            
            return  residues

        return calculate_residues

    def create_jacobian_dot_vec(self):
        """Create the function which merform the matrix multiplication
        of the Jacobian and a vector.
        """

        field_times_grad_convolution=self.field_times_grad_convolve
        first_derivate=self.first_derivate
        
        
        optimize_ln_impedance=self._optimize_fields[0]
        optimize_wavelet=self._optimize_fields[1]
        optimize_amplitude=self._optimize_fields[2]



        ln_impedance=self._fields[0]
        wavelet=self._fields[1]
        amplitude=self._fields[2]
        reflectivity=self.reflectivity
        
        
        #Set up a constant ln_impedance or its possition in the multiparametric field:
        

        imp_offset=self._field_offsets[0]
        imp_length=self._field_lengths[0]
            
        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        wav_offset=self._field_offsets[1]
        wav_length=self._field_lengths[1]


        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        amp_offset=self._field_offsets[1]
        amp_length=self._field_lengths[1]
 
  
        def jacobian_dot_vec(x_opt,residues,output):
            nonlocal amplitude, ln_impedance, wavelet

            if optimize_wavelet:
                wavelet[:]=x_opt[wav_offset:wav_offset+wav_length]
                
                
            if optimize_ln_impedance:
                ln_impedance[:]=x_opt[imp_offset:imp_offset+imp_length]

            if optimize_amplitude:
                amplitude[:]=x_opt[amp_offset:amp_offset+amp_length]

            if optimize_wavelet:
                output_wav=output[wav_offset:wav_offset+wav_length]
                field_times_grad_convolution(residues,ln_impedance,output_wav)
                
            if optimize_ln_impedance:
                grad_ref=output[imp_offset:imp_offset+imp_length]
                field_times_grad_convolution(residues,wavelet,reflectivity)               
                first_derivate.right_apply(reflectivity,grad_ref)
                    
            if optimize_amplitude:
                grad_amp=output[amp_offset:amp_offset+amp_length]
                grad_amp[:]=-residues[:]


            return output


        return jacobian_dot_vec

        



    
