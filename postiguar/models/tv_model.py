import numpy as np
import scipy.sparse as scp

from .model_base import inversion_model, register_model
from ..utilities.function_blocks import unpack_mp_field_entry
from ..utilities.norms import get_norm, get_gradient
from ..operators.derivatives import derivative_operator

@register_model
class total_variation(inversion_model):
    """Class implementing the total variation regularizer with fractional
    derivatives. Currently, it only works with a single field. so please, use 
    different models if you want to regularize different fields.
    """
    is_linear=True
    has_grad_function_factory=True
            

    model_names=["total_variation","TV","Total Variation"]
    short_description="Implement the total variation regulariation method."

    
    def __init__(self,input_fields,epsilon=1.,norm="L2",
                 order=1,dimensions=-1,verbose=True, **kwargs):


        super().__init__([input_fields],
                       optimize_fields=(True,),
                       norm=norm,
                       epsilon=epsilon)

        #Create derivative dimensions list:
        try:
            self.dimensions=list(dimensions)
        except TypeError:
            self.dimensions=[dimensions]

        try:
            self.order=tuple(order)
        except TypeError:
            self.order=(order,)
    

        self.kwargs=kwargs
        self.verbose=verbose
        
    def setup(self):

        if self.verbose:

            print('Initializing Tv-regulalizer:\n')
            print(f'epsilon(scaling):      {self.epsilon}')
            print(f'Derivative Order:      {self.order}')
            print(f'Derivative Dimensions: {self.dimensions}')
            print(f'Residues norm:         {self.norm_name}\n')


        if self.order==(0,):

            self.dimensions=[-1]
        
        self.multiparametric_size=self._working_multiparametric_field.get_total_size()

        #Create the matrix to perform the derivative:
        self.derivate_operator=derivative_operator(self._field_shapes[0],
                                                   dimensions=self.dimensions,
                                                   order=self.order)

        #Allocate memory to be reused in residues calculation:
        residues_size, _=self.derivate_operator.shape
        
        self.grad=np.zeros(self.multiparametric_size)
        self.residues=np.zeros(residues_size)

        
        #Create functions to calculate redidues and grad_residues
        self.calculate_residues=self.create_residues_function()
        self.jacobian_dot_vec=self.create_jacobian_dot_vec()

        return self
        
    def create_residues_function(self):


        of_offset=self._field_offsets[0]
        of_length=self._field_lengths[0]
        derivate_operator=self.derivate_operator

        def calculate_residues(x_opt,residues):

            opt_field=x_opt[of_offset:of_offset+of_length]
            derivate_operator.left_apply(opt_field,residues)
            return  residues
        
        return calculate_residues


    def create_jacobian_dot_vec(self):
        """ Construct and return the gradient function.

        Parameters:
        ----------

             
            Returns:
            -------

             grad_func: func(x0,*args,**kwargs)
               Function which calculates the gradient of x0.

        """
        
        of_offset=self._field_offsets[0]
        of_length=self._field_lengths[0]
        derivate_operator=self.derivate_operator

    
        def jacobian_dot_vec(x_opt,vector,output): 
            
            grad_opt=output[of_offset:of_offset+of_length]
            derivate_operator.right_apply(vector,grad_opt)
            
            
            return output


        return jacobian_dot_vec


        
