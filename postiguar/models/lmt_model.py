import numpy as np
from .model_base import inversion_model, register_model
from postiguar.utilities.function_blocks import unpack_mp_field_entry
from ..utilities.norms import get_norm, get_gradient
from postiguar.base.multiparametric_field import multiparametric_field

@register_model
class Levenberg_Marquardt_Tykhonov(inversion_model):
    """Class implementing LMT-regularizer.    
    """
    
    has_grad_function_factory=True
            

    model_names=["Levenberg_Marquardt_Tykhonov","LMT-regularizer","LMT","lmt"]
    short_description="Implement the LMT(Levenberg_Marquardt_Tykhonov) regulariation method."

    
    def __init__(self,input_field, target_field,weigth_field=None,
                 norm="L2",epsilon=1.,optimize_fields=(True,False,False),**kwargs):


        #Unpack the required field position pairs:


        if weigth_field is not None:
            
            super().__init__([input_field,
                              target_field,
                              weigth_field],
                             optimize_fields,
                             norm,
                             epsilon)
        

        else:

            #Create temporay multiparatric field for weight:
            
            weigth_field=np.ones(input_field.shape)

            super().__init__([input_field,
                             target_field,
                             weigth_field],
                             optimize_fields,
                             norm,
                             epsilon)

        self.kwargs=kwargs


        self.optimize_fields=tuple(optimize_fields)
        
        
        if self._field_lengths[0] != self._field_lengths[1] or \
           self._field_lengths[1] != self._field_lengths[2] :
            raise ValueError("Optimize and target data with different shapes")

        

    def setup(self):
        


        self.multiparametric_size=self._working_multiparametric_field.get_total_size()
        
        self.optimize_opt=self.optimize_fields[0]
        self.optimize_tar=self.optimize_fields[1]
        self.optimize_eps=self.optimize_fields[2]
              
        self.residues=np.zeros(self._field_lengths[0])
        self.grad=np.zeros(self.multiparametric_size)
        
        #Create functions to calculate redidues and grad_residues
        self.calculate_residues=self.create_residues_function()
        self.jacobian_dot_vec=self.create_jacobian_dot_vec()

        return self
    
    def create_residues_function(self,**kwargs):
        """ Construct and return the objective function.

        Parameters:
        ----------

        Returns:
        -------

            obj_func: func(x0,*args,**kwargs)
              Objective function to be minimised.

        """

        
        optimize_opt=self.optimize_opt
        optimize_tar=self.optimize_tar
        optimize_eps=self.optimize_eps
        

            
        opt_offset=self._field_offsets[0]
        opt_length=self._field_lengths[0]
            
        tar_offset=self._field_offsets[1]
        tar_length=self._field_lengths[1]


        eps_offset=self._field_offsets[2]
        eps_length=self._field_lengths[2]



        x_opt=self._fields[0]
        x_tar=self._fields[1]
        weigth=self._fields[2]

        
        def calculate_residues(x0,residues):
            nonlocal x_opt, x_tar, weigth

            if optimize_opt:
                x_opt=x0[opt_offset:opt_offset+opt_length]

            if optimize_tar:                
                x_tar=x0[tar_offset:tar_offset+tar_length]
                
            if optimize_eps:
                weigth=x0[eps_offset:eps_offset+eps_length]

            
            
            residues[:]=weigth*(x_tar-x_opt)

            return residues


        return calculate_residues


    def create_jacobian_dot_vec(self,**kwargs):
        """ Construct and return the gradient function.

        Parameters:
        ----------

             
        Returns:
        -------

            grad_func: func(x0,*args,**kwargs)
               Function which calculates the gradient of x0.

        """

        
        
        optimize_opt=self.optimize_opt
        optimize_tar=self.optimize_tar
        optimize_eps=self.optimize_eps
        

            
        opt_offset=self._field_offsets[0]
        opt_length=self._field_lengths[0]
            
        tar_offset=self._field_offsets[1]
        tar_length=self._field_lengths[1]


        w_offset=self._field_offsets[2]
        w_length=self._field_lengths[2]



        x_opt=self._fields[0]
        x_tar=self._fields[1]
        weigth=self._fields[2]
        
        residues=self.residues
        grad=self.grad
  
        def jacobian_dot_vec(x0,vector,output):
            nonlocal grad, residues, x_opt, x_tar, weigth
        
            if optimize_opt:
                x_opt=x0[opt_offset:tar_offset+opt_length]
                grad_opt=output[opt_offset:opt_offset+opt_length]
                grad_opt[:]=-vector[:]
                
            if optimize_tar:
                x_tar=x0[tar_offset:tar_offset+tar_length]
                grad_tar=output[tar_offset:tar_offset+tar_length]
                grad_tar[:]=vector[:]
                
            if optimize_eps:

                grad_weigth=output[w_offset:w_offset+w_length]
                grad_weigth[:]=grad_tar-grad_opt[:]

                
            return output


        return jacobian_dot_vec



    
