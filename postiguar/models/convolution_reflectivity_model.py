import numpy as np

from .model_base import inversion_model, register_model
from ..operators.convolution import create_convolution_function, create_convolution_transposed_function

@register_model
class convolution_reflectivity(inversion_model):
    """Class implementing the stationary Robinson convolution model.    
    """


    has_grad_function_factory=True
            

    model_names=["convolution_reflectivity","convref"]
    short_description=" Model the objective function as the sum of the squared residuals with the forward operator applied on the reflectivity."

    
    def __init__(self,reflectivity,wavelet,amplitude,epsilon=1., norm="L2",optimize_fields=(True,False,False),causal_wavelet=True,convolution_mode="full",  **kwargs):

        

        #Unpack the required field position pairs:
        super().__init__([reflectivity,wavelet,amplitude],
                       optimize_fields,
                       norm,
                       epsilon)
        
        self.kwargs=kwargs
        self.optimize_fields=optimize_fields


        self.causal_wavelet=causal_wavelet
        self.convolution_mode=convolution_mode
        
    def setup(self,**kwargs):


        #Set up the information necessary 
        self.reflectivity_shape=self._field_shapes[0]
        self.wavelet_shape=self._field_shapes[1]
        self.amplitude_shape=self._field_shapes[2]

        self.multiparametric_size=self._working_multiparametric_field.get_total_size()

        self.optimize_reflectivity=self.optimize_fields[0]
        self.optimize_wavelet=self.optimize_fields[1]
        self.optimize_amplitude=self.optimize_fields[2]


        print('Initializing stationary Robinson convolution-model:\n')
        print(f'epsilon(scaling):      {self.epsilon}')
        print(f'Residues norm:         {self.norm_name}\n')



        #Allocate memory to be reused in residues calculation:
        self.residues=np.zeros(self.amplitude_shape).ravel()
        self.grad=np.zeros(self.multiparametric_size)
        
        #Create convolution operator and its derivative:
        self.convolve=create_convolution_function(self.wavelet_shape,
                                                  self.reflectivity_shape,
                                                  self.amplitude_shape,
                                                  causal_wavelet=self.causal_wavelet,
                                                  convolution_mode=self.convolution_mode)        

        self.field_times_grad_convolve=create_convolution_transposed_function(self.amplitude_shape,
                                                                              self.wavelet_shape,
                                                                              self.reflectivity_shape,
                                                                              causal_wavelet=self.causal_wavelet,
                                                                              convolution_mode=self.convolution_mode)        

        #Create functions to calculate redidues and grad_residues
        self.calculate_residues=self.create_residues_function()
        self.jacobian_dot_vec=self.create_jacobian_dot_vec()
        
        
        return self

    def create_residues_function(self):

        convolve=self.convolve

        optimize_wavelet=self.optimize_wavelet
        optimize_reflectivity=self.optimize_reflectivity
        optimize_amplitude=self.optimize_amplitude
    
        
        reflectivity=self._fields[0]
        wavelet=self._fields[1]
        amplitude=self._fields[2]
        
        
        #Set up a constant ln_impedance or its possition in the multiparametric field:
        

        ref_offset=self._field_offsets[0]
        ref_length=self._field_lengths[0]
            
        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        wav_offset=self._field_offsets[1]
        wav_length=self._field_lengths[1]


        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        amp_offset=self._field_offsets[2]
        amp_length=self._field_lengths[2]
        

        def calculate_residues(x_opt,residues):
            nonlocal wavelet, amplitude, reflectivity

            
            if optimize_wavelet:
                wavelet=x_opt[wav_offset:wav_offset+wav_length]

            if optimize_reflectivity:
                reflectivity=x_opt[ref_offset:ref_offset+ref_length]

            if optimize_amplitude:
                amplitude=x_opt[amp_offset:amp_offset+amp_length]
            
            #Calculate the residues:
            convolve(wavelet,reflectivity,residues)
            residues-=amplitude
            
            return  residues

        return calculate_residues

    def create_jacobian_dot_vec(self):
        """Create the function which merform the matrix multiplication
        of the jacobian and a vector.
        """

        multiparametric_size=self._working_multiparametric_field.get_total_size()
        field_times_grad_convolution=self.field_times_grad_convolve

        
        
        optimize_wavelet=self.optimize_wavelet
        optimize_reflectivity=self.optimize_reflectivity
        optimize_amplitude=self.optimize_amplitude
    
        
        reflectivity=self._fields[0]
        wavelet=self._fields[1]
        amplitude=self._fields[2]
        
        
        #Set up a constant ln_impedance or its possition in the multiparametric field:
        

        ref_offset=self._field_offsets[0]
        ref_length=self._field_lengths[0]
            
        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        wav_offset=self._field_offsets[1]
        wav_length=self._field_lengths[1]


        #Set up a constant wavelet or its possition in the multiparametric field:
        
            
        amp_offset=self._field_offsets[2]
        amp_length=self._field_lengths[2]
 
  
        def jacobian_dot_vec(x_opt,residues,output):
            nonlocal amplitude, reflectivity, wavelet

            if optimize_wavelet:
                wavelet[:]=x_opt[wav_offset:wav_offset+wav_length]
                
                
            if optimize_reflectivity:
                reflectivity[:]=x_opt[ref_offset:ref_offset+ref_length]

            if optimize_amplitude:
                amplitude[:]=x_opt[amp_offset:amp_offset+amp_length]

            if optimize_wavelet:
                output_wav=output[wav_offset:wav_offset+wav_length]
                field_times_grad_convolution(residues,reflectivity,output_wav)
                
            if optimize_reflectivity:
                grad_ref=output[ref_offset:ref_offset+ref_length]
                field_times_grad_convolution(residues,wavelet,grad_ref)

            if optimize_amplitude:
                grad_amp=output[amp_offset:amp_offset+amp_length]
                grad_amp[:]=-residues[:]


            return output


        return jacobian_dot_vec

        



    
