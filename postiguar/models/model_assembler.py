import numpy as np
from ..models.model_base import inversion_model


class model_assembler(inversion_model):




    model_names=["Model Assembler"]
    short_description="Class develpod to assemble several model and regularizers together."


    def __init__(self):

        self._model_list=[]
        self._working_multiparametric_field=None

    def add_model(self,model):

        self._model_list.append(model)

    def setup(self):

        self.has_grad_function_factory=True
        mp_fields=[]
        for model in self._model_list:

            self.has_grad_function_factory=self.has_grad_function_factory and model.has_grad_function_factory
            
            mp_fields.append(model._working_multiparametric_field)

        
        self._set_working_multiparametric_field(mp_fields)

        return self

    def _set_working_multiparametric_field(self,mp_fields):
        
        for multiparametric_field in mp_fields:
            
            if self._working_multiparametric_field==None:
                
                self._working_multiparametric_field=multiparametric_field
                
            else:
                
                if self._working_multiparametric_field is not multiparametric_field:
                    
                    raise ValueError("Impossible to assemble models belonging to different multiparametric fields.")
    
    
    def get_objective_function(self,optimized=False,**kwargs):

        objective_functions=[]
        
        for ii in self._model_list:

            obj_func=ii.get_objective_function()
            objective_functions.append(obj_func)
        
        def obj_func(x0,*args,**kwargs):
            nonlocal objective_functions

            misfit=0
            for objective_function in objective_functions:

                misfit+=objective_function(x0)

            return misfit
        
        return obj_func
        
    def get_gradient_function(self,**kwargs):

        gradient_functions=[]
        
        for ii in self._model_list:

            grad_func=ii.get_gradient_function()
            gradient_functions.append(grad_func)


        multiparametric_size=self._working_multiparametric_field.get_total_size()

        grad=np.zeros(multiparametric_size)
        first_grad=gradient_functions.pop(0)

        def grad_func(x0,*args,**kwargs):
            nonlocal gradient_functions, grad

            grad[:]=first_grad(x0)
            for grad_function in gradient_functions:

                grad+=grad_function(x0)

            return grad

        return grad_func
    
    
    def get_hessian_function(self,**kwargs):

    
        return None
