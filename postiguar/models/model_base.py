from abc import ABC, abstractmethod
from ..utilities.norms import get_norm, get_gradient

_available_models={}



def register_model(model):
    """Function decorator: Register an inversion model in the list of
    availabel models. You must pass this decorator to every model
    class you implement, if you intend to use the available models
    list (which is the way this program is intended to be used.

    Parameters:
    ----------
       
        model: inverion_model 
            Model to be registered. We strong advise to use
           this function only as a decorator.

    """

    
    names=model.model_names

    #Check if the model has properly defined model_names:
    if not isinstance(names,list):
            
        raise TypeError("""Trying to import a model with a "model_names" variable improperly defined.
        Please, in the source code, define the "model_names" variable and make sure it is a list and try again.""")

    #register the model in the available model dict:
    for name in names:
        _available_models[name]=model

        
    return model


def get_model(model_name):
    """Search am inversion model in the list of available ones. The 
    search is performed by the model name.

        Parameters:
        ----------

            model_name: str
                The name of the mode to be searched (as a string)

        Returns:
        -------

            model: inversion_model
                An instance of the searched model.

    """
    
    model=_available_models[model_name]

    return model



def list_available_models(search_key=None,*args,**kwargs):

    models=list(set(_available_models.values()))
    
    print("Available Models:\n")
    for model in models:

        Header=""
        for name in model.model_names:

            Header+=name+", "
            

        
        Header=Header[0:-2]    
        info=Header+":\n"+model.short_description+"\n"

        print(info)

        
    return None




class inversion_model(ABC):
    """Abstract class to help implementing a new inversion model.
    """

    #Model information:
    is_linear=False #If the model is linear.
    is_operator_linear=False

    is_convex=False
    is_operator_convex=False
        
    #Functions availability:
    has_grad_function_factory=False
    has_hessian_function_factory=False               


    #Key which the model will be searched:
    model_names=None    
    short_description="No description provided."
    
    #Inversion characteristics:
    
    _number_of_fields=None #Number of fiedls the model works with.
    
    
    
    
    #Model Mandatory Parameters:
    epsilon=0


    def __init__(self,working_fields,
                 optimize_fields,
                 norm,
                 epsilon):


        self._field_shapes=None
        self._field_offsets=None
        self._field_lengths=None
        self._fields=None
        self._working_multiparametric_field=None
        
        
        
        self._optimize_fields=optimize_fields
        self.norm_name=norm
        
        self.set_epsilon(epsilon)
        
        self._set_field_properties(working_fields)
        self._set_working_multiparametric_field(working_fields,optimize_fields)
    
        
    
    def _set_field_properties(self,working_fields):
        """
        

        Parameters
        ----------
        working_fields : list(fields)
            fields passed to the optimizer (being part of a multiparametric field or not.

        Returns
        -------
        None.

        """
    
        self._fields=[]
        self._field_shapes=[]
        self._field_offsets=[]
        self._field_lengths=[]
        self._fields=[]
        
        for field in working_fields:
            
            
            try:
                #Get the aprent  multiparatric field:
                mp_field=field.dtype.metadata["multiparametric_field"]
                
            except (KeyError,TypeError):
                
                #Get field size:
                self._field_lengths.append( field.size )
                self._field_shapes.append(field.shape)
                self._field_offsets.append( 0 )
                
            else:
                
                #Find the field position in the parent multiparatric field:
                field_position=mp_field._get_external_field_position(field)


                field_length=mp_field.get_field_length(field_position)
                field_offset=mp_field.get_field_offset(field_position)
                field_shape=mp_field.get_field_shape(field_position)

                #Get field size:
                self._field_lengths.append( field_length )
                self._field_offsets.append( field_offset )
                self._field_shapes.append(field_shape)
            
            finally:
                
                self._fields.append(field.flatten())
    def _set_working_multiparametric_field(self,working_fields,optimize_fields):
        """Set the _working_multiparametric_field and check wether 
        all the optimization fields belongs o the same multiparametric
        field.
        """
        
        self._working_multiparametric_field=None
        for field,optimize_field in zip(working_fields,optimize_fields):

            if optimize_field:

                if self._working_multiparametric_field is None:

                    
                    try:
                        self._working_multiparametric_field=field.dtype.metadata["multiparametric_field"]
                    except:
                        self._working_multiparametric_field=field.flatten()
                        
                        
                else:
                    #The codes reaches here when there is more than one field to optimize.
                    
                    #Check if the this field belongs to the same multiparametric field of the other optimized fields:
                        
                    if field.dtype.metadata["multiparametric_field"] is not self._working_multiparametric_field:

                        raise ValueError("Some of the optimization fields belong to different multipramatric fields")  

        if self._working_multiparametric_field is None:

            raise ValueError("No optmization field was passed.")

     
    def set_epsilon(self,epsilon):
        """Set the model weight paremeter"""
        
        self.epsilon=epsilon
        
        return self
        
    @abstractmethod    
    def setup(self,*args,**kwargs):
        """Setp up the seismic model.

        Returns:
        ----------------------

        itself"""

        return self

    def get_objective_function(self,**kwargs):
        """ Construct and return the objective function.

        Returns:
        -------

            obj_func: func(x_opt,*args,**kwargs)
              Objective function to be minimised.

        """

        
        norm=get_norm(self.norm_name,**self.kwargs)
        calculate_residues=self.calculate_residues
        epsilon=self.epsilon
        residues=self.residues
        
        
        def obj_func(x_opt,*args,**kwargs):
            nonlocal residues

            calculate_residues(x_opt,residues)
            return epsilon*norm(residues)


        return obj_func


    def get_gradient_function(self,**kwargs):
        """ Construct and return the gradient function.

        Parameters:
        ----------

             
        Returns:
        -------

            grad_func: func(x_opt,*args,**kwargs)
               Function which calculates the gradient of x_opt.

        """

        epsilon=self.epsilon
        
        norm_grad=get_gradient(self.norm_name,**self.kwargs)
        calculate_residues=self.calculate_residues
        jacobian_dot_vec=self.jacobian_dot_vec

        
        residues=self.residues
        grad=self.grad
        
  
        def grad_func(x_opt,*args,**kwargs):
            nonlocal grad, residues
        
            calculate_residues(x_opt,residues)
            norm_grad(residues,residues)
            jacobian_dot_vec(x_opt,residues,grad)
            

            return epsilon*grad


        return grad_func



    def get_hessian_function(self,target_data,flatenned=False,**kwargs):
        """ Create and returns the function to calculate the model hessian.
           This model has no Hessian.
        """
        
        return None

    def calculate_missfit(self,*args,**kwargs):
        """Calculate curent the model missfit.
        
        This function calculates the difference between the data calculated 
        by the forward operator and the inversion target data, which can be expressed mathematically as:
        
        missfit=norm(\sum r)
        where norm is the inversion norm passed to the model and r are the residues (difference between calculated and target data). 
        
        Note: This function uses the exact same expression that is used in the inversion process.

        Parameters:
            
            None:
              This function requires no parameters since the model class already has all the information necessary.
              

        Returns:

            Missfit: float
              The model missfit.
        
        
        """
        x_opt=self._working_multiparametric_field.get_multiparametric_field()
        objective_function=self.get_objective_function()
        missfit=objective_function(x_opt)
        return missfit
        
    def get_working_multiparametric_field(self):
        """
        

        Returns
        -------
        multiparametric_field:

        """
        
        return self._working_multiparametric_field
        