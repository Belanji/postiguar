import numpy as np
import scipy.sparse as scp

#import scipy.special as ssp
from .model_base import inversion_model, register_model
from ..utilities.function_blocks import unpack_mp_field_entry
from ..utilities.norms import get_norm, get_gradient


@register_model
class fractional_tv(inversion_model):
    """Class implementing the total variation regularizer with fractional
    derivatives. Currently, it only works with a single field. so please, use 
    different models if you want to regularize different fields.
    """
    
    has_grad_function_factory=True
            

    model_names=["fractional_tv","FracTV","Fractional Total Variation"]
    short_description="Implement the fractional total variation regulariation method."

    def __init__(self,input_fields,epsilon=1.,norm="L2",order=1,dimension=-1, **kwargs):

        #Process an set up the input fields and target data:
        try:
            optimize= input_fields 
        except ValueError:
            print("The model total variation requires 1 input_fields")
            raise 

        #Unpack the required field position pairs:
        self.mp_opt, self.optimize, self.opt_position=unpack_mp_field_entry(optimize)
        self.epsilon=epsilon

        #Get the minimization norm and its associated gradient:
        self.norm_name=norm
        self.dimension=dimension
        self.order=order

        self.kwargs=kwargs
        
        
    def setup(self):


        print('Initializing Fractional-Tv regulalizer:\n')
        print(f'epsilon(scaling):      {self.epsilon}')
        print(f'Derivative Order:      {self.order}')
        print(f'Derivative Dimension:  {self.dimension}')
        print(f'Residues norm:         {self.norm_name}\n')
        
        self._working_multiparametric_field=self.mp_opt
        self.multiparametric_size=self._working_multiparametric_field.get_total_size()
        self.of_length=self.mp_opt.get_field_length(self.opt_position)

        #Create sparse matrix to perform derivates:
        self.Derivate_matrix=self._create_derivative_matrix()
        

        #Create functions to calculate redidues and grad_residues
        self.calculate_residues=self.create_residues_function()
        self.jacobian_dot_vec=self.create_jacobian_dot_vec()

        residues_size, _=self.Derivate_matrix.shape

        #Allocate memory to be reused in residues calculation:
        self.grad=np.zeros(self.multiparametric_size)
        self.residues=np.zeros(residues_size)

        return self
        
    def create_residues_function(self):

        opt_field_position=self.opt_position
        of_offset=self.mp_opt.get_field_offset(opt_field_position)
        of_length=self.mp_opt.get_field_length(opt_field_position)

        def calculate_residues(x_opt,residues):

            opt_field=x_opt[of_offset:of_offset+of_length]
            residues[:]=self.Derivate_matrix._mul_vector(opt_field)
            return  residues
        
        return calculate_residues


    def create_jacobian_dot_vec(self):
        """ Construct and return the gradient function.

        Parameters:
        ----------

             
            Returns:
            -------

             grad_func: func(x0,*args,**kwargs)
               Function which calculates the gradient of x0.

        """
        
        opt_position=self.opt_position
        opt_offset=self.mp_opt.get_field_offset(opt_position)
        opt_length=self.mp_opt.get_field_length(opt_position)

        jacobian=self.Derivate_matrix.transpose(copy=True)
    
        def jacobian_dot_vec(x_opt,vector,output): 
            
            grad_opt=output[opt_offset:opt_offset+opt_length]
            grad_opt[:]=jacobian._mul_vector(vector)
            
            
            return output


        return jacobian_dot_vec



    
    def _create_derivative_matrix(self):
        """Create the matrix which performs the vector derivatives.
        """

        #Get basic information:
        opt_dimension=self.dimension
        opt_position=self.opt_position
        Matrix_Size=self.mp_opt.get_field_length(opt_position)

        
        print("Filling Derivative materix")
        
        #Transform actual data to a 3D data array (even if some dimensions has 0 size):
        temp_shape=np.array(self.mp_opt.get_field_shape(self.opt_position))
        temp_length=len(temp_shape)
        dim_diff=3-temp_length
        
        opt_dimension=dim_diff+opt_dimension if opt_dimension>= 0 else 3+opt_dimension 
        
        opt_shape=np.ones(3,dtype="int32")
        opt_shape[dim_diff:]=temp_shape


        #Calculate each dimension element offset and size:
        dimns=set([0,1,2])
        dimns.remove(opt_dimension)
        
        dim1=dimns.pop()
        dim2=dimns.pop()

        
        
        offset1=opt_shape[-1:dim1:-1].prod()
        offset2=opt_shape[-1:dim2:-1].prod()
        opt_offset=offset1=opt_shape[-1:opt_dimension:-1].prod()

        #Create the functinion which calculate point position:
        point_position= lambda dim1,dim2,opt: dim1*offset1+dim2*offset2+opt*opt_offset                        
        
        Nx=opt_shape[opt_dimension]        
        nu=self.order
        #gamma=ssp.gamma


        
        Dm=scp.lil_matrix((Matrix_Size,Matrix_Size))    
        Cnk=np.empty(Nx)
        #prefac=gamma(nu+1)#/dx**nu
    
        Cnk[0]=1.
    
        for ii in range(1,Nx): 
            Cnk[ii]=Cnk[ii-1]*(1- (nu+1)/ii) 



        #Filling the matrix operator:

        for pp in range(opt_shape[dim1]):
            for qq in range(opt_shape[dim2]):
                for ii in range(Nx):
                    for jj in range(0,ii+1):                
                

                        ip=point_position(pp,qq,ii)
                        ij=point_position(pp,qq,jj)
                
                
                        
                        Dm[ip,ij]=Cnk[ii-jj]
            
    
        return Dm.tocsr()
    

        
    def get_objective_function(self,optimized=False,**kwargs):
        """ Construct and return the objective function.

        Parameters:
        ----------

        Returns:
        -------

            obj_func: func(x0,*args,**kwargs)
              Objective function to be minimised.

        """
        
        epsilon=self.epsilon
        residues=self.residues
        calculate_residues=self.calculate_residues
        
        norm=get_norm(self.norm_name,**self.kwargs)        
        
        def obj_func(x_opt,*args,**kwargs):
            nonlocal residues
            
            calculate_residues(x_opt,residues)
            return epsilon*norm(residues)


        return obj_func


    def get_gradient_function(self,**kwargs):
        """ Construct and return the gradient function.

        Parameters:
        ----------

             
        Returns:
        -------

            grad_func: func(x0,*args,**kwargs)
               Function which calculates the gradient of x0.

        """

        epsilon=self.epsilon



        norm_grad=get_gradient(self.norm_name,**self.kwargs)

        grad=self.grad 
        residues=self.residues

        calculate_residues=self.calculate_residues
        jacobian_dot_vec=self.jacobian_dot_vec
        
        def grad_func(x_opt,*args,**kwargs):
            nonlocal grad, residues            
            
            calculate_residues(x_opt,residues)
            norm_grad(residues,residues)
            jacobian_dot_vec(x_opt,residues,grad)
            
            grad*=epsilon
            
            return grad


        return grad_func

