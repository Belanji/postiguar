import numpy as np


class  multiparametric_field:
    """
    Class implementing the container multiparametric fields.
    """

    def __init__(self, 
                 number_of_fields=0,
                 field_names=None,
                 short_names=None,
                 field_shapes=None,
                 dtype=np.float64):

        #Status variables:
        self._assembled=False #Variable informing if the multiparametric field was assembled.

        #Vector containing all the data:
        self._multiparametric_data=None

        #Variables describing the data:
        self.field_shapes=[]
        self._field_lengths=[]
        self.number_of_fields=number_of_fields

        self._dtype=np.dtype(dtype,metadata={"multiparametric_field":self})
        #Variables for qualitative description (but we have plans to use these names in the future):
        self.field_names=[]
        self.short_field_names=[]


        
        #Constructing the object:
        
        #Set the field names:
        if field_names is not None:

            self._set_field_names(field_names)

        else:
            
            std_names=list("field"+str(i) for i in range(number_of_fields))
            self._set_field_names(std_names)
        
        #Setting the short names:
        if short_names is not None:

            self._set_short_field_names(short_names)

        else:
            
            std_short_names=list("x"+str(i) for i in range(number_of_fields))
            self._set_short_field_names(std_short_names)



        #Seting the field shapes and assembly the data field: 
        if field_shapes is not None and number_of_fields > 0:

            self._set_field_shapes(field_shapes)
            self._assembly()
            
    def _get_external_field_position(self,external_field):
        """
        Given a external field view, find which internal field it corresponds.

        Parameters
        ----------
        field : np.array
            A view of one of the fields belongig to this multiparametric field.

        Returns
        -------
        offset: int.
            Offset of the field relative to the multiparametric_field.
         

        """
        #Get the memory address the external fields points to:
        external_mem_address,_= external_field.__array_interface__['data']
        
        
        for ii in range(self.number_of_fields):
            
            #Get the memory address of each field:
            internal_mem_address,_=self.get_field(ii).__array_interface__['data']
            #Check if both fields points to the same address:
            if internal_mem_address == external_mem_address:
                
                return ii
            
        raise ValueError("Passed field does not belong to the multiparametric field")
    def _get_field_position(self,key):
        """Get the position of the field (short)named key.
        
        Parameters:
        ----------------------------------------------------------
        
            key: str.
                The name or short name of the field.
        
        Returns:
        ----------------------------------------------------------
            position: int
                THe position of the field in the multiparametric_field.
        
        
        """
        
        
        try:
            
            field_position=self.short_field_names.index(key)
            
        except ValueError:
            
            try:
                field_position=self.field_names.index(key)
                
            except ValueError:
                
                raise
    
    
        return field_position
    

    def _set_field_names(self,field_names):
        """Set tthe names of the individual fields. Intendend for internal use.


           Parameters:
           -----------
             
              field_names: list of strings (or any other iterable)
                    The names to assign to each parameter field.
        """

        if isinstance(field_names, str):
            field_names=(field_names,)
        
        if len(field_names) != self.number_of_fields:
                
            raise IndexError("Number of field names is not equal de number of fields.")
                
        self.field_names=list(field_names)
        
        return self
        
        
        
    def _set_short_field_names(self,short_names):
        """Set the short names of the individual fields. Intended for internl use.
        
           Parameters:
           -----------
             
              field_names: list of strings (or any other iterable)
                    The short names to assign to each parameter field.
        """

        if isinstance(short_names, str):
            short_names=(short_names,)


        
        if len(short_names) != self.number_of_fields:
                
                raise IndexError("Number of field short names is not equal de number of fields.")
                
        self.short_field_names=list(short_names)
        
        
        return self


    def _set_field_shapes(self,field_shapes):
        """
        Set the shape of each field. Intende for internal use.

        Parameters
        ----------
        
        field_shapes : list of shapes (or any iterable)
            Each hape should be inclosed in a list of the form (dim1,[dim2],[dim3]).

        Raises
        ------
        
        IndexError
            Raises an error if length of field_shapes does not meets the number of fields.

        Returns
        -------
            
            Returns the object that called the function.

        """
        

        try:
            field_shapes[0][0]
        except TypeError:
            field_shapes=(field_shapes,)
        
            
        
        if len(field_shapes) != self.number_of_fields:
                
                raise IndexError("Number of field shapes is not equal de number of fields.")
                
        self.field_shapes=list(field_shapes)
        
        
        return self
        
    
    def add_field(self,data_array=None,field_shape=None,field_name:"str"=None,short_field_name:"str"=None):
        """
        Add a new field to the list off fields.
        
        
           Parameters:
           -----------
           
           field_shape: (dim1,[dim2],[dim3])
               A tupple (or any iterable) containing the dimension of the field being added.
        
           field_name: str
               The complete name of the added field.

           short_field_name: str
               The short name of the field being added.

           Return:
           -------
           
           Self: returns the calling object.
           
           """

        if data_array is None and field_shape is None:

            raise ValueError("Both data array and data shape  are null. One must be passed to add the field.")

        if data_array is not None:

            if field_shape is None:
                field_shape=data_array.shape
            else:
                array_length=self._calculate_data_length(data_array.shape)
                field_shape_length=self._calculate_data_length(field_shape)

                if array_length !=field_shape_length:

                    raise ValueError("Data array and field shape have imcompatible shapes.")
                

        
            
        self.field_shapes.append(field_shape)        
        self._assembly() 


        #Copy the data fro mdata_array if it was provided:
        if data_array is not None:
            
            new_field=self.get_field(-1)
            new_field[:]=data_array.flatten()[:]


            

        if field_name is not None:
               
            self.field_names.append(field_name)
               
        else:
               
            field_name="field"+str(self.number_of_fields-1)
            self.field_names.append(field_name)
           
           

           
        if short_field_name is not None:
               
            self.short_field_names.append(short_field_name)
               
        else:
               
            short_field_name="x"+str(self.number_of_fields-1)
            self.short_field_names.append(short_field_name)
            

            
        return self

    def _assembly(self):
        """
        Assemby the multiparametric field.
        """
        #Check if the multiparametric field was previously assembled:
        if not self._assembled:
        
            data_size=self._calculate_data_length(self.field_shapes)

            for ii in self.field_shapes:
                self._field_lengths.append(self._calculate_data_length(ii))
            
            
            self._multiparametric_data=np.zeros(data_size,dtype=self._dtype)
            self._assembled=True
            self._old_shapes=list(self.field_shapes)
            self.number_of_fields=len(self.field_shapes)
            
        shape_diff=len(self.field_shapes)-len(self._old_shapes)

        if shape_diff>0:

                for ii in range(-shape_diff,0):
                    
                    previous_data_size=len(self._multiparametric_data)
                
                
                    data_size_increment=self._calculate_data_length(self.field_shapes[ii])
                    new_data_size=previous_data_size+data_size_increment
                    self._field_lengths.append(data_size_increment)
                
                
                    self._multiparametric_data.resize(new_data_size,refcheck=False)
                    
                
                    self._old_shapes.append(self.field_shapes[ii])
                    self.number_of_fields+=1
        
                    
        for ii in range(0,len(self.field_shapes)):
        
            previous_shape=self._old_shapes[ii]    
            new_shape=self.field_shapes[ii]
        
            previous_vec_length=self._calculate_data_length(previous_shape)
            new_vec_length=self._calculate_data_length(new_shape)
        
            self._old_shapes[ii]=tuple(new_shape)
        
            
            if previous_vec_length != new_vec_length:
                        
                #Calculate data size before and after the the field to be removed:
            
                pre_data_length=self._calculate_data_length(self.field_shapes[:ii])
                pos_data_length=self._calculate_data_length(self.field_shapes[ii+1:])
            
                #Save data after the field that will be removed:
                temp_data=self._multiparametric_data[-pos_data_length:].copy()
                
                self._multiparametric_data.resize(pre_data_length+new_vec_length+pos_data_length,refcheck=False)
            
            
                if pos_data_length>0:
                    self._multiparametric_data[-pos_data_length:]=temp_data[-pos_data_length:]
                    
                self._field_lengths[ii]=new_vec_length        
                    
        
        return self

    def reshape_field(self,field_position,new_shape):
        """
        Reshape an exiting field to a new shape:

        Parameters
        ----------
        field_position : integer
            Integer number giving the field position in the multiparametric sequence.
            
        new_shape : tuple(dim1,[dim2],[dim3])
            New field shape.

        Returns
        -------
        

        """

        self.field_shapes[field_position]=tuple(new_shape)
        self._assembly()
        
    
    def _calculate_data_length(self,shapes):
        """
        Calculate the data size of the the variable shapes.
        The function return 0 if no shapes are provided.
        """
        
        #If shape is just integer, return the integer:
        if isinstance(shapes, int):
            return shapes
        
        data_size=0 
        
        try:
            shapes[0][0]
        except TypeError:
            shapes=(shapes,)
        except IndexError:
            return data_size
        
        for param_shape in shapes:
        
            field_data=1    
            for ii in param_shape:    
                field_data*=ii
            
            data_size+=field_data


        
            
        return data_size



    def get_field(self,key,flatten=True):
        """
        Get acess to a single parameter field.

        Parameters
        ----------

        key : interger
            The field number or (short)name.

        Returns
        -------
         
        field: ndarray
           Numpy array containing the required field.

        """
       
        if isinstance(key, int):
            
            field_number=key
        
        elif isinstance(key,str):
        
            field_number=self._get_field_position(key)
        
        else:
            
            raise TypeError("Type of multiparametric field not allowed.\nPlease use a str(field name or short-name) or an int (field position) to querry for fields.") 
       
        if field_number > self.number_of_fields-1:
            
            raise IndexError(f'Out of range. Current multiparametric field has {self.number_of_fields}.')
        
        if field_number <0:
            
            field_number=self.number_of_fields+field_number
            
            if field_number <0:
                
                raise IndexError(f'Out of range. Current multiparametric field has {self.number_of_fields}.')
        
        offset=0
        for ii in range(0,field_number):
            offset+=self._field_lengths[ii]
        
        end_vec=offset+self._field_lengths[field_number]
        
        field_shape=self.field_shapes[field_number]
        returning_field=self._multiparametric_data[offset:end_vec]
        
        
        if flatten:
            return returning_field
        else:
            
            return returning_field.reshape(field_shape)


    def __getitem__(self,key):
        

        return self.get_field(key,flatten=False)


    def get_multiparametric_field(self):
        """
        Get acess to the multiparametric Field. If you need to work only with
        the single parameter version, use the routine get_field instead.

        Returns
        -------
       
            multiparametric_field: ndarray

        """
        
        
        return self._multiparametric_data

    def rename_field(self,field_number,new_name=None,new_short_name=None):
        """
        Rename one of the fields.

        Parameters:

           field_number: integer
              The number of the field to be renamed.

           new_name: string(optional)
              The new field name.
        
           new_short_name: string(optional)
              The new field short name.

        """
        
        if new_name is not None:

            self.field_names[field_number]=new_name

        if new_short_name is not None:

            self.short_field_names[field_number]=new_short_name

    def get_field_shape(self,field_position):
        """Get shape of the field.

              Parameters:
              ----------
             
                 field_position: int
                    The field position to ask the shape/

              Returns:
              ------- 

                field_shape: tuple(int,[int],[int])
                   The field shape as a tuple of ints.
        """
        
        return self.field_shapes[field_position]

    def get_field_length(self,field_position):
        """Get length of the field.

              Parameters:
              ----------
             
                 field_position: int
                    The field position to ask the shape/

              Returns:
              ------- 

                field_length: int
                   The field length.
        """
        
        return self._field_lengths[field_position]

    
    def get_field_offset(self,field_number):
        """Get offset of the field in the multiparametric array.

              Parameters:
              ----------
             
                 field_position: int
                    The field position to ask the shape/

              Returns:
              ------- 

                field_length: int
                   The array offset.
        """

        if field_number>self.number_of_fields-1:
            raise IndexError("list index out of range")

        if field_number<0:
            field_number=self.number_of_fields+field_number

            if field_number<0:

                raise IndexError("list index out of range")

            
        offset=0
        for ii in range(0,field_number):
            offset+=self._field_lengths[ii]

        
        return offset
        
    def get_total_size(self):
        """
        Get the size of the whole multiparametric field.

        Returns
        -------
        
        multiparametric field size: int
            The size of the whole multiparametric field.

        """
        
        return len(self._multiparametric_data)
    
