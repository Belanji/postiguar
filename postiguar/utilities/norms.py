import numpy as np
from functools import partial 

_available_norms={}
_available_grads={}
_available_shrinkages={}




def get_norm(norm_name,**kwargs):
    """Search a norm in the list of available ones. The 
    search is performed by the norm name.

        Parameters:
        ----------

            norm_name: str
                The name of the norm to be searched (as a string)

        Returns:
        -------

            norm: func
                The norm Function.

    """
    
    norm=_available_norms[norm_name]

    if kwargs == {}:

        return norm
    
    else:

        return partial(norm,**kwargs)
    
    


def get_gradient(gradient_name,**kwargs):
    """Search a gradient in the list of available ones. The 
    search is performed by the gradient name.

        Parameters:
        ----------

            gradient_name: str
                The name of the gradient to be searched (as a string)

        Returns:
        -------

            gradient: func
                The gradient Function.

    """
    
    gradient=_available_grads[gradient_name]

    if kwargs== {}:
        
        return gradient

    else:
        return partial(gradient,**kwargs)


def get_shrinkage(shrinkage_name,**kwargs):
    """Search a shrinkage function in the list of available ones.
    The search is performed by the shrinkage function name.

        Parameters:
        ----------

            shrinkage_name: str
                The name of the shrinkage to be searched (as a string)

        Returns:
        -------

            shrinkage: func
                The shrinkage Function.

    """
    
    shrinkage=_available_shrinkages[shrinkage_name]

    if kwargs== {}:
        
        return shrinkage

    else:
        return partial(shrinkage,**kwargs)




def register_norm(name):

    def norm_registerer(norm):
        _available_norms[name]=norm

        return norm
    return norm_registerer

def register_grad(name):

    def grad_registerer(gradient_func):
        _available_grads[name]=gradient_func

        return gradient_func
    return grad_registerer



def register_shrinkage(name):

    def shrinkage_register(shrinkage_func):
        _available_shrinkages[name]=shrinkage_func

        return shrinkage_func
    
    return shrinkage_register

#---------------------------------------------------------------
#L0_norm:
@register_norm("L0")
def L0_norm(x0,precision_n=1e-12,**kwargs):

    non_zeros=np.zeros(len(x0))
    non_zeros[np.fabs(x0)>precision_n]=1.
    return non_zeros.sum()

    
@register_shrinkage("L0")
def L0_shrinkage(x0,alpha_i,**kwargs):

    
    output=x0.copy()

    alpha_m=2*np.fabs(alpha_i)
    output[np.fabs(x0)<alpha_m]=0.
    
    return output




#L1_norm:
@register_norm("L1")
def L1_norm(residues,nu=1e-12,**kwargs):

    return np.fabs(residues).sum()

#L1 norm smoothed grad:
@register_grad("L1")
def L1_norm_grad(residues,grad,nu=1e-12,**kwargs):

    mask=np.fabs(residues)>=nu
    not_mask=np.fabs(residues)<=nu

    
    grad[mask]=np.sign(residues[mask])
    grad[not_mask]/=nu

    
@register_shrinkage("L1")
def L1_shrinkage(x0,alpha_i,**kwargs):
    
    return np.maximum(np.fabs(x0)-alpha_i,0)*np.sign(x0)

    
#----------------------------------------------------------------

#L2_norm:

@register_norm("L22")
def L2_norm(residues,beta=1.e-4,**kwargs):

    return np.sqrt( (residues*residues).sum() +beta**2 )



@register_grad("L22")
def L2_norm_grad(residues,grad,beta=1.e-4,**kwargs):

    norm_fac=1./np.sqrt( (residues*residues).sum() +beta**2 )

    grad[:]=norm_fac*residues[:]

    

#L2_norm-Squared:  
@register_norm("L2")
def L2_squared_norm(residues,**kwargs):

    return (residues*residues).sum()

@register_grad("L2")
def L2_squared_norm_grad(residues,grad,**kwargs):

    grad[:]=2*residues[:]

    
@register_shrinkage("L2")
def L2_squared_shrinkage(x0,alpha_i,**kwargs):

    return x0/(2.*alpha_i+1)

#---------------------------------------------------------

@register_norm("q-L1")
def QL1_norm(residues,q,**kwargs):

    q_fac=(q-1)/(3-q)

    return ( np.log( 1. + q_fac*np.fabs(residues) ).sum() )/(q-1)


@register_grad("q-L1")
def QL1_grad(residues,grad,q,**kwargs):

    q_fac=(q-1)/(3-q)

    grad[:]=np.sign(residues)/( (1.+q_fac*np.fabs(residues) ) * (3.-q) )


@register_shrinkage("q-L1")
def QL1_shrinkage(x0,alpha_i,q,**kwargs):

    gamma=alpha_i/(q-1.)
    q_fac=(3.-q)/(q-1.)
    dx=np.maximum(q_fac*np.fabs(x0)-gamma,0)
    

    return 0.5*np.sign(x0)*(  np.fabs(x0)-q_fac+np.sqrt( (q_fac-np.fabs(x0))**2 + 4.*dx )  )


    
#-------------------------------------------------
@register_norm("q-L2")
def Q_norm(residues,q,**kwargs):

    q_fac=(q-1)/(3-q)

    return ( np.log( 1. + q_fac*residues*residues ).sum() )/(q-1)


@register_grad("q-L2")
def Q_grad(residues,grad,q,**kwargs):

    q_fac=(q-1)/(3-q)

    grad[:]=2. * residues/( (1+q_fac*residues*residues) * (3-q) )


#--------------------------------------------------------------------

#"Norm" induced by the kaniadakis disteribution:

@register_norm("k-L2")
def k_norm(residues,k,**kwargs):

    residues2=residues*residues
    log_K_exp=np.log( np.sqrt(1.+residues2*residues2*(k*k/4.)) -residues2*(k/2.) )
        
    return -(log_K_exp.sum())/k


@register_grad("k-L2")
def k_grad(residues,grad,k,**kwargs):
    
    residues2=residues*residues #Get the residues squared.

            
    sqrt_fac=np.sqrt( 1.+residues2*residues2*(k*k/4.) )
    divisor=-k*(sqrt_fac-residues2*(k/2.))
            
            
    grad[:]=0.5*(k*k*residues2*residues)/sqrt_fac-k*residues
    grad[:]=grad[:]/divisor[:]
