#homebrew version of the ricker wavelet.
import numpy as np

def ricker(time,f1=55.,wavelet_center=0):
    """Construct a ricker wavelet interpolated at times specified in the time array.
    
    Parameters:
    ----------------
    
        time: ndarray.
            Time positions to calculate the wavelete (in seconds)
            
    Optional paratameter:
    --------------------     
    
        f1: float
            Main frequency of the wavelet (in Hz).
            
        wavelet_center: float
            Center of the wavelet.
            
    Returns:
    ------------

        wavelet: ndarray
           Numpy array containing the wavelet.
   
    """

        
    #shift time to center the ricker wavelet at wavelet_center:
    time=time-wavelet_center
    nu=f1  #just aliasing the main frequency.
    
    wav= (1 - 2 * (np.pi * nu * time) ** 2) * np.exp(-(np.pi * nu * time) ** 2)
    
    return wav


def ormsby(time, f1=5.,f2=10., f3=40., f4=45.,wavelet_center=0,scale=True):
    """Construct a ricker wavelet. The wavelet properties can be passed as 
    optinional kwargs.
    
    Parameters:
    ----------------
    
        time: ndarray.
            Time positions to calculate the wavelete (in seconds)
            
    Optional paratameter:
    --------------------     
    
        f1: number.
        f2: number.
        f3: number.
        f4: number.
            The four frequencies necessary to define the ormsby wavelet (in Hz).
            
        wavelet_center: int.
            Center of the wavelet.
            
    
    """

        
    #shift time to center the ricker wavelet at wavelet_center:
    time=time-wavelet_center
    
    sinc1=np.sinc(f1*time)**2
    sinc2=np.sinc(f2*time)**2
    sinc3=np.sinc(f3*time)**2
    sinc4=np.sinc(f4*time)**2

    pre_fact1=(f1**2)/(f2-f1)
    pre_fact2=-(f2**2)/(f2-f1)
    pre_fact3=-(f3**2)/(f4-f3)
    pre_fact4=(f4**2)/(f4-f3)

    if scale:

        global_prefact=1./np.abs(pre_fact1+pre_fact2+pre_fact3+pre_fact4)
    else:

        global_prefact=np.pi

                                   
    wav=global_prefact*(pre_fact4*sinc4+pre_fact3*sinc3+pre_fact2*sinc2+pre_fact1*sinc1)
    return wav


