from ..base.multiparametric_field import multiparametric_field

def unpack_mp_field_entry(input_field,flatten=True):
    """
    Function used to parse an pair of multiparametric field plus field position into a the multiprametric field, the single field and its position.
    Note: It is designed to handle some standard values.
    """
    
    if isinstance(input_field,multiparametric_field):

        mp_input=input_field
        input_data=input_field.get_field(0)
        input_field_position=0
        
    else:

        mp_input, input_field_position=input_field
        input_data=mp_input.get_field(input_field_position,flatten)


    return mp_input, input_data, input_field_position
