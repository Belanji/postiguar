from abc import  ABC, abstractmethod
from ..models.model_assembler import model_assembler
from ..models.model_base import inversion_model
from ..utilities.norms import get_shrinkage, get_norm, get_gradient

from numba import njit
import numpy as np

_available_optimizers={}

def register_optimizer(optimizer):
    """Function decorator: Register a optimizer in the list of
    available optimizers. You must pass this decorator to every optimizer
    that you implement if you intend to use the available optimizers
    list (which is the way this program is intended to be used).

    Parameters:
    ----------
       
        optimizer: optimizer 
            optimizer to be registered. We strong advise to use
           this function only as a decorator.

    """

    
    names=optimizer.optimizer_names

    #Check if the optimizer has properly defined optimizer_names:
    if not isinstance(names,list):
            
        raise TypeError("""Trying to import a optimizer with a "optimizer_names" variable improperly defined.
        Please, in the source code, define the "optimizer_names" variable and make sure it is a list and try again.""")

    #register the optimizer in the available optimizer dict:
    for name in names:
        _available_optimizers[name]=optimizer

        
    return optimizer


def get_optimizer(optimizer_name):
    """Search an inversion optimizer in the list of available ones. The
    search is performed by the optimizer name.

        Parameters:
        ----------

            optimizer_name: str
                The name of the mode to be searched (as a string)

        Returns:
        -------

            optimizer: inversion_optimizer
                Return the class of the searched optimizer.

    """
    
    optimizer=_available_optimizers[optimizer_name]

    return optimizer



def list_available_optimizers(search_key=None,*args,**kwargs):

    optimizers=list(set(_available_optimizers.values()))
    
    print("Available optimizers:\n")
    for optimizer in optimizers:

        Header=""
        for name in optimizer.optimizer_names:

            Header+=name+", "
            

        
        Header=Header[0:-2]    
        info=Header+":\n"+optimizer.short_description+"\n"

        print(info)

        
    return None


class data_optimizer(ABC):
    """Abstract base class to help implementing a optimizer."""

    optimizer_names=None
    short_description="No description provided."

    def __init__(self,smooth_model,
                     nonsmooth_model,
                     set_shrinkage_functions=False):

        self.multiparametric_field=None
        self.smooth_global_objective_function=None
        self.smooth_global_gradient_function=None

        self.process_smooth_objective(smooth_model)        
        self.process_nonsmooth_objective(nonsmooth_model,set_shrinkage_functions)

        self.opt_size=self.multiparametric_field.get_total_size()
        
        if self.smooth_global_objective_function==None:
            self._set_zero_smooth_objective_functions()

    def process_smooth_objective(self,smooth_model):
        
        #Process smoth model:
        if smooth_model is not None:
            
            self.smooth_model=self._assembly_model(smooth_model)

            #Get smooth multiparametric field:
            smooth_multiparametric_field=self.smooth_model.get_working_multiparametric_field()
            
            #Check if the smooth model works on the same multiparametric field:
            if self.multiparametric_field==None:
                
                self.multiparametric_field=smooth_multiparametric_field
                
            elif smooth_multiparametric_field is not self.multiparametric_field:
                
                raise ValueError("There are models working in different multiparametric fields.\nPlease review your script and ensure your models works on the same multiparametric fields.")
                
            #Get acces to the smooth objective function and smooth gradient:
            self.smooth_global_objective_function=self.smooth_model.get_objective_function()
        
        
            if self.smooth_model.has_grad_function_factory:
                self.smooth_global_gradient_function=self.smooth_model.get_gradient_function()
            else:
                optimizer_name=self.optimizer_names[0]
                print(f"{optimizer_name} optimizator requires a gradient equiped smooth  model to work with.\n Please ensure all the smooth models have a gradient function. If it is not the case, please consider moving them to the nonsmooth model list.\nAborting the setup.")
                return None

        
            #If no smooth model is being passed, create a zero jac and zero objetive function:
                
    def _set_zero_smooth_objective_functions(self):
        
        
            def Fx(xk):

                return 0

            opt_size=self.opt_size
            @njit
            def DFx(xk):

                return np.zeros(opt_size)


            self.smooth_global_objective_function=Fx
            self.smooth_global_gradient_function=DFx

        

    def process_nonsmooth_objective(self,nonsmooth_model,set_shrinkage_functions=False):


        #Processing non smooth model:
        self.nonsmooth_norm_names=[]
        self.nonsmooth_kwargs=[]
        self.nonsmooth_calculate_residues=[]
        self.nonsmooth_jacobian_dot_vec=[]
        self.nonsmooth_epsilon=[]
        self.nonsmooth_objective_functions=[]
        self.nonsmooth_global_objective_function=None

        
        self.nonsmooth_residues_workspace=[]
        self.nonsmooth_grad_workspace=[]

        self.nonsmooth_shrinkage_functions=[]
        self.nonsmooth_norms=[]


        #Exit if no nonsmooth model was passed to the function.
        if nonsmooth_model is None:

            return
                       
        #Construct the list of nonsmooth-models:
        if isinstance(nonsmooth_model,list):
            
            self.nonsmooth_model_list=nonsmooth_model
            self.nonsmooth_model=self._assembly_model(nonsmooth_model)
            
        elif isinstance(nonsmooth_model,model_assembler):

            self.nonsmooth_model_list=nonsmooth_model._model_list
            self.nonsmooth_model=nonsmooth_model
            
        elif isinstance(nonsmooth_model,inversion_model):

            self.nonsmooth_model_list=[nonsmooth_model]
            self.nonsmooth_model=nonsmooth_model
            
        else:

            raise TypeError("Incompatible parameter passed as nonsmooth_model.")
            

        #Get nonsmooth multiparametric field:
        nonsmooth_multiparametric_field=self.nonsmooth_model.get_working_multiparametric_field()
            
        #Check if the smooth model works on the same multiparametric field:
        if self.multiparametric_field==None:
                
            self.multiparametric_field=nonsmooth_multiparametric_field
                
        elif nonsmooth_multiparametric_field is not self.multiparametric_field:
                
            raise ValueError("There are models working in different multiparametric fields.\nPlease review your script and ensure your models works on the same multiparametric fields.")


        
        self.nonsmooth_global_objective_function=self.nonsmooth_model.get_objective_function()



        for nonsmooth_model in self.nonsmooth_model_list:
            
            self.nonsmooth_norm_names.append(nonsmooth_model.norm_name)
            self.nonsmooth_kwargs.append(nonsmooth_model.kwargs)

            self.nonsmooth_calculate_residues.append(nonsmooth_model.calculate_residues)
            
            self.nonsmooth_jacobian_dot_vec.append(nonsmooth_model.jacobian_dot_vec)
            self.nonsmooth_epsilon.append(nonsmooth_model.epsilon)

            self.nonsmooth_objective_functions.append(nonsmooth_model.get_objective_function())


            
            #Getting workspace memory:
            self.nonsmooth_residues_workspace.append(nonsmooth_model.residues)
            self.nonsmooth_grad_workspace.append(nonsmooth_model.grad)

                        

        #Get the nonsmooth norm functions:
        for norm_name, kwargs in zip(self.nonsmooth_norm_names,self.nonsmooth_kwargs):

            
            self.nonsmooth_norms.append(get_norm(norm_name,**kwargs))



        if set_shrinkage_functions:
            
            for norm_name, kwargs in zip(self.nonsmooth_norm_names,self.nonsmooth_kwargs):

                self.nonsmooth_shrinkage_functions.append(get_shrinkage(norm_name,**kwargs) )


                
        
    @abstractmethod
    def setup(self,model,multiparametric_field,**kwargs):
        """Setup the optimizer.
        """

    @abstractmethod
    def iterate(self,n_iter,**kwargs):
        """Iteration function"""


        
    def _assembly_model(self,model_list):
        """Assemble a list of models in a single model"
        If an model_assembler class is passed instead the function
        just call the setup function and return the assembled_model.


        Parameters:
        ----------------------------------------------

            model_list: {list,model_assembler}
               A list of models to assembled or an assembled model.

        Returns:
        ---------------------------------------------

            assembled_model: model
               A model containing a combined objective function and a gradient if all models passed has it.
        """

        
        if isinstance(model_list,model_assembler):
            #If the model is already assembled, just acutalize it:
            assembled_model=model_list.setup()

        elif isinstance(model_list,inversion_model):

            assembled_model=model_list.setup()
            
        else:

            #Check if the model list contains a single model:
            if len(model_list)==1:

                assembled_model=model_list[0]

            else:
                
                assembled_model=model_assembler()

                for model in model_list:

                    assembled_model.add_model(model)

                assembled_model.setup()
            
        return assembled_model
