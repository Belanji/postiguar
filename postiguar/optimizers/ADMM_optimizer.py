import numpy as np 
from numba import njit
from .optimizer_base import data_optimizer, register_optimizer
from ..utilities.norms import get_shrinkage, get_norm, get_gradient
from .. import models
from ..models.model_base import inversion_model
from ..models.model_assembler import model_assembler

from scipy import optimize

@register_optimizer
class ADMM(data_optimizer):
    """Class containing a driver for the ADMM optimizer as described in:
    Monica Pragliola, Luca Calatroni, Alessandro Lanzad and
    Fiorella Sgallari.ADMM-based residual whiteness principle for
    automatic parameter selection in super-resolution
    problems.
    
    There is the option to use adaption as presented in:
        
    1) Non-stationary Douglas–Rachford and alternating direction method of multipliers: adaptive step-sizes and convergence
    Dirk A. Lorenz, Quoc Tran-Dinh.
    https://doi.org/10.1007/s10589-019-00106-9
    
    
    There is the option to use acceleration as presented in: 
    
    2) ADMM and Accelerated ADMM as Continuous Dynamical Systems.
    Guilherme França, Daniel P. Robinson, René Vida.    
    
    """

    optimizer_names=["ADMM"]
    short_description="ADMM optimizer."

    def __init__(self,
                 smooth_model=None,
                 nonsmooth_model=None,
                 splitting_constants=1.,
                 tol=1e-12,
                 iteration_method='L-BFGS-B',
                 opt_iterations=200,
                 adapt_splitting_constants=False,
                 adapt_every=1,
                 adapt_constant_sequence=None,
                 max_splitting_constants=1e7,
                 min_splitting_constants=1e-4,
                 accelerate_ADMM=False,
                 acceleration_constant=3.,
                 verbose=True,**kwargs):
        """ Create and set the variables necessary to implement the 
           optimizer.
           
           Parameters:
           ----------
        
               smooth_model: model
                 The model for the smooth part of the composite function.
                
               nonsmooth_model: model, [models]
                 A list of nonsmooth models.

               splitting_constants: float
                 Splitting constants beta.
 
               tol: float
                 tolerance (the algorithm will stop when ||x^{k+1} -x^{k}||_1<tol).

               inner_iterations: integer
                 Number of iterations performed in the Split-Bregman inner loop. 
        
               iteration_method: string.
                 1st-step minimiation solver (it is advisable to use "cg" or "L-BFGS-B").

               opt_iterations: integer
                 Number of iterations performed in the 1st-step.
                 
                 
               adapt_splitting_constants: Boolean
                 Set to true to adapt the splitting constants.
                 
               adapt_every: integer
                 How many outer iterations to be performed betwen two splitting constatants adaptations.
               
               adapt_constant_sequence: function(integer)
                   A function giving a summable "conservation sequence", where each element in the squence is calcultes as a function of the iteration kk.
                   If no sequence is provided, postiguar uses omeg_k=2^(-k/100).
               
               max_splitting_constants: float
                   maximum value allowed for the splitting constants.
               
               min_splitting_constants: float
                   minimum value allowed for the splitting constants.
                   
                   
               accelerate_ADMM: Boolean (True,False)
                   Wether to use aceleration or don't.
                   
                   
               acceleration_constant: float
                   Constant r used in the gamma_k sequence calculation as presented in the reference [2].
                   

            """

        super().__init__(smooth_model=smooth_model,
                         nonsmooth_model=nonsmooth_model,
                         set_shrinkage_functions=True)
        
        self.splitting_constants=splitting_constants

        #Construct list of splitting constants:
        if isinstance(self.splitting_constants,list):
             
            if len(self.splitting_constants) < len(self.nonsmooth_model_list):
                 
                raise ValueError("Number of splitting constants is smaller than the number of non-smooth models.")
                 
        elif isinstance(self.splitting_constants, (float,int)):
             
            constatn_list=[self.splitting_constants]
            self.splitting_constants=len(self.nonsmooth_model_list)*constatn_list


        self.beta_s=self.splitting_constants
        
        self.tol=tol
        self.optmize_options=kwargs
        self.kwargs=kwargs

        self.iteration_method=iteration_method
        self.optimizer_keywargs={"options":{"maxiter":opt_iterations,
                                            "ftol":1e-16,
                                            "gtol":1e-16}}                                          
        self.verbose=verbose
        self.check_convergence_every=1  
        
        self.adapt_splitting_constants=adapt_splitting_constants
        self.adapt_every=adapt_every
        
        
        self.max_splitting_constants=max_splitting_constants
        self.min_splitting_constants=min_splitting_constants
        
        if adapt_constant_sequence is  None:
        
            self.adapt_constant_sequence=lambda kk: 2.**(-float(kk)/100.)
        
        else:
            self.adapt_constant_sequence=adapt_constant_sequence
        
        
        self.accelerate_ADMM=accelerate_ADMM
        self.acceleration_constant=acceleration_constant
        
    def setup(self):

        #Creating workspace memory:
        self.xk=self.multiparametric_field.get_multiparametric_field()
        self.xk_1=self.xk.copy()
        

        self.Fx=self.smooth_global_objective_function
        self.DFx=self.smooth_global_gradient_function

        self.Rx_ress=self.nonsmooth_residues_workspace
        self.Rx_grad=self.nonsmooth_grad_workspace
        
        self.calculate_ress=self.nonsmooth_calculate_residues
        self.calculate_ress_grad=self.nonsmooth_jacobian_dot_vec


        self.tk=[]
        self.lk=[]

        self.tk_1=[]
        self.lk_1=[]

        for nonsmooth_model in self.nonsmooth_model_list:

            self.tk.append(np.zeros(nonsmooth_model.residues.shape))
            self.lk.append(np.zeros(nonsmooth_model.residues.shape))
            
            self.tk_1.append(np.zeros(nonsmooth_model.residues.shape))
            self.lk_1.append(np.zeros(nonsmooth_model.residues.shape))

        self.assemble_ADMM_objectives()
     
          


    def assemble_ADMM_objectives(self):
        
        L2_squared=get_norm("L2")

        
        tk=self.tk
        lk=self.lk

        Rx_ress=self.nonsmooth_residues_workspace
        Rx_grad=self.nonsmooth_grad_workspace
        
        
        Fx=self.Fx
        DFx=self.DFx

        
        calculate_ress=self.nonsmooth_calculate_residues
        calculate_ress_grad=self.nonsmooth_jacobian_dot_vec

        self.Rx=self.nonsmooth_global_objective_function

                  
        def inner_ADMM_obj(xk):

            misfit=Fx(xk)

            for ii in range(len(self.nonsmooth_model_list)):
                    
                #Calculate residues:
                calculate_ress[ii](xk,Rx_ress[ii])
                    
                beta_1=1./self.beta_s[ii]
                
                #Calcualte missfit:
                misfit+=0.5*self.beta_s[ii]*L2_squared(Rx_ress[ii]-tk[ii]+lk[ii]*beta_1)

            return misfit

                    
        def inner_ADMM_grad(xk):

            inner_grad=DFx(xk)
                 
            for ii in range(len(self.nonsmooth_model_list)):

                beta_1=1./self.beta_s[ii]
                
                calculate_ress[ii](xk,Rx_ress[ii])
                calculate_ress_grad[ii](xk,Rx_ress[ii]-tk[ii]+lk[ii]*beta_1,Rx_grad[ii]) 
                    
                inner_grad+=self.beta_s[ii]*Rx_grad[ii]
                     
            return inner_grad
        

     

                
        self.inner_ADMM_obj=inner_ADMM_obj
        self.inner_ADMM_grad=inner_ADMM_grad



    def update_tk(self,xk,tk,lk):

        shrinkage=self.nonsmooth_shrinkage_functions          
  
        for ii in range(len(self.nonsmooth_model_list)):

            beta_1=1./self.beta_s[ii]
            self.calculate_ress[ii](xk,self.Rx_ress[ii])
            tk[ii][:]=shrinkage[ii](self.Rx_ress[ii]+lk[ii]*beta_1,
                                        self.nonsmooth_epsilon[ii]*beta_1)

   

    def update_tk_and_lk(self,xk,tk,lk):
        
        shrinkage=self.nonsmooth_shrinkage_functions
        
        for ii in range(len(self.nonsmooth_model_list)):

                beta_1=1./self.beta_s[ii]
                self.calculate_ress[ii](xk,self.Rx_ress[ii])
                tk[ii][:]=shrinkage[ii](self.Rx_ress[ii]+lk[ii]*beta_1,
                                        self.nonsmooth_epsilon[ii]*beta_1)
                
                lk[ii][:]-=self.beta_s[ii]*( tk[ii]- self.Rx_ress[ii] ) 


    def update_lk(self,xk,tk,lk):

            for ii in range(len(self.nonsmooth_model_list)):

                self.calculate_ress[ii](xk,self.Rx_ress[ii])
                lk[ii][:]-=self.beta_s[ii]*( tk[ii]- self.Rx_ress[ii] ) 

     

    def calculate_primal_residues(self,lk,lk_1):
        
        rk=0
        for ii in range(len(self.nonsmooth_model_list)):
            
            beta_1=1./self.beta_s[ii]
            rk+=beta_1*( (lk[ii]-lk_1[ii])**2 ).sum()
        
        return rk



    def calculate_dual_residues(self,tk,tk_1):

        dk=0
        for ii in range(len(self.nonsmooth_model_list)):

                
            self.calculate_ress_grad[ii](self.xk,tk_1[ii][:]-tk[ii][:],self.Rx_grad[ii]) 
            dk+=self.beta_s[ii]*( (self.Rx_grad[ii]**2).sum())

        return dk
    
    def adapt_beta_s(self,tk,lk,kk):
        
        omega_k=self.adapt_constant_sequence(kk)
        print("")
        print("Adapting splitting constants:")
        for ii in range(len(self.nonsmooth_model_list)):
            
            
            tk_l2=(tk[ii]*tk[ii]).sum()
            lk_l2=(lk[ii]*lk[ii]).sum()
            
            if tk_l2>1e-16:
                
                delta_beta=np.sqrt(lk_l2/tk_l2)
                delta_beta=max(delta_beta,self.min_splitting_constants)
                delta_beta=min(delta_beta,self.max_splitting_constants)
            
                self.beta_s[ii]=(1.-omega_k)*self.beta_s[ii]+omega_k*delta_beta
            
                print(f"beta[{ii}]={self.beta_s[ii]}")

        print("")


    def accellerate(self,tk,lk,tk_1,lk_1,kk):
        
        gamma_k=float(kk)/(float(kk)+self.acceleration_constant)
        gamma_1=1./(1+gamma_k)
        
        for ii in range(len(self.nonsmooth_model_list)):
            
            tk[ii][:]*=(gamma_k+1.)
            tk[ii][:]-=gamma_k*tk_1[ii][:]
            
            lk[ii][:]*=(gamma_k+1.)
            lk[ii][:]-=gamma_k*lk_1[ii][:]
            
            tk_1[ii][:]=tk[ii][:]+gamma_k*tk_1[ii][:]
            tk_1[ii][:]*=gamma_1
            
            lk_1[ii][:]=lk[ii][:]+gamma_k*lk_1[ii][:]
            lk_1[ii][:]*=gamma_1
            
    def iterate(self, n_iter,**kwargs):

        tol=self.tol
       
        xk=self.xk
        xk_1=self.xk_1

        lk=self.lk
        lk_1=self.lk_1
        
        tk=self.tk
        tk_1=self.tk_1

        
        inner_ADMM_obj=self.inner_ADMM_obj
        inner_ADMM_grad=self.inner_ADMM_grad
        
        Fx=self.Fx
        Rx=self.Rx

        update_tk_and_lk=self.update_tk_and_lk

        converged=False
        kk=0


        failures=0
        last_failure=0
        
        
        while(not converged and kk<n_iter):

            
                            
            self.opt_result=optimize.minimize(inner_ADMM_obj,xk, method=self.iteration_method,jac=inner_ADMM_grad,**self.optimizer_keywargs)
            xk[:]=self.opt_result.x[:]

            update_tk_and_lk(xk,tk,lk)

            
            #Calculate error estimations:
            err=np.abs( (xk-xk_1 )).sum()            
            rk=self.calculate_primal_residues(lk,lk_1)
            dk=self.calculate_dual_residues(tk,tk_1)
                   
            kk+=1


            obj_error=Fx(xk)+Rx(xk)
            #err=(Fx_1-Fx)

            if self.verbose:
                print(f'k={kk},Fx={obj_error}, dx={err},rk={rk},dk={dk}')
                
            if rk<=tol and dk<=tol:
                
                if not self.opt_result.success:
                    
                    if last_failure==(kk-1):
                        
                        print("Failure in the inner problem convergence.\nTrying to recover.")
                        failures+=1
                    
                    else:
                        
                        failures=1
                        
                    
                    last_failure=kk
                    if failures>5:
                        
                        print("Failure to converge to the desired precision.")           
                        return converged
                    
                else:
                    print(f'ADMM converged after {kk} iterations.')
                    print(f'Objective Missfit={obj_error}, delta_x={err}')
                    print(f'dual residues={dk}, primal_residues={rk}')
                    converged=True
            
            
            if self.adapt_splitting_constants:
                
                if np.mod(kk,self.adapt_every)==0:
                    
                    self.adapt_beta_s(tk, lk, kk)
            
            
            
            
            if self.accelerate_ADMM:
                
                self.accellerate(tk,lk,tk_1,lk_1,kk)
                
            else:
                for ii in range(len(self.nonsmooth_model_list)):
                
                    tk_1[ii][:]=tk[ii][:]
                    lk_1[ii][:]=lk[ii][:]
            
            
            xk_1[:]=xk[:]
            
        return converged
        
    

