import numpy as np 
from .optimizer_base import data_optimizer, register_optimizer
from ..utilities.norms import get_shrinkage, get_norm

@register_optimizer
class fista(data_optimizer):
    """Class containing a driver for the fista optimizer as described in the
    article doi:  10.1137/080716542.


    Fista is an Iterative Shrinkage-Thresholding algorithm which finds a minimum of
    a compoisity function \phi(x)=f(x)+lambda_m*||x||_g, where f(x) is a smooth function,
    g is one of the available norms and lambda_m is a constant.

    The user has to provide the model for the smooth function f(x), while the function
    g(x) has to be chosen among the ones available through the parameter "regularizer".

    Available Norms:

    L1: l1-norm ( \sum abs(x) ).
    Q-L1: "Norm" induced by the tsallis distribution.


    Unrecomended available norms (use another optimizer):
    
    L2: L2-norm ( \sum x**2 ).
    
    """

    optimizer_names=["fista","FISTA","Fista"]
    short_description="Fista (Fast Iterative Shrinkage-Thresholding Algorithm) optimizer."
    
    def __init__(self,
                 smooth_model,
                 epsilon=1e-2,
                 Lc=1e2,tol=1e-6,
                 regularizer_norm='L1',
                 **kwargs):
        """ Create and set the variables necessary to implement the 
           model.
           
           Parameters:
           ----------
           
               smooth_model: smooth_model
                 The model for the smooth part of the composite function.
        
               epsilon: float
                 The scale value of the regularizer function.
 
               tol: float
                 f(x) tolerance (the algorithm will stop when F(x^(k+1))-F(x^k)<tol).

               Lc: float
                 The model Lipschitz constant.
        
               regularizer_norm: string
                 The norm of the Tikhonov regularizator.

    """


        super().__init__(smooth_model=smooth_model,
                         nonsmooth_model=None,
                         set_shrinkage_functions=False)
        

        self.epsilon=epsilon
        self.Lc=Lc
        self.tol=tol

        self.regularizer_norm=regularizer_norm
        self.optmize_options=kwargs

        self.shrinkage_function=get_shrinkage(regularizer_norm,**kwargs)
        self.regularizer_norm=get_norm(regularizer_norm,**kwargs)
            

    def setup(self):

        #Get acces to objective function and smooth gradient:
        self.obj_func=self.smooth_global_objective_function
        
        #Check wheter the working model has a grad function factory or not:
        self.grad_func=self.smooth_global_gradient_function


        self.xk=self.multiparametric_field.get_multiparametric_field()

        
        self.Lc_1=1/self.Lc
        self.yk=self.xk.copy()
        self.xk_1=self.xk.copy()


        self.tk_1=1.
        self.check_convergence_every=20

        return True


    def iterate(self, n_iter,**kwargs):

        Lc_1=self.Lc_1                
        tol=self.tol
        epsilon=self.epsilon

        obj_func=self.obj_func

        shrinkage=self.shrinkage_function        
        regularizer_norm=self.regularizer_norm
        grad_func=self.grad_func

        
        xk=self.xk
        yk=self.yk
        xk_1=self.xk_1

        
        

        converged=False
        kk=0
        tk_1=self.tk_1
        alpha_i=Lc_1*epsilon
        while(not converged and kk<n_iter):


            #Calculate new xk:            
            yk[:]=yk[:]-Lc_1*grad_func(yk)[:]
            xk[:]=shrinkage(yk,alpha_i)[:]

            #Actualize iteration parameters:
            tk=0.5*(1.+np.sqrt(1.+4.*tk_1**2))
            yk[:]=xk[:]+((tk_1-1)/tk)*(xk[:]-xk_1[:])

            
                    
            kk+=1

            #Check if the algorithm has converged:
            if( np.mod(kk,self.check_convergence_every) == 0 ):

                err=np.fabs(xk-xk_1).sum()
                
                if err<=tol:

                    print(f'Fista converged with {kk} iterations.')
                    print(f'Absolute error: err={err}')
                    converged=True

                    
            xk_1[:]=xk[:]


            
        self.tk_1=tk
        return converged
        
    
    def get_optimized_results(self,**kwargs):

        return (self.ic)
    
    def set_epsilon(self,epsilon):
        """Reset the regularizer scaling parameter
        
        Parameters:
        ----------
        
            epsilon: float
                regularizer scale parameter.
        
        
        """
        
        self.epsilon=epsilon

    
