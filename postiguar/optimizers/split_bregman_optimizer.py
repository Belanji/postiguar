import numpy as np 
from numba import njit
from .optimizer_base import data_optimizer, register_optimizer
from ..utilities.norms import get_shrinkage, get_norm, get_gradient
from .. import models
from ..models.model_base import inversion_model
from ..models.model_assembler import model_assembler

from scipy import optimize

@register_optimizer
class split_bregman(data_optimizer):
    """Class containing a driver for the split-Bregman optimizer as described in:
    THE SPLIT BREGMAN METHOD FOR L1 REGULARIZED PROBLEMS doi:  10.1137/080716542.
    
 ------------Notice:

    Beta version: This algorithm still do not achieve very good convergence behavior.
    
    """

    optimizer_names=["split_bregman","Split-Bregman"]
    short_description="Split-Bregman optimizer."

    def __init__(self, 
                 smooth_model=None,
                 nonsmooth_model=None,
                 lambda_b=0.5,
                 tol=1e-6,
                 inner_iterations=5,
                 iteration_method='L-BFGS-B',
                 opt_iterations=20,
                 estimate_auxiliary_vectors=True,
                 verbose=True,**kwargs):
        """ Create and set the variables necessary to implement the 
           model.
           
           Parameters:
           ----------
        
               smooth_model: model
                 The model for the smooth part of the composite function.
                
               nonsmooth_model: model, [models]
                 A list of nonsmooth models.

               lambda_m: float
                 The scale value of the composite function.
 
               tol: float
                 tolerance (the algorithm will stop when ||x^{k+1} -x^{k}||_1<tol).

               inner_iterations: integer
                 Number of iterations performed in the Split-Bregman inner loop. 
        
               iteration_method: string.
                 1st-step minimiation solver (it is advisable to use "cg" or "L-BFGS-B").

               opt_iterations: integer
                 Numer of iterations performed in the 1=step method.

               estimate_auxiliary_vectors: Boolean
                  Wether to start dk=bk=0 or to estimate a value before the iteration start.
    """
        
    
    
        super().__init__(smooth_model=smooth_model,
                         nonsmooth_model=nonsmooth_model,
                         set_shrinkage_functions=True)
        
        self.lambda_b=lambda_b
        self.smooth_model=smooth_model
        
        self.tol=tol
        self.optmize_options=kwargs
        self.kwargs=kwargs

        self.inner_iterations=inner_iterations
        self.iteration_method=iteration_method
        self.optimizer_keywargs={"options":{"maxiter":opt_iterations,
                                            "ftol":1e-16,
                                            "gtol":1e-16}}                                          
        self.verbose=verbose
        self.check_convergence_every=1        
        self.estimate_auxiliary_vectors=estimate_auxiliary_vectors
        
    def setup(self):
       

        #Creating workspace memory:
        self.uk=self.multiparametric_field.get_multiparametric_field()
        self.uk_1=self.uk.copy()
        
        self.assemble_bregman_auxiliary_variables()

        self.assemble_bregman_objectives()
        
        if self.estimate_auxiliary_vectors:
            
            self.update_dk(self.uk,self.bk,self.dk)
            self.update_dk(self.uk,self.bk,self.dk)
        
        return True            
        
    
    def assemble_bregman_auxiliary_variables(self):
        
        self.dk=[]
        self.bk=[]

        for nonsmooth_model in self.nonsmooth_model_list:
            
            self.dk.append(np.zeros(nonsmooth_model.residues.shape))
            self.bk.append(np.zeros(nonsmooth_model.residues.shape))



            
    
    def assemble_bregman_objectives(self):
        
        lambda_b=self.lambda_b
        lambda_m=self.nonsmooth_epsilon
        
        shrinkage=self.nonsmooth_shrinkage_functions

        L2_squared=get_norm("L2")

        dk=self.dk
        bk=self.bk

        phi_ress=self.nonsmooth_residues_workspace
        phi_grad=self.nonsmooth_grad_workspace
        #Remember that nonsmooth grad is not an actual gradient, but
        #a workspace reserved for the gradient calculation, which will be used
        #here to save space.
        
        Hu=self.smooth_global_objective_function
        DHu=self.smooth_global_gradient_function
        
        Phi=self.nonsmooth_calculate_residues
        DPhi=self.nonsmooth_jacobian_dot_vec


        def Ju(uk):

            misfit=0
            
            for ii in range(len(self.nonsmooth_model_list)):
                
                misfit+=self.nonsmooth_objective_functions[ii](uk)

            return misfit

                  
        def inner_bregman_obj(uk):

            misfit=Hu(uk)

            for ii in range(len(self.nonsmooth_model_list)):
                    
                #Calculate residues:
                Phi[ii](uk,phi_ress[ii])
                    
                
                #Calcualte missfit:
                misfit+=0.5*lambda_b*L2_squared(dk[ii]-phi_ress[ii]-bk[ii])

            return misfit

                    
        def inner_bregman_grad(uk):

            inner_grad=DHu(uk)
                 
            for ii in range(len(self.nonsmooth_model_list)):

                Phi[ii](uk,phi_ress[ii])
                DPhi[ii](uk,dk[ii]-phi_ress[ii]-bk[ii],phi_grad[ii]) 
                    
                inner_grad-=lambda_b*phi_grad[ii]
                     
            return inner_grad
        
        def update_dk(uk,bk,dk):
            
            for ii in range(len(self.nonsmooth_model_list)):
                
                    Phi[ii](uk,phi_ress[ii])
                    dk[ii][:]=shrinkage[ii](phi_ress[ii]+bk[ii],lambda_m[ii]/lambda_b)

                    
        def update_bk(uk,bk,dk):
            
            for ii in range(len(self.nonsmooth_model_list)):
                
                    #Actualize iteration parameters:
                    bk[ii][:]=bk[ii]+phi_ress[ii]-dk[ii]

        self.Ju=Ju
        self.inner_bregman_obj=inner_bregman_obj
        self.inner_bregman_grad=inner_bregman_grad
        self.update_dk=update_dk
        self.update_bk=update_bk
        
    def iterate(self, n_iter,**kwargs):

        tol=self.tol
        inner_iterations=self.inner_iterations

        
        uk=self.uk
        uk_1=self.uk_1
        dk=self.dk
        bk=self.bk

        
        inner_bregman_obj=self.inner_bregman_obj
        inner_bregman_grad=self.inner_bregman_grad
        
        Hu=self.smooth_global_objective_function
        Ju=self.Ju

        update_dk=self.update_dk
        update_bk=self.update_bk

        converged=False
        kk=0


        Fx=0
        failures=0
        last_failure=0
        
        
        while(not converged and kk<n_iter):

            
            for ii in range(inner_iterations):
                
                
                self.opt_result=optimize.minimize(inner_bregman_obj,uk, method=self.iteration_method,jac=inner_bregman_grad,**self.optimizer_keywargs)
                uk[:]=self.opt_result.x[:]

                update_dk(uk,bk,dk)

            
            err=np.abs( (uk-uk_1 )).sum()
            update_bk(uk,bk,dk)
            

                   
            kk+=1


            Fx=Hu(uk)+Ju(uk)
            #err=(Fx_1-Fx)

            if self.verbose:
                print(f'misfit={Fx} err={err}')
                
            if err<=tol:
                
                if not self.opt_result.success:
                    
                    if last_failure==(kk-1):
                        
                        print("Failure in the inner problem convergence.\nTrying to recover.")
                        failures+=1
                    
                    else:
                        
                        failures=1
                        
                    
                    last_failure=kk
                    if failures>5:
                        
                        print("Failure to converge to the desired precision.")           
                        return converged
                    
                else:
                    print(f'Split-Bregman converged after {kk} iterations.')
                    print(f'misfit={Fx} err={err}\n')
                    converged=True
            
            uk_1[:]=uk[:]
        return converged
        
    
