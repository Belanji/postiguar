from scipy import optimize
from .optimizer_base import data_optimizer, register_optimizer


@register_optimizer
class scipy(data_optimizer):
    """Class containing a a driver for the  of the 
       of the minimum squares misfit function calculated from 
       the reflectivity.
    """

    optimizer_names=["scipy","general scipy"]
    short_description="Scipy general optimizer (scipy.optimize)."
    def __init__(self,smooth_model,method='L-BFGS-B',**kwargs):
        """ Create and set the variables necessary to implement the 
           model.
           
           Parameters:
           ----------

               target_data: ndarray.
                Array containing the data to be inverted.

               model: model
                One of the model class intances. 

    """
        super().__init__(smooth_model,
                         nonsmooth_model=None,
                         set_shrinkage_functions=False)


        self.ic=self.multiparametric_field.get_multiparametric_field()

        
        self.method=method
        self.optmize_options=kwargs
        
    def setup(self):


        self.obj_func=self.smooth_model.get_objective_function()
        
        #Check wheter the working model has a grad function factory or not:
        if self.smooth_model.has_grad_function_factory:
            self.grad_func=self.smooth_model.get_gradient_function()
        else:   
            self.grad_func=None

              
        return None
        
    def iterate(self, n_iter,**kwargs):
        
        kwargs.update(self.optmize_options)
        
        try:
            kwargs["options"].update({"maxiter":n_iter})
        except:
            kwargs["options"]={"maxiter":n_iter}
        
        self.opt_results=optimize.minimize(self.obj_func,self.ic, method=self.method,jac=self.grad_func,**kwargs)
        
        self.ic[:]=(self.opt_results.x)[:]

        return self.opt_results.success
        
    
    def get_optimized_results(self,**kwargs):

        return (self.ic)

    
