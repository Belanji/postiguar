import numpy as np




def load_data(data_file,data_shape,data_type=np.float32,**kwargs):
    """Loads a binary data file and returs an array shaped as given by data_shape variable. 


    Prameters:
    ---------

        data_file: string
            The file address to import.

        data_shape: tuple (actually, any iterable).
            Shape of the output matrix.

       data_type: numpy.dtype
         Varaible containing the data type of the input file. The ouput file will have the same type.

    
    Returns:
    -------

       output_array: ndarray
         Numpy array containing the imported data.

       data_characteristics: obj
         Here a null object. In other formats it will contain the information describing the data.
    

    Notes:
    -----

        The dimension of the output array will be given by the number of arguments in the data_shape iterable.
    
    Advice: The user should avoid this format since it is not portable
            and very error prone.
    """
        
    filename=open(data_file)
    data_characteristics=None

    try:
        dim=len(data_shape)
    except TypeError:
        dim=1
        data_shape=[data_shape]    
        
    model = np.fromfile(filename, dtype=data_type)
    output_array = model.reshape(data_shape[::1])[::1]
    
    
    return output_array, data_characteristics
    
def save_data(data_file,data_array,data_type=None,**kwargs):
    """Loads a binary data file and returs an array shaped as given by data_shape variable. 


    Prameters:
    ---------

        data_file: string
            The file address to import.

        data_array: ndarray
            Array containing the data to be exported.

       data_type: numpy.dtype
         Varaible containing the data type of the output_file.

    
    Returns:
    -------

        No output generated.
    

    Notes:
    -----

        The dimension of the output array will be given by the number of arguments in the data_shape iterable.
    
        Advice: The user should avoid this format since it is not
                portable and very error prone.
    """
    
    data_array.tofile(data_file)    

    
