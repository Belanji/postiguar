from .filter_base import register_filter, data_filter
import numpy as np
from ..utilities.function_blocks import unpack_mp_field_entry


@register_filter
class add_gaussian_noise(data_filter):

    Filter_names=["add_gaussian_noise","add gaussian noise","gaussian noise"]

    @staticmethod
    def filter_data(input_field:"[multiparametric field,position]",
                    output_field:"[multiparametric field,position]",
                    mean:"float"=0,spread:"float"=1.,
                    seed=None):
        """Add a gaussian noise centered at mean with spread=spread to the input data.
    
        Parameters:
        ----------

            input_data: multiparametric_field, field position.
                Array contaning the data to be extracted.

            output_data: multiparametric_field, field position.
                Array contaning the data to be extracted.

            center: flaot
               Mean of the noise.
            
            spread: flaot
               Spread of the random noise.

        """


        

            
        
        gaussian_noise=np.random.default_rng(seed).normal(mean,spread,input_field.shape)

        
        output_field[...]=input_field[...]+gaussian_noise[...]
    
        
