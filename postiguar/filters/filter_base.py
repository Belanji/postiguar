from abc import ABC, abstractmethod

_available_Filters={}

def register_filter(Filter):
    """Function decorator: Register a Filter in the list of
    availabel Filters. You must pass this decorator to every Filter
    that you implement, if you intend to use the available Filters
    list (which is the way this program is intended to be used).

    Parameters:
    ----------
       
        Filter: Filter 
            Filter to be registered. We strong advise to use
           this function only as a decorator.

    """

    
    names=Filter.Filter_names

    #Check if the Filter has properly defined Filter_names:
    if not isinstance(names,list):
            
        raise TypeError("""Trying to import a Filter with a "Filter_names" variable improperly defined.
        Please, in the source code, define the "Filter_names" variable and make sure it is a list and try again.""")

    #register the Filter in the available Filter dict:
    for name in names:
        _available_Filters[name]=Filter

        
    return Filter


def get_filter(Filter_name):
    """Search am inversion Filter in the list of available ones. The 
    search is performed by the Filter name.

        Parameters:
        ----------

            Filter_name: str
                The name of the mode to be searched (as a string)

        Returns:
        -------

            Filter: inversion_Filter
                An instance of the searched Filter.

    """
    
    Filter=_available_Filters[Filter_name]

    return Filter



def list_available_filters(search_key=None,*args,**kwargs):

    Filters=list(set(_available_Filters.values()))
    
    print("Available Filters:\n")
    for Filter in Filters:

        Header=""
        for name in Filter.Filter_names:

            Header+=name+", "
            

        
        Header=Header[0:-2]    
        info=Header+":\n"+Filter.short_description+"\n"

        print(info)

        
    return None


class data_filter(ABC):
    """Abstract base class to help implementing a filter."""

    filter_names=None
    short_description="No description provided."
    
#    @abstractmethod
#    def filter_data(self,args,kwargs):
#        """Pass the filter in the data field."""
