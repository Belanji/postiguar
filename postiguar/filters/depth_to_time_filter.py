#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 14:22:08 2021

@author: souzarf
"""

from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry

import numpy as np
from  scipy import interpolate

@register_filter
class depth_to_time(data_filter):

    Filter_names=["depth_to_time","depth to time"]

    @staticmethod
    def filter_data(input_field,
                    velocity_field,
                    output_field,
                    time_sampling,
                    depth_spacing,
                    max_time=None):
                    
        """Transform fields defined in depth(in units of metters) to fields defined over T.W.T(in units of ms), for now very roughtly.
    
        Parameters:
        -----------

            input_data: field(s), multiparametric_field.
                
            velocity_field: field.
                Velocity field used for conversion.


            output_data: multiparametric_field.
                Field where transformed data will be placed. A new multiparetric_field is created if no value is passed.
                
        Usage:
        ------
        
            Fill here later.

        """
        
        
        mp_input, input_data, input_pos=unpack_mp_field_entry(input_field)
            
        #Process output field:        
        mp_output, output_data, output_pos=unpack_mp_field_entry(output_field)
        
        mp_velocity, velocity_data, velocity_pos=unpack_mp_field_entry(velocity_field)
        
        

        time_coordinate=depth_to_time._calculate_time_positions(velocity_data,depth_spacing)
        
    @staticmethod
    def _calculate_time_positions(velocity_data,dz,reference_time):
    
        depth_length=velocity_data.shape[-1]
        time_coordinate=velocity_data.copy()
        time_coordinate[...,0]=reference_time
        
        
        
    
        for ii in range(1,depth_length):
    
            time_coordinate[...,ii]=time_coordinate[...,ii-1]+2*dz/velocity_data[...,ii-1]

        return time_coordinate




        
        
        