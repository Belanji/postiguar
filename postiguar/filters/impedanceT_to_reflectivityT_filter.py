import numpy as np

from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry
@register_filter

class impedanceT_to_reflectivityT(data_filter):

    Filter_names=["impedanceT_to_reflectivity","impedanceT to reflectivity","impT2refT"]
    short_description="Transform data from impedance time to reflectivity time."

    @staticmethod
    def filter_data(input_field,
                    output_field,
                    ln_aproximation=False):
        """Transform data from impedance time to reflectiviy time.
     
    
        Input Parameters:
        ---------------
    
            input_data: ndarray.
                data to be transformed.
                    
            ln_aproximation:bool
                Wheter de aproximation r=D_t ln[Z(t)] should be used or not (True for yes). 
        

        Returns:
        -------
    
            output_data:np.ndarray
                Array containing the amplituted time.
        
        Notes:
        -----
        
            If data dim>1, the time dimension should be the first index.
        
            Converted data has one less element in the time dimension.
        """


        #Process de input field:
        mp_input, input_data, input_pos=unpack_mp_field_entry(input_field)
            
        #Process output field:        
        mp_output, _, output_pos=unpack_mp_field_entry(output_field)


        #Reshape Input data:
        input_shape=mp_input.field_shapes[input_pos]
        input_data=input_data.reshape(input_shape)
        
        #Calculate the reflectivity:
        if ln_aproximation:
            reflectivity=(np.log(input_data[...,1:])-np.log(input_data[...,0:-1]))/2.
        else:    
            reflectivity=( ( input_data[...,1:]-input_data[...,0:-1])/
                 ( input_data[...,0:-1]+input_data[...,1:]) )
    
                

        #Rehape the input and output  fields:
        mp_output.reshape_field(output_pos,reflectivity.shape)        
        reflectivity=reflectivity.flatten()
                        
        #replace the data:
        output_data=mp_output.get_field(output_pos)
        output_data[:]=reflectivity[:]
        
 
