import numpy as np
from ..operators.convolution import create_convolution_function

from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry

@register_filter
class impedanceT_to_amplitudeT(data_filter):

    Filter_names=["impedanceT_to_amplitudeT","impedanceT to amplitudeT","impT2ampT"]
    short_description="Transform data from impedance time to amplitude time."

    @staticmethod
    def filter_data(impedance,
                    wavelet,
                    amplitude,
                    ln_approximation=False,
                    causal_wavelet=True,
                    convolution_mode="full",
                    dt=1.):
        """Transform data from impedance time to amplitude time.
     
    
        Input Parameters:
        ---------------
    
            impedance: (multiparametric_field,field_position)
                impedance per time.
            
        
            wavelet: (multiparametric_field,field_position)
               wavelet used to perform the transformation.
            
            amplitude: (multiparametric_field,field_position).
                Array containing the amplituted time.
        
            ln_approximation:bool
                Wheter de aproximation r=D_t ln[Z(t)] should be used or not (True for yes). 
        

            dt: wavelet sampling time.
                Time spacing which the wavelet is sampled (it must conform with the amplitude units).            
        
        Notes:
        -----
        
            If data dim>1, the time dimension should be the first index.
        
            Converted data has one less element in the time dimension.
        """


        #Get inputvariables shape:
    
        wavelet_shape=wavelet.shape
        amplitude_shape=amplitude.shape
       
        #impedance=impedance.ravel()
        wavelet=wavelet.ravel()
        amplitude=amplitude.ravel()

        #Process de input field:

        
        #Calculate the reflectivity:
        if ln_approximation:
            reflectivity=0.5*(np.log(impedance[...,1:])-np.log(impedance[...,0:-1]))
        else:    
            reflectivity=( ( impedance[...,1:]-impedance[...,0:-1])/
                 ( impedance[...,0:-1]+impedance[...,1:]) )
    
    
       
        
        reflectivity_shape=reflectivity.shape
        reflectivity=reflectivity.flatten()
        
        
        convolve=create_convolution_function(wavelet_shape,reflectivity_shape,amplitude_shape,causal_wavelet=causal_wavelet,convolution_mode=convolution_mode)
                
        convolve(wavelet,reflectivity,amplitude)
      
        
                        

