from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry
import numpy as np

@register_filter
class slice_field(data_filter):

    Filter_names=["slice_field","slice field", "slice"]

    @staticmethod
    def filter_data(input_field,data_window,output_field=None,**kwargs):
        """Filter that takes an input data field and extract a window with given shape.
    
        Parameters:
        ----------

            input_data: ndarray
                Array contaning the data to be extracted.

            data_window: iterable ( (start=0,end,stride=1), (start=0,end,stride=1, ... )
               An iterable containing the shape of the data to be
               extracted.

        Returns:
        -------

            output_data: ndarray
               An ndarray containing the extracted data.


        Notes:
        -----

            The iterable should contain dim iterables organized as
           ([start], end, [stride]). Start and stride re optinoal
           arguments.


        """

        #If output is not passed, perform the transformation in place:
        if output_field is None:
            output_field=input_field

        
        #Process de input field:
        mp_input, input_data, input_pos = unpack_mp_field_entry(input_field)
            
        #Process output field:        
        mp_output, _, output_pos = unpack_mp_field_entry(output_field)


        #Get the data shape:
        input_data_shape=mp_input.field_shapes[input_pos]
        
        #Ckeck input_data number of dimensions:    
        dim=len(input_data_shape)

        temp_data=input_data.reshape(input_data_shape).copy()
    
        if dim==1:
            sliced_data=temp_data[slice(*data_window)]
        else:
            window=tuple(slice(*ii) for ii in data_window)
            sliced_data=temp_data[window]

            #Guarantee that we work with a data copy:
            sliced_data=sliced_data.copy()


        #Get new data shape:
        new_shape=sliced_data.shape
        sliced_data=sliced_data.flatten()
        
        
        #Rehape de field:
        mp_output.reshape_field(output_pos,new_shape)

        #Repalce the data:
        output_data=mp_output.get_field(output_pos)
        output_data[:]=sliced_data[:]
        
