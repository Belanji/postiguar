import numpy as np
from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry

@register_filter
class transpose_field(data_filter):

    Filter_names=["transpose_field","transpose field","transpose"]
    short_description="Perform the transpose operator in a 2D field."
    
    @staticmethod
    def filter_data(input_field,output_field=None):
        """Perform the transpose operator in a 2D field.
     
    
        Input Parameters:
        ---------------
    
            input_data: ndarray (dim=2).
                data to be transposed.
            
        
        Returns:
        -------
    
            output_data:np.ndarray
                Array containing the transposed field.
        
        """
        if output_field is None:
            output_field=input_field

        
        #Process de input field:
        _, input_data, _=unpack_mp_field_entry(input_field,flatten=False)
            
        #Process output field:        
        mp_output, _, output_pos=unpack_mp_field_entry(output_field)

        
        temp_data=input_data.copy().T

        new_shape=mp_output.field_shapes[output_pos]
        new_shape=(new_shape[1],new_shape[0])

        #Rehape de field:
        mp_output.reshape_field(output_pos,new_shape)
        
        #Replace de data:
        output_data=mp_output.get_field(output_pos)
        output_data[:]=temp_data.flatten()[:]
        
        
