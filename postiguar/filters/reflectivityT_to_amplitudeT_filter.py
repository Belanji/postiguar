from ..operators.convolution import create_convolution_function

from .filter_base import register_filter, data_filter
from ..utilities.function_blocks import unpack_mp_field_entry

@register_filter
class reflectivityT_to_amplitudeT(data_filter):

    Filter_names=["reflectivityT_to_amplitudeT","reflectivityT to amplitudeT","refT2ampT"]
    short_description="Transform data from reflectivity time to amplitude time."

    @staticmethod
    def filter_data(reflectivity,
                    wavelet,
                    amplitude,
                    causal_wavelet=True,
                    convolution_mode="full",
                    dt=1.):
        """Transform data from reflectivity time to amplitude time.
     
    
        Input Parameters:
        ---------------
    
            input_data: ndarray.
                data to be transformed.
            
        
            wavelet: ndarray.
               wavelet used to perform the transformation.
            
            dt: wavelet sampling time.
                Time spacing which the wavelet is sampled (it must conform with the amplitude units).

            output_data:np.ndarray
                Array containing the amplituted time.
        
        Notes:
        -----
        
            If data dim>1, the time dimension should be the first index.
        
            Converted data has one less element in the time dimension.
        """



        
        #Get inputvariables shape:
        reflectivity_shape=reflectivity.shape     
        wavelet_shape=wavelet.shape
        amplitude_shape=amplitude.shape
       
        reflectivity=reflectivity.ravel()
        wavelet=wavelet.ravel()
        amplitude=amplitude.ravel()
        
        convolve=create_convolution_function(wavelet_shape,reflectivity_shape,amplitude_shape,causal_wavelet=causal_wavelet,convolution_mode=convolution_mode)                
        convolve(wavelet,reflectivity,amplitude)
        
