import numpy as np
from scipy.sparse import  lil_matrix
from numba import njit
from operator import mul, add
from functools import reduce




class derivative_operator:
    
    def __init__(self,input_field_shape,dimensions,order):

        try:
            self.order=tuple(order)
        except TypeError:
            self.order=(order,)
    
            
        try:
            temp_shape=list(input_field_shape)
        except TypeError:
            temp_shape=[input_field_shape]


        #Create derivative dimension list:
        try:
            dimensions=list(dimensions)
        except TypeError:
            dimensions=[dimensions]


        dim_diff=3-len(temp_shape)  
        #Check if derivative dimensions and order are compatible:

        if len(dimensions)!=len(self.order):
            raise ValueError(f'Derivatives dimension {dimensions} incompatible with derivatives order {order}.\n Please, ensure that the derivatives order and dimensions have the same length.\n')

        

        #Check if the derivative dimensions are valid:

        for dimension in dimensions:
            if (dimension)>=len(temp_shape) or dimension<-len(temp_shape):
            
                raise ValueError("Derivative dimension incompatibile with data dimesnion.")

        
        for ii in range(len(dimensions)):
            
            dimensions[ii]=dim_diff+dimensions[ii] if dimensions[ii]>= 0 else 3+dimensions[ii]    
            
        self.input_vector_size=reduce(mul,input_field_shape)
        line_reduction=reduce(add,self.order)
        

        #recast input shape to the 3D version:
        self.input_shape=np.ones(3,dtype="int32")
        self.input_shape[dim_diff:]=temp_shape

        
        #Calculate output_vector_size:
        self.output_shape=np.ones(3,dtype="int32")
        self.output_shape[dim_diff:]=temp_shape

        for dimension in dimensions:
            self.output_shape[dimension]+=-line_reduction        
    
        self.output_vector_size=reduce(mul,self.output_shape)
        
        #Set the operator shape atribute:
        self.shape=(self.output_vector_size,self.input_vector_size)


        #Calculate offsets for left multiplication:
        self.input_offsets=np.ones(3,dtype=np.int32)
        
        self.input_offsets[0]=self.input_shape[-1:0:-1].prod()
        self.input_offsets[1]=self.input_shape[-1:1:-1].prod()
        
    

        #Calculate offsets for right multiplication:
        self.output_offsets=np.ones(3,dtype=np.int32)
        
        self.output_offsets[0]=self.output_shape[-1:0:-1].prod()
        self.output_offsets[1]=self.output_shape[-1:1:-1].prod()
        

       
        self.derivative_offsets=np.ones(len(dimensions),dtype="int32")
        for ii in range(len(dimensions)):
            dim=dimensions[ii]
            self.derivative_offsets[ii]=self.input_shape[-1:dim:-1].prod()


        
        self.left_apply=self._create_left_apply()
        self.right_apply=self._create_right_apply()

    def _create_left_apply(self):
        
        Nx=self.output_shape[0]
        Ny=self.output_shape[1]
        Nz=self.output_shape[2]
        
        dx=self.input_offsets[0]
        dy=self.input_offsets[1]
        dz=1
        
        
        
        if self.order==(0,):

            @njit
            def left_apply(input_field,derivated_field):
            
                derivated_field[:]=input_field[:]
            
        elif self.order==(1,):

            offset=self.derivative_offsets[0]

            @njit
            def left_apply(input_field,derivated_field):    
            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                        
                            ij=ii*dx+jj*dy+kk*dz
                            derivated_field[line]=input_field[ij+offset]-input_field[ij]
                            line+=1
                            
        elif self.order==(2,):

            offset=self.derivative_offsets[0]
            
            @njit
            def left_apply(input_field,derivated_field):
            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                        
                            ij=ii*dx+jj*dy+kk*dz
                            derivated_field[line]=input_field[ij+2*offset]-2*input_field[ij+offset]+input_field[ij]
                            line+=1
        elif self.order==(1,1):

            off1=self.derivative_offsets[0]
            off2=self.derivative_offsets[1]
            
            @njit
            def left_apply(input_field,derivated_field):
            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                        
                            ij=ii*dx+jj*dy+kk*dz
                            derivated_field[line]=input_field[ij+2*off1+2*off2]+input_field[ij]-input_field[ij+2*off1]-input_field[ij+2*off2]
                            line+=1

        else:
            raise ValueError("Derivative order not implemented chose among 0,1 or 2")
        return left_apply


    def _create_right_apply(self):
        
        Nx=self.output_shape[0]
        Ny=self.output_shape[1]
        Nz=self.output_shape[2]
        
        dx=self.input_offsets[0]
        dy=self.input_offsets[1]
        dz=1

        
        
        if self.order==(0,):
            @njit
            def right_apply(input_field,derivated_field):
            
                derivated_field[:]=input_field[:]
            
        elif self.order==(1,):

            offset=self.derivative_offsets[0]
            
            @njit
            def right_apply(input_field,derivated_field):
                
                derivated_field[:]=0
                
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                            ij=ii*dx+jj*dy+kk*dz
                            
                                
                            derivated_field[ij]+=-input_field[line]
                            line+=1
                            
                            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                            ij=offset+ii*dx+jj*dy+kk*dz
                            
                                
   
                            derivated_field[ij]+=input_field[line]
                            line+=1

        elif  self.order==(2,):

            offset=self.derivative_offsets[0]

            @njit           
            def right_apply(input_field,derivated_field):
                
                derivated_field[:]=0
                
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                            ij=ii*dx+jj*dy+kk*dz
                            
                                
                            derivated_field[ij]+=input_field[line]
                            line+=1


                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                            ij=offset+ii*dx+jj*dy+kk*dz
                            derivated_field[ij]+=-2*input_field[line]
                            line+=1
                            
                            
                            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                            ij=2*offset+ii*dx+jj*dy+kk*dz
                            
                            derivated_field[ij]+=input_field[line]
                            line+=1

        elif self.order==(1,1):

            off1=self.derivative_offsets[0]
            off2=self.derivative_offsets[1]
            
            @njit
            def right_apply(input_field,derivated_field):

                derivated_field[:]=0
                
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
               
                        
                            ij=ii*dx+jj*dy+kk*dz
                            derivated_field[ij]+=input_field[line]
                            line+=1
                            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):

                            ij=2*off1+2*off2+ii*dx+jj*dy+kk*dz
                            derivated_field[ij]+=input_field[line]
                            line+=1
                            
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
                            
                            ij=2*off1+ii*dx+jj*dy+kk*dz
                            derivated_field[ij]+=-input_field[line]
                            line+=1
                
                line=0
                for ii in range(Nx):
                    for jj in range(Ny):
                        for kk in range(Nz):
                            
                            
                            ij=2*off2+ii*dx+jj*dy+kk*dz
                            derivated_field[ij]+=-input_field[line]
                            line+=1

        else:
            raise ValueError("Derivative order not implemented chose among 0,1 or 2")
        return right_apply



def create_derivative_matrix(target_field_shape,dimensions=(-2,-1),order=(1,1)):
    """Create the matrix which performs the vector derivatives.
    """

    try:
        order=tuple(order)
    except TypeError:
        order=(order,)
    
    #Get basic information:

    try:
        dimensions=list(dimensions)
    except TypeError:
        dimensions=[dimensions]

    try:
        temp_shape=list(target_field_shape)
    except TypeError:
        temp_shape=[target_field_shape]
        
    input_vector_size=reduce(mul,target_field_shape)
    line_reduction=reduce(add,order)
    
    #Calculate output vector shape:
    temp_length=len(temp_shape)
    dim_diff=3-temp_length

    for ii in range(len(dimensions)):
        
        dimensions[ii]=dim_diff+dimensions[ii] if dimensions[ii]>= 0 else 3+dimensions[ii] 

    #Recast input shape to a 3D array:
    input_shape=np.ones(3,dtype="int32")
    input_shape[dim_diff:]=temp_shape
    
    #Calculate output shape:
    output_shape=np.ones(3,dtype="int32")
    output_shape[dim_diff:]=temp_shape

    for dimension in dimensions:
        output_shape[dimension]+=-line_reduction        

    output_vector_size=reduce(mul,output_shape)
    
    offsets=np.ones(3,dtype="int32")
    offsets[0]=input_shape[-1:0:-1].prod()
    offsets[1]=input_shape[-1:1:-1].prod()

    #Create the functinion which calculate point position:
    point_position= lambda dim1,dim2,dim3: dim1*offsets[0]+dim2*offsets[1]+dim3                        

    if order==(0,):

        
        star=[0]
        values=[1.]
        
    elif order==(1,):

        off1=offsets[dimensions[0]]
        
        star=[0,off1]
        values=[-1,1]
        
    elif order==(2,):

        off1=offsets[dimensions[0]]
        
        star=[0,off1,2*off1]
        values=[1.,-2.,1]
        
    elif order==(1,1):

        off1=offsets[dimensions[0]]
        off2=offsets[dimensions[1]]


        #Set derivative star:
        star=[2*off1+2*off2,0,2*off1,2*off2]
        values=[1.,1.,-1.,-1.]
        
    else:
        print(f'Derivative order {order} is not implemented. Please chose among 0,1,2 or (1,1)\n')
        return 0
        
    Dm=lil_matrix((output_vector_size,input_vector_size))    
    
    line=0
    #Filling the matrix operator:

    for pp in range(output_shape[0]):
        for qq in range(output_shape[1]):
            for ii in range(output_shape[2]):
                    
                for jj,value in zip(star,values):
                              
                    
                    ij=point_position(pp,qq,ii)+jj
            
                    
                    Dm[line,ij]=value
                line+=1

    return Dm.tocsr()
