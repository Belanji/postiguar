import numpy as np
from scipy.linalg import toeplitz
from scipy.sparse import csr_matrix
from numba import njit


@njit
def convolve_fields(field1,field2,output_field,field_length):

    output_field[:]=0
        
    for ii in range(field_length):        
        for jj in range(ii,field_length):
            
            output_field[jj]+=field2[ii]*field1[jj-ii]
    

def create_stationary_convolution_matrix(wavelet,sparse=False,sparse_eps=1e-9):
    """ Create a convolution matrix based in a stationary wavelet.
    
    Parameters:
    ----------
        
        wavelet:np.ndarray (dim==1)
            One dimensional array containing the wavelet.
        
        sparse: bool [optional]
             Wheter the convolution matrix is a csr sparse matrix(True) or a
            dense one (false).
        
    Returns:
    -------

        convolution_matix: np.ndarray
            matrix which can be used to map a elastic model to an amplitude one.
    
    """

    
    w_size=len(wavelet)


    #Construct the convolution operator as a toeplitz matrix:
    rows=np.zeros(w_size)
    rows[0]=wavelet[0]    
    convolution_matrix=toeplitz(wavelet,rows).astype(dtype=wavelet.dtype)        

    
    if  sparse:

        #Get the indexes and values where wav[i,j]>sparse_eps
        wav_values=convolution_matrix[np.abs(convolution_matrix)>sparse_eps]
        wav_indexes=np.argwhere(np.abs(convolution_matrix)>sparse_eps)
        row_index=wav_indexes[:,0]
        col_index=wav_indexes[:,1]
        
        #Create the sparse convolution matrix
        convolution_matrix=csr_matrix((wav_values,(row_index,col_index)),dtype=wavelet.dtype)
        
    return convolution_matrix

def create_convolution_function(field1_shape,field2_shape,output_shape,causal_wavelet=True,convolution_mode="full"):
    """ Create a convolution function based in a stationary wavelet.
    
    Parameters:
    ----------
        
        field1_shape: tuple([int],[int],int) 
            shape of the first field to be convoluted.
        
        field2_shape: tuple([int],[int],int)
            shape of the second field to be convoluted with.

        output_shape: tuple([int],[int],int)
            Shape of the output vector.
            
        causal_wavelet: bollean,optional
            whether the input wavelet is causal or not.
            
        convolution_mode: str{"full", "same"},optional

    Returns:
    -------

        convolution_func: func(wavelet,field)
            Function which can be used to convlute a field with a wavelet.
    
    """

    imcompatibility_message=f"Field1 with shape {field1_shape} incompatible with field 2 shaped as {field2_shape} and output shaped as {output_shape} in convolution mode {convolution_mode}."

    full_mode_message="\nThe output vector must must be shaped as field1.shape[...:-1]+field2[...:-1]-1.\n"

     
    #Get the last dimesnion field_length
    if causal_wavelet:

        vec1_length=field1_shape[-1]
        vec2_length=field2_shape[-1]
        output_length=output_shape[-1]

    else:

        vec1_length=field1_shape[-1]//2
        vec2_length=field2_shape[-1]
        output_length=output_shape[-1]
        
        
    if convolution_mode=="full":
        
        if output_length != vec1_length + vec2_length-1:
            
            raise ValueError(imcompatibility_message+full_mode_message)
            
    elif convolution_mode=="same":
        
        if output_length!=vec2_length:
            
            raise ValueError(imcompatibility_message)
            
    else:
        
        raise ValueError('Invalid convolution mode.\nPlease choose between "same" or "full".')
            

        
    #Calculate the number of fields to convolve:
    if len(field2_shape)>1:
        
        N_vec=np.prod(field2_shape[:-1])
        
    else:
        N_vec=1
        

    
    if causal_wavelet:

        #Causal convolution:

        
        @njit(fastmath=True)
        def convolution_function(field1,field2,output):
            
            output[:]=0

            
            #Convolution with the lower half:
            for kk in range(N_vec):     
                for ii in range(vec1_length):        
                    for jj in range(ii,vec2_length):
            
                        output[kk*output_length+jj]+=field1[ii]*field2[kk*vec2_length-ii+jj]


#Convolution with the upper half:
            if convolution_mode=="full":
                
                vec_length=vec1_length-1
                
                
                
                #Convolution with the upper half:
                for kk in range(N_vec):     
                    for ii in range(0,vec_length):        
                        for jj in range(0,vec_length-ii):
                
                            output[kk*output_length+vec2_length+jj]+=field1[vec_length-ii]*field2[(kk+1)*vec2_length-vec_length+ii+jj]

        return convolution_function

        


    #Non-causal convolution:
    else:


        @njit(fastmath=True)
        def convolution_function(field1,field2,output):
            
            output[:]=0

            #Convolution with the upper half:
            for kk in range(N_vec):     
                for ii in range(0,vec1_length+1):        
                    for jj in range(0,vec2_length-ii):
                
                        output[kk*output_length+jj]+=field1[vec1_length-ii]*field2[kk*vec2_length+ii+jj]

                
            #Convolution with the lower half:
            for kk in range(N_vec):     
                for ii in range(1,vec1_length+1):        
                    for jj in range(ii,vec2_length):
            
                        output[kk*output_length+jj]+=field1[vec1_length+ii]*field2[kk*vec2_length-ii+jj]
        
        

        return convolution_function


                        
                
    

def create_convolution_transposed_function(field2_shape,field1_shape,output_shape,causal_wavelet=True,convolution_mode="full"):
    """ Create a convolution function to be multiplated by the left based in a stationary wavelet.
    
    Parameters:
    ----------
        
        field1_shape: 
            shape of the wavelet which will be used in the convolution.
        
        field2_shape:
            shape of the field to be convoluted with.

    Returns:
    -------

        convolution_func: func(wavelet,field)
            Function which can be used to convlute a field with a wavelet.
    
    """

    imcompatibility_message=f"Field1 with shape {field1_shape} incompatible with field 2 shaped as {field2_shape} and output shaped as {output_shape} in convolution mode {convolution_mode}."

    full_mode_message="\nThe field1 vector must must be shaped as output.shape[-1]+field1.shape[-1]-1.\n"



    #Calculate the length
    if causal_wavelet:

        vec1_length=field1_shape[-1]
        vec2_length=field2_shape[-1]
        output_length=output_shape[-1]
        
    else:
        
        vec1_length=field1_shape[-1]//2
        vec2_length=field2_shape[-1]
        output_length=output_shape[-1]
        
    #Calculate the number of fields to convolve:
    if len(field2_shape)>1:
        
        N_vec=np.prod(field2_shape[:-1])

    else:
        N_vec=1
        



    if convolution_mode=="full":
        
        if vec2_length != vec1_length + output_length-1:
            
            raise ValueError(imcompatibility_message+full_mode_message)
            
    elif convolution_mode=="same":
        
        if output_length!=vec2_length:
            
            raise ValueError(imcompatibility_message)
            
    else:
        
        raise ValueError('Invalid convolution mode.\nPlease choose between "same" or "full".')            
        
    size_diff=vec2_length-output_length
    
    if causal_wavelet:

        if len(field1_shape)==1:

            @njit(fastmath=True)
            def convolution_function(field2,field1,output):
            
                output[:]=0

                #Convolution with the lower half:
                for kk in range(N_vec):     
                    for ii in range(vec1_length):        
                        for jj in range(output_length-ii):
                
                            output[kk*output_length+jj]+=field1[ii]*field2[kk*vec2_length+ii+jj]

                
        
                if convolution_mode=="full":

                    size_diff=vec1_length-1
                    
                    for kk in range(N_vec):
                        
                        #breakpoint()    
                        for ii in range(0,size_diff):        
                            for jj in range(ii,size_diff):
            
                                
                                output[(kk+1)*output_length-size_diff+jj]+=field1[size_diff-ii]*field2[(kk+1)*vec2_length-size_diff-ii+jj]
                                                                        

                    

            return convolution_function

        else:

            raise ValueError("Non stationary wavelet currently removed form postiguar")
    else:

        
        if len(field1_shape)==1:

            @njit(fastmath=True)
            def convolution_function(field2,field1,output):
            
                output[:]=0

                #Convolution with the lower half:
                for kk in range(N_vec):     
                    for ii in range(vec1_length+1):        
                        for jj in range(output_length-ii):
                
                            output[kk*output_length+jj]+=field1[vec1_length+ii]*field2[kk*output_length+ii+jj]

                
                #Convolution with the upper half:
                for kk in range(N_vec):     
                    for ii in range(1,vec1_length+1):        
                        for jj in range(ii,output_length):
            
                            output[kk*output_length+jj]+=field1[vec1_length-ii]*field2[kk*output_length-ii+jj]
        
        

            return convolution_function

        
        else:

            raise ValueError("Non-Stationary wavelet implementation removed for a while")
            

    
        
    

